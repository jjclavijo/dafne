# Crossvalidate parameters

An example of generating a config.json file to crossvalidate
training hyperparameters.

The `config.py` script generates the configuration file, then, the invoke script
can be called just as explained on [the models section](../../models/)
