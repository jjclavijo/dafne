import hashlib
import json

cases = []

# Generate configurations in loop, example, test batch sizes
for ex in [8,9,10,11]:
                dicc = {
                    "routine": "01-Gc-GAN",
                    "d_dropout": 0.1,
                    "d_sn": False,
                    "d_lr": 1e-5,
                    "g_dropout": 0.05,
                    "g_sn": False,
                    "g_lr": 1e-5,
                    "bs": 2**ex,
                    "dataset": "ds_soft",
                    "reg_n": True,
                    "reg_norm": 'square',
                    "reg_self": False,
                    "alpha": 100,
                    "epochs": 1000,
                    "step_rotates": True,
                }
                # Generate a hash to identify folders
                h = hashlib.md5()
                h.update(repr(frozenset(dicc.items())).encode())
                h = h.hexdigest()
                dirs = {
                    "report": f"r-{h[:8]}",
                    "ckpt": f"c-{h[:8]}-",
                    "loss_fname": f"r-{h[:8]}/l-{h[:8]}",
                }
                dicc.update(dirs)
                cases.append(dicc)

with open("config.json", "w") as f:
    json.dump(cases, f)

