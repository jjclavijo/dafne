# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.13.5
#   kernelspec:
#     display_name: Python 3 (ipykernel)
#     language: python
#     name: python3
# ---

import click

### Parametros

# tipo Variante del modelo
# rutaD ruta a los checkpoints
# rutaG
# eq = 4830
# dist = 1500
# fname



def loadModel(tipo,rutaD,rutaG):
    if tipo == 1:
        import dfmodels.models.model1 as model
    if tipo == 2:
        import dfmodels.models.model2labels as model
    if tipo == 25:
        import dfmodels.models.model2 as model
    if tipo == 3:
        import dfmodels.models.model3 as model

    G = model.deceiv()
    G.load_weights(rutaG)

    D = model.discrim()
    D.load_weights(rutaD)

    return G,D

def ll_grid(*limits):

    import shapely.geometry as geom
    from math import ceil,floor
    import numpy as np
    import geopandas as gpd

    min_lon, max_lon, min_lat, max_lat = limits

    max_lon = floor(max_lon/5)*5
    min_lon = ceil(min_lon/5)*5
    max_lat = floor(max_lat/5)*5
    min_lat = ceil(min_lat/5)*5

    mer_lon = np.arange(min_lon,max_lon+1,5)
    mer_lat = np.linspace(min_lat-5,max_lat+5,100)

    par_lat = np.arange(min_lat,max_lat+1,5)
    par_lon = np.linspace(min_lon-5,max_lon+5,100)

    Mlo,Mla = np.meshgrid(mer_lon,mer_lat)
    Plo,Pla = np.meshgrid(par_lon,par_lat)

    merid = [*zip(Mlo.T,Mla.T)]
    par = [*zip(Plo,Pla)]

    geom_m = [*map(geom.LineString,[[*zip(*i)] for i in merid])]
    geom_p = [*map(geom.LineString,[[*zip(*i)] for i in par])]

    paralelos = gpd.GeoSeries(geom_p, crs='EPSG:4326')
    meridianos = gpd.GeoSeries(geom_m, crs='EPSG:4326')

    return paralelos,meridianos


@click.command()
@click.option('--tipo',default=1)
@click.option('--rutaD',default='')
@click.option('--rutaG',default='')
@click.option('--eq',default=4830)
@click.option('--dist',default=1500)
@click.option('--fname',default='out.png')
@click.option('--scale',default=0.1)
@click.option('--base',default=False)
@click.option('--scalediff',default=5)
@click.option('--mapzoom',default=5)
@click.option('--limites',default='')
@click.option('--factor',default=1.0)
@click.option('--factorl',default=1.0)
@click.option('--factors',default=1.0)
@click.option('--jsonfile',default='')

def main(tipo,rutad,rutag,eq,dist,
         fname,scale,base,scalediff,
         mapzoom,limites,factor,factorl,
         factors,jsonfile):

    import psycopg2 as pg
    import pandas as pd
    import geopandas as gpd
    import numpy as np
    import json
    import contextily as ctx
    import subprocess as sp
    import matplotlib.pyplot as plt
    from dfmodels.tf.fun import apply_step as S
    import shapely.geometry as geom
    from math import ceil,floor

    #conn = pg.connect(host='localhost',user='postgres',database='sismoident')
    #data = gpd.read_postgis('SELECT * FROM http_estaciones',conn)

    G,D = loadModel(tipo,rutad,rutag)

    # cmd = ['/opt/bin/proy','-e',f'{eq}','--dist',f'{dist*1000}','--days','30']

    # output = sp.check_output(cmd,cwd='/')

    with open(jsonfile) as f:
        jdata = json.load(f)

    # jdata = json.loads(output)

    sitios_series, data, data_eq = jdata
    sitios = sitios_series['puntos']
    series = sitios_series['instances']

    series = [[[i if i != 'NaN' else np.nan for i in j] for j in k] for k in series]
    seriesS = pd.Series(series,index=sitios)
    seriesS = seriesS.map(np.array).map(lambda x: x - np.nanmean(x,axis=0))

    # +
    d = np.stack(seriesS.values)

    s = G(d)

    d1 = S(d,s)
    s += G(d1)

    d1 = S(d,G(d))
    s += G(d1)

    d1 = S(d,G(d))
    s += G(d1).numpy()

    print(s)

    # -

    # data = gpd.read_postgis('SELECT * FROM http_estaciones WHERE id IN %s',conn,params=[tuple(sitios)])
    # data_eq = gpd.read_postgis('SELECT * FROM terremotos WHERE ogc_fid = %s',conn,params=[eq],geom_col='geog')

    data = gpd.GeoDataFrame(data,
                            geometry= [*map(lambda x:geom.Point(x["long"],
                                                                x["lat"]  ),
                                            data
                                           )
                                      ],
                            crs='EPSG:4326')

    data = data.rename(columns={'estacion':'id'})

    from pyproj import CRS

    # +
    basico = {'proj':'stere',
              'ellips':'GRS80',
              'x_0':0,
              'y_0':0}

    # crs = CRS.from_dict({**basico,'lat_0':data_eq.geog[0].y,'lon_0':data_eq.geog[0].x})
    crs = CRS.from_dict({**basico
                        , 'lat_0':data_eq['latitude']
                        , 'lon_0':data_eq['longitude']})
    # -

    if limites:
        print('Using provided Limits')
        max_lat,max_lon,min_lat,min_lon = [int(i) for i in limites.split()]
    else:
        print('Computing limits')
        max_lon = ceil(max(data.geometry.x))
        min_lon = floor(min(data.geometry.x))
        max_lat = ceil(max(data.geometry.y))
        min_lat = floor(min(data.geometry.y))
        print(max_lat,max_lon,min_lat,min_lon)

    lslong = np.linspace(min_lon,max_lon,100)
    lslat = np.linspace(min_lat,max_lat,100)

    borde1 = [*zip(lslong,[min_lat]*100)]
    borde2 = [*zip(lslong,[max_lat]*100)][::-1]
    borde3 = [*zip([max_lon]*100,lslat)]
    borde4 = [*zip([min_lon]*100,lslat)][::-1]

    paralelos, meridianos = ll_grid(min_lon,max_lon,min_lat,max_lat)

    borde = gpd.GeoSeries([*map(geom.LineString,
        [borde1,borde2,borde3,borde4])],crs='EPSG:4326')

    bordeC = gpd.GeoSeries([geom.Polygon(
        [*borde1,*borde3,*borde2,*borde4])],crs='EPSG:4326')

    borde_merc = borde.to_crs(crs)
    bordeC_merc = bordeC.to_crs(crs)
    data_merc = data.to_crs(crs)

    ref = gpd.GeoDataFrame({'geometry':geom.Point((min_lon,min_lat))},index=[0],crs='EPSG:4326')
    ref = ref.to_crs(crs)
    ref['X'] = ref['geometry'].x
    ref['Y'] = ref['geometry'].y
    ref['U'] = scale
    ref['V'] = 0

    paralelos = paralelos.to_crs(crs)
    meridianos = meridianos.to_crs(crs)

    # +
    geo_df = data_merc.set_index('id')

    geo_df['X'] = geo_df['geometry'].x
    geo_df['Y'] = geo_df['geometry'].y

    l = len(geo_df)

    geo_df['U'] = -s[:,0]
    geo_df['V'] = -s[:,1]

    # -

    # +
    #geo_df = geo_df[geo_df.lon < 180]

    fig,ax = plt.subplots(figsize=(10,10),tight_layout=True)

    ax = geo_df.plot(marker='^',facecolor='none',edgecolor='black',ax=ax,
            markersize=36*factors**2)
    borde_merc.plot(ax=ax)
    #bordeC_merc.plot(ax=ax)

    paralelos.plot(ax=ax,color='k')
    meridianos.plot(ax=ax,color='k')

    ax.scatter([0],[0],[1000*factors**2],marker='*',facecolor='orange',edgecolor='black')

    if scalediff <= 1:
        mask = np.zeros_like(s[:,0]).astype(bool)
    else:
        mask = (s[:,0]**2 + s[:,1]**2 ) ** 0.5 > (2 * scale)
        mask = mask.numpy()

    grandes = geo_df[mask]
    chicas = geo_df[~mask]

    ax.quiver(chicas['X'],
              chicas['Y'],
              chicas['U']*factorl,
              chicas['V']*factorl,
             color='black',scale=scale,units='inches',
             width=factors*0.05/2.5,headwidth=0,headlength=0)

    ax.quiver(grandes['X'],
              grandes['Y'],
              grandes['U']*factorl,
              grandes['V']*factorl,
             color='red',scale=scalediff*scale,units='inches',
             width=factors*0.1/2.5,headwidth=0,headlength=0)

    ax.quiver(ref['X'],
              ref['Y'],
              ref['U']*factorl,
              ref['V']*factorl,
             color='black',scale=scale,units='inches',
             width=factors*0.05/2.5,headwidth=0,headlength=0)

    x0,x1 = ax.get_xlim()
    y0,y1 = ax.get_ylim()
    #ax.set_xlim(x0-6e5,x1)

    source = ctx.providers.Stamen.TonerBackground
    if base:
        ctx.add_basemap(ax,source=source,crs=crs,zoom=mapzoom)
    else:
        lims = [x0-10e5,x1+10e5]
        mlim = sum(lims)/2
        dlim = mlim-lims[0]
        ax.set_xlim(mlim-dlim*factor,mlim+dlim*factor)
        #ax.set_ylim(y0-10e5,y1+10e5)

    plt.savefig(fname)

    fig,axs = plt.subplots(16,9,figsize=(20,20),tight_layout=True)

    muestra=0

    dcved = S(d,G(d))
    dcved = S(dcved,G(dcved))
    dcved = S(dcved,G(dcved))
    dcved = S(dcved,G(dcved))


    fin = False
    for row in axs:
        for i,ax in enumerate(row):
            canal = i % 3
            m = muestra + i // 3
            try:
                ax.plot(d[m,:,canal])
                ax.plot(dcved[m,:,canal])
            except Exception:
                fin = True
                break
            ax.set_xticklabels([])
            ax.set_yticklabels([])

            x,_ = ax.get_xlim()
            y,y2 = ax.get_ylim()
        if fin: break
        muestra +=3

    plt.savefig('S-{}'.format(fname))
    plt.close(fig)

if __name__ == '__main__':
    exit(main())

"""
from dfmodels.tf.fun import apply_step

import matplotlib.pyplot as plt

# +
show_dset = [np.stack(seriesS.values)]

S = apply_step

def gen_plot(fname=None):

    fig,axs = plt.subplots(16,9,figsize=(20,20),tight_layout=True)

    muestra=0

    d = show_dset[0]

    dcved = S(d,G(d))
    dcved = S(dcved,G(dcved))
    dcved = S(dcved,G(dcved))
    dcved = S(dcved,G(dcved))


    for row in axs:
        for i,ax in enumerate(row):
            canal = i % 3
            m = muestra + i // 3
            ax.plot(show_dset[0][m,:,canal])
            ax.plot(dcved[m,:,canal])
            ax.set_xticklabels([])
            ax.set_yticklabels([])

            x,_ = ax.get_xlim()
            y,y2 = ax.get_ylim()

        muestra +=3

    if fname is None:
        plt.show()
    else:
        plt.savefig(fname)
        plt.close(fig)


# -

gen_plot()

import tensorflow as tf


# +
#Define loss functions

def gf(v):
    v = tf.maximum(v,-9.0) # saturar V para evitar errores
    v = tf.minimum(v,21.0) # saturar V para evitar errores

    return v

def fstar(t):
    return tf.math.exp(t-1)

#def gf(v):
#    return tf.sigmoid(v)
#def fstar(t):
#    return t

def disc_loss(fake_output, real_output):

    fo = gf(fake_output)
    ro = gf(real_output)

    return tf.reduce_mean(ro) + tf.reduce_mean(-fstar(fo))

def gen_loss(fake_output):

    fo = gf(fake_output)

    return tf.reduce_mean(-fstar(fo))


# -

data = tf.constant(np.stack(seriesS.values),dtype='float32')

vectors = dcv([np.stack(seriesS.values)]).numpy()[:,:3]
l = data.numpy().shape[0]
steps = tf.Variable(initial_value=vectors,shape=(l,3),dtype='float32')

opt = tf.optimizers.SGD(1e-4)

for i in range(10000):

    data = tf.constant(np.stack(seriesS.values),dtype='float32')
    with tf.GradientTape() as tape:
        modified = apply_step(data,steps)
        discr = discriminator(modified)
        loss = gen_loss(discr[:,0])

    grads = tape.gradient(loss,steps)

    opt.apply_gradients(zip([grads],[steps]))

    print('{:.2f}'.format(loss),end='.')

steps


def mid_to_nan(serie):
    a = serie[:30]
    b = serie[31:]

    salida = np.array([*a,np.nan,*b])
    return salida


# +
show_dset = [np.stack(seriesS.values)]

def gen_plot2(fname=None):

    fig,axs = plt.subplots(16,9,figsize=(20,20),tight_layout=True)

    muestra=0
    dcved = apply_step(show_dset[0],steps)

    for row in axs:
        for i,ax in enumerate(row):
            canal = i % 3
            m = muestra + i // 3
            ax.plot(mid_to_nan(show_dset[0][m,:,canal]))
            ax.plot(mid_to_nan(dcved[m,:,canal]))
            ax.set_xticklabels([])
            ax.set_yticklabels([])

            x,_ = ax.get_xlim()
            y,y2 = ax.get_ylim()

        muestra +=3

    if fname is None:
        plt.show()
    else:
        plt.savefig(fname)
        plt.close(fig)


# -

gen_plot2()

# +
geo_df = data_merc.set_index('id')
geo_df['X'] = geo_df['geometry'].x
geo_df['Y'] = geo_df['geometry'].y

l = len(geo_df)

geo_df['U'] = steps[:,0]
geo_df['V'] = steps[:,1]

# +
geo_df = geo_df[geo_df.lon < 180]
ax = geo_df.plot()


ax.quiver(geo_df['X'],
          geo_df['Y'],
          -geo_df['U'],
          geo_df['V'],
         color='deepskyblue',scale=0.1)

source = ctx.providers.Stamen.Toner

ctx.add_basemap(ax,source=source,zoom=5)
# -

"""
