import sys
from pyarrow import parquet as pq
import matplotlib.pyplot as plt
import matplotlib as mpl
from tensorflow.data.experimental import load as tfload
import numpy as np
import subprocess as sp
import tempfile as tmp
import seaborn as sbs

from matplotlib.lines import Line2D
from matplotlib.collections import PolyCollection

from sklearn.neighbors import KernelDensity

rpt = sys.argv[1]
#ep = int(sys.argv[2])
#lowr = float(sys.argv[3])
#hir = float(sys.argv[4])
#bw = float(sys.argv[5])
try:
    fname = sys.argv[2]
except IndexError:
    fname = ''

ep,lowr,hir,bw = (600, -4, 4.5, 0.25)

defi = 1000

tst = tfload("ds_soft/validation").batch(int(2e6))
l = tst.take(1).get_single_element()[1][:, 0].numpy().astype(int)


def preprocess_s(S):
    S['label'] = l
    data = S.drop(columns=['steps']).applymap(lambda x: x[0] if not isinstance(x,int) else x).set_index('label').unstack().reset_index()

    data = data.rename(columns={0:'data'})

    data['origin'] = data.T.apply(lambda x: x.level_0[0])
    data['out'] = data.T.apply(lambda x: x.level_0[-1])
    data['L-orig'] = data.T.apply(lambda x: '{}{}'.format(x.label,x.origin))

    #data = data.loc[data['L-orig'] != '1g']
    #data = data.loc[data['L-orig'] != '1d']

    data_o = data[data.out == 'o']
    data_l = data[data.out == 'l']

    return data_o, data_l

plt.rcParams['mathtext.fontset'] = 'custom'
plt.rcParams['mathtext.it'] = 'Montserrat:italic'
plt.rcParams['mathtext.rm'] = 'Montserrat'
plt.rcParams['mathtext.sf'] = 'Montserrat'
plt.rcParams['mathtext.cal'] = 'jsMath\-cmsy10'

label_dic={'1g':'$g_\\theta(X|Y^\\star=1)$',
           '0g':'$g_\\theta(X|Y^\\star=0)$',
           '1d':'$X|Y^\\star=1$',
           '0d':'$X|Y^\\star=0$'}

cm = mpl.colormaps.get('viridis')
c= [
    '#'+bytearray([*map(lambda x:int(x*255),cm(0.1)[:3])]).hex().upper(),
    '#'+bytearray([*map(lambda x:int(x*255),cm(0.5)[:3])]).hex().upper(),
    '#'+bytearray([*map(lambda x:int(x*255),cm(0.9)[:3])]).hex().upper()
    ]

if True:
    ep=50
    S = pq.read_table(f"{rpt}/v{ep:d}.pq").to_pandas()

    datao, data = preprocess_s(S)


    fig,(ax1,ax2,ax3,ax4) = plt.subplots(4,tight_layout=True,
            figsize=(9/2.54,10/2.54),gridspec_kw={'height_ratios': [1, 1, 4, 4]})

    X = data[data['L-orig']=='1d'].data.values[:,np.newaxis]
    X_plot = np.linspace(lowr,hir,1000)[:,np.newaxis]

    ax1.plot(X[:, 0], np.random.rand(X.shape[0]) * -0.03 - 0.01, '+',color=f"{c[0]}")

    X = data[data['L-orig']=='1g'].data.values[:,np.newaxis]

    ax1.plot(X[:, 0], np.random.rand(X.shape[0]) * -0.03 - 0.05, '+',color=f"{c[1]}")

    X = data[data['L-orig']=='0d'].data.values[:,np.newaxis]

    ax1.plot(X[:, 0], np.random.rand(X.shape[0]) * -0.03 - 0.09, '+',color=f"{c[2]}")

    #X = data[data['L-orig']=='0g'].data.values[:,np.newaxis]

    #ax1.plot(X[:, 0], np.random.rand(X.shape[0]) * -0.03 - 0.12, 'x',color=f"{c[3]}")


if True:
    ep=100
    S = pq.read_table(f"{rpt}/v{ep:d}.pq").to_pandas()

    datao, data = preprocess_s(S)


    X = data[data['L-orig']=='1d'].data.values[:,np.newaxis]
    X_plot = np.linspace(lowr,hir,1000)[:,np.newaxis]

    ax2.plot(X[:, 0], np.random.rand(X.shape[0]) * -0.03 - 0.01, '+',color=f"{c[0]}")

    X = data[data['L-orig']=='1g'].data.values[:,np.newaxis]

    ax2.plot(X[:, 0], np.random.rand(X.shape[0]) * -0.03 - 0.05, '+',color=f"{c[1]}")

    X = data[data['L-orig']=='0d'].data.values[:,np.newaxis]

    ax2.plot(X[:, 0], np.random.rand(X.shape[0]) * -0.03 - 0.09, '+',color=f"{c[2]}")

    #X = data[data['L-orig']=='0g'].data.values[:,np.newaxis]

    #ax2.plot(X[:, 0], np.random.rand(X.shape[0]) * -0.03 - 0.12, 'x',color=f"{c[3]}")

if True:
    ep=300
    S = pq.read_table(f"{rpt}/v{ep:d}.pq").to_pandas()

    datao, data = preprocess_s(S)

    X = data[data['L-orig']=='1d'].data.values[:,np.newaxis]
    X_plot = np.linspace(lowr,hir,1000)[:,np.newaxis]

    # Gaussian KDE
    kde = KernelDensity(kernel="epanechnikov", bandwidth=bw).fit(X)
    log_dens = kde.score_samples(X_plot)

    ax3.fill_between(X_plot[:, 0], np.exp(log_dens)/np.exp(max(log_dens)), fc=f"{c[0]}44")
    ax3.plot(X_plot[:, 0], np.exp(log_dens)/np.exp(max(log_dens)), color=f"{c[0]}")

    ax3.plot(X[:, 0], np.random.rand(X.shape[0]) * -0.03 - 0.01, '+',color=f"{c[0]}")

    X = data[data['L-orig']=='1g'].data.values[:,np.newaxis]
    # Gaussian KDE
    kde = KernelDensity(kernel="epanechnikov", bandwidth=bw).fit(X)
    log_dens = kde.score_samples(X_plot)
    ax3.fill_between(X_plot[:, 0], np.exp(log_dens)/np.exp(max(log_dens)), fc=f"{c[1]}44")
    ax3.plot(X_plot[:, 0], np.exp(log_dens)/np.exp(max(log_dens)),linestyle='--', color=f"{c[1]}")

    ax3.plot(X[:, 0], np.random.rand(X.shape[0]) * -0.03 - 0.05, '+',color=f"{c[1]}")

    X = data[data['L-orig']=='0d'].data.values[:,np.newaxis]
    # gaussian kde
    kde = KernelDensity(kernel="epanechnikov", bandwidth=bw).fit(X)
    log_dens = kde.score_samples(X_plot)
    ax3.fill_between(X_plot[:, 0], np.exp(log_dens)/np.exp(max(log_dens)), fc=f"{c[2]}44")
    ax3.plot(X_plot[:, 0], np.exp(log_dens)/np.exp(max(log_dens)), color=f"{c[2]}")

    ax3.plot(X[:, 0], np.random.rand(X.shape[0]) * -0.03 - 0.09, '+',color=f"{c[2]}")

    #X = data[data['L-orig']=='0g'].data.values[:,np.newaxis]

    ## gaussian kde
    #kde = KernelDensity(kernel="epanechnikov", bandwidth=bw).fit(X)
    #log_dens = kde.score_samples(X_plot)
    #ax3.fill_between(X_plot[:, 0], np.exp(log_dens)/np.exp(max(log_dens)), fc=f"{c[3]}44")
    #ax3.plot(X_plot[:, 0], np.exp(log_dens)/np.exp(max(log_dens)), color=f"{c[3]}")

    #ax3.plot(X[:, 0], np.random.rand(X.shape[0]) * -0.03 - 0.12, 'x',color=f"{c[3]}")

ep,lowr,hir,bw = (600, -4, 15, 0.5)

if True:
    ep=600
    S = pq.read_table(f"{rpt}/v{ep:d}.pq").to_pandas()

    datao, data = preprocess_s(S)


    X = data[data['L-orig']=='1d'].data.values[:,np.newaxis]
    X_plot = np.linspace(lowr,hir,1000)[:,np.newaxis]

    # Gaussian KDE
    kde = KernelDensity(kernel="epanechnikov", bandwidth=bw).fit(X)
    log_dens = kde.score_samples(X_plot)

    ax4.fill_between(X_plot[:, 0], np.exp(log_dens)/np.exp(max(log_dens)), fc=f"{c[0]}44")
    ax4.plot(X_plot[:, 0], np.exp(log_dens)/np.exp(max(log_dens)), color=f"{c[0]}")

    ax4.plot(X[:, 0], np.random.rand(X.shape[0]) * -0.03 - 0.01, '+',color=f"{c[0]}")

    X = data[data['L-orig']=='1g'].data.values[:,np.newaxis]
    # Gaussian KDE
    kde = KernelDensity(kernel="epanechnikov", bandwidth=bw).fit(X)
    log_dens = kde.score_samples(X_plot)
    ax4.fill_between(X_plot[:, 0], np.exp(log_dens)/np.exp(max(log_dens)), fc=f"{c[1]}44")
    ax4.plot(X_plot[:, 0], np.exp(log_dens)/np.exp(max(log_dens)),linestyle='--', color=f"{c[1]}")

    ax4.plot(X[:, 0], np.random.rand(X.shape[0]) * -0.03 - 0.05, '+',color=f"{c[1]}")

    X = data[data['L-orig']=='0d'].data.values[:,np.newaxis]
    # gaussian kde
    kde = KernelDensity(kernel="epanechnikov", bandwidth=bw).fit(X)
    log_dens = kde.score_samples(X_plot)
    ax4.fill_between(X_plot[:, 0], np.exp(log_dens)/np.exp(max(log_dens)), fc=f"{c[2]}44")
    ax4.plot(X_plot[:, 0], np.exp(log_dens)/np.exp(max(log_dens)), color=f"{c[2]}")

    ax4.plot(X[:, 0], np.random.rand(X.shape[0]) * -0.03 - 0.09, '+',color=f"{c[2]}")

    X = data[data['L-orig']=='0g'].data.values[:,np.newaxis]

    ## gaussian kde
    #kde = KernelDensity(kernel="epanechnikov", bandwidth=bw).fit(X)
    #log_dens = kde.score_samples(X_plot)
    #ax4.fill_between(X_plot[:, 0], np.exp(log_dens)/np.exp(max(log_dens)), fc=f"{c[3]}44")
    #ax4.plot(X_plot[:, 0], np.exp(log_dens)/np.exp(max(log_dens)), color=f"{c[3]}")

    #ax4.plot(X[:, 0], np.random.rand(X.shape[0]) * -0.03 - 0.12, 'x',color=f"{c[3]}")

    #ax1.invert_xaxis()
    #ax2.invert_xaxis()
    #ax3.invert_xaxis()
    #ax4.invert_xaxis()

    if fname:
        plt.savefig(fname,dpi=500)
    plt.show()
