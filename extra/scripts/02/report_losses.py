# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.13.5
#   kernelspec:
#     display_name: Python 3 (ipykernel)
#     language: python
#     name: python3
# ---

import sys

rpt = sys.argv[1]

import matplotlib.pyplot as plt
import json

with open(f'{rpt}/l-{rpt[2:]}','r') as f:
    data = json.load(f)

losses = data

fig, ((ax1,ax2),(ax3,ax4),(ax5,_)) = plt.subplots(3,2)
# +
ax1.plot([i[0][0] for i in losses],'x',label='TR-Do',color='gray')
ax1.plot([i[1][0] for i in losses],'.',label='TS-Do',color='k')
#plt.ylim(-1,0)
ax1.legend()

ax2.plot([i[0][1] for i in losses],'x',label='TR-Go',color='gray')
ax2.plot([i[1][1] for i in losses],'.',label='TS-Go',color='k')
#plt.ylim(-1,0)

ax2.legend()

ax3.plot([i[0][2] for i in losses],'x',label='TR-Dl',color='gray')
ax3.plot([i[1][2] for i in losses],'.',label='TS-Dl',color='k')
#plt.ylim(-1,0)
ax3.legend()

ax4.plot([i[0][3] for i in losses],'x',label='TR-Gl',color='gray')
ax4.plot([i[1][3] for i in losses],'.',label='TS-Gl',color='k')
#plt.ylim(-1,0)
ax4.legend()

ax5.plot([i[0][4] for i in losses],'x',label='TR-Regularization',color='gray')
ax5.plot([i[1][4] for i in losses],'.',label='TS-Regularization',color='k')
#plt.ylim(-1,0)
ax5.legend()

plt.savefig('losses.svg')
# -


