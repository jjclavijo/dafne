# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.13.5
#   kernelspec:
#     display_name: Python 3 (ipykernel)
#     language: python
#     name: python3
# ---

# # Variante 2: cGAN

# +
# #!/opt/venv/bin/python -m pip install pydot matplotlib
# #!apt-get -y -qq install graphviz

# +
import matplotlib.pyplot as plt
import time
import numpy as np
from sklearn.ensemble import BaggingClassifier, RandomForestClassifier
from sklearn.multiclass import OneVsRestClassifier
from sklearn.svm import SVC

from sklearn.metrics import confusion_matrix
# -

BUFFER = int(2e6)
BS = int(2**14)

# +
from dafnedset.presets_simple import pos10,neg10
import dafnedset.tf.glue as glue
from dafnedset import fun_ops as fop
from dafnedset import fun_transformations as ftr

# load test dataset

todo_tst = pos10.parts[1] + neg10.parts[1]
dset_tst = glue.dafneFbToTf(todo_tst).cache().batch(BUFFER)

# +
# Load and augment train dataset

algo = pos10.parts[0] * 18 #+ neg10.parts[0] * 4

algo.options.batch_size = 1e5
algo = fop.map(algo,ftr.channel_mix_w_relabel(p_true_pos=1,p_true_pos_ch=1))
aumenta = fop.map(algo,ftr.random_rot_dir())

algo = neg10.parts[0] * 18
aumenta += fop.map(algo,ftr.random_rot_dir())

algo = pos10.parts[0] * 6 + neg10.parts[0] * 12
algo.options.batch_size = 1e5
algo = fop.map(algo,ftr.channel_mix_w_relabel(p_true_pos=1,p_true_pos_ch=1))
aumenta += fop.map(aumenta,ftr.random_rot_dir())

todo = (pos10.parts[0] + neg10.parts[0])
todo = todo + aumenta

todo.options.columns = ['este','norte','altura','etiqueta']

dset = glue.dafneFbToTf(todo).cache().shuffle(BUFFER).batch(BS)

# +
p = pos10.parts[3]

show_dset = glue.dafneFbToTf(p).batch(BS).take(1).get_single_element()
# -

d,l = dset.take(1).get_single_element()

# +
#_ = !/opt/venv/bin/python -m pip install sklearn
# -

data = d.numpy().reshape(-1,61*3)
target = l.numpy()[:,0].astype(int)

data = np.nan_to_num(data)

# +

#iris = datasets.load_iris()
X, y = data, target

n_estimators = 10
start = time.time()
clf = OneVsRestClassifier(BaggingClassifier(SVC(kernel='rbf', probability=True, class_weight='balanced'), max_samples=1.0 / n_estimators, n_estimators=n_estimators))
clf.fit(X, y)
end = time.time()
print("Bagging SVC", end - start, clf.score(X,y))
proba = clf.predict_proba(X)

#start = time.time()
#clf = RandomForestClassifier(min_samples_leaf=20)
#clf.fit(X, y)
#end = time.time()
#print("Random Forest", end - start, clf.score(X,y))
#proba = clf.predict_proba(X)
# -

d,l = dset_tst.take(1).get_single_element()

# +
#_ = !/opt/venv/bin/python -m pip install sklearn
# -

data = d.numpy().reshape(-1,61*3)
target = l.numpy()[:,0].astype(int)

data = np.nan_to_num(data)

clf.score(data,target)

confusion_matrix(clf.predict(data),target)



# + tags=[]
import dfmodels.models.model1 as model
# -

D = model.discrim()
G = model.deceiv()

checkpoint='ckpts/0.ckpt'
D.load_weights(checkpoint)
checkpoint='ckptsDi/0.ckpt'
G.load_weights(checkpoint)

d,l = dset_tst.take(1).get_single_element()

# +
#_ = !/opt/venv/bin/python -m pip install sklearn
# -

from dfmodels.tf.fun import apply_step as S

d = S(d,G(d))
d = S(d,G(d))

data = d.numpy().reshape(-1,61*3)
target = l.numpy()[:,0].astype(int)

data = np.nan_to_num(data)

clf.score(data,target)

confusion_matrix(clf.predict(data),target)



# + tags=[]
import dfmodels.models.model1noise as modelN
# -

Dn = modelN.discrim()
Gn = modelN.deceiv()

import tensorflow as tf
def Gn_(x,**kwargs):
    return Gn([x,tf.random.uniform([tf.shape(x)[0],3],-1,1)],**kwargs)



checkpoint='ckpts/1.ckpt'
Gn.load_weights(checkpoint)
checkpoint='ckptsDi/1.ckpt'
Dn.load_weights(checkpoint)

d,l = dset_tst.take(1).get_single_element()

# +
#_ = !/opt/venv/bin/python -m pip install sklearn
# -

from dfmodels.tf.fun import apply_step as S

d = S(d,Gn_(d))

data = d.numpy().reshape(-1,61*3)
target = l.numpy()[:,0].astype(int)

data = np.nan_to_num(data)

clf.score(data,target)

confusion_matrix(clf.predict(data),target)



# +
#Define loss functions

def gf(v):
    v = tf.maximum(v,-9.0) # saturar V para evitar errores
    v = tf.minimum(v,21.0) # saturar V para evitar errores

    return v

def fstar(t):
    return 1/4 * t**2 + t

#def gf(v):
#    return tf.sigmoid(v)
#def fstar(t):
#    return t

def d_loss(fake_output, real_output):

    fo = gf(fake_output)
    ro = gf(real_output)

    return tf.reduce_mean(ro) + tf.reduce_mean(-fstar(fo))

def g_loss(fake_output):

    fo = gf(fake_output)

    return tf.reduce_mean(-fstar(fo))


# -

d,l = dset_tst.take(1).get_single_element()

data = d #tf.constant(np.stack(seriesS.values),dtype='float32')

#vectors = dcv([np.stack(seriesS.values)]).numpy()[:,:3]
vectors = G(d)
le = data.numpy().shape[0]
steps = tf.Variable(initial_value=tf.zeros_like(vectors),shape=(le,3),dtype='float32')

opt = tf.optimizers.Adam(1e-4)

for i in range(500):

    data = d#tf.constant(np.stack(seriesS.values),dtype='float32')
    with tf.GradientTape() as tape:
        modified = S(d,steps)
        discr = D(modified)
        loss = gen_loss(discr[:,0])

    grads = tape.gradient(loss,steps)

    opt.apply_gradients(zip([grads],[steps]))

    print('{:.2f}'.format(loss),end='.')

d = S(d,steps)
#d = S(d,G(d))

data = d.numpy().reshape(-1,61*3)
target = l.numpy()[:,0].astype(int)

data = np.nan_to_num(data)

clf.score(data,target)

confusion_matrix(clf.predict(data),target)

d = S(d,G(d))
data = d.numpy().reshape(-1,61*3)
data = np.nan_to_num(data)
clf.score(data,target)

confusion_matrix(clf.predict(data),target)

d,l = dset_tst.take(1).get_single_element()



def fun(a):
    ((a,b),(c,dd))=confusion_matrix((D(d)< a),target)
    return -(a+dd)/(a+b+c+dd)


x = np.linspace(1,2,100)
plt.plot(x,[*map(fun,x)])

confusion_matrix((D(d)<1.81),target)

# +
((a,b),(c,dd))=confusion_matrix((D(d)< 1.81),target)

(a+dd)/(a+b+c+dd)

# +
d1 = S(d,Gn_(d))
d1 = S(d1,Gn_(d1))
d1 = S(d1,Gn_(d1))
d1 = S(d1,Gn_(d1))
d1 = S(d1,Gn_(d1))
d1 = S(d1,Gn_(d1))
d1 = S(d1,Gn_(d1))
d1 = S(d1,Gn_(d1))
d1 = S(d1,Gn_(d1))
d1 = S(d1,Gn_(d1))

confusion_matrix((D(d1) <1.81),target)
# -

data = d1.numpy().reshape(-1,61*3)
data = np.nan_to_num(data)
clf.score(data,target)

confusion_matrix(clf.predict(data),target)



d,l = dset_tst.take(1).get_single_element()


def fun(a):
    ((a,b),(c,dd))=confusion_matrix((Dn(d)< a),target)
    return -(a+dd)/(a+b+c+dd)


x = np.linspace(0,2,100)
plt.plot(x,[*map(fun,x)])

confusion_matrix((Dn(d)<1.12),target)

# +
((a,b),(c,dd))=confusion_matrix((D(d)< 1.75),target)

(a+dd)/(a+b+c+dd)

# +
d1 = S(d,G(d))
d1 = S(d1,G(d1))
d1 = S(d1,G(d1))
d1 = S(d1,G(d1))
d1 = S(d1,G(d1))
d1 = S(d1,G(d1))
d1 = S(d1,G(d1))
d1 = S(d1,G(d1))
d1 = S(d1,G(d1))
d1 = S(d1,G(d1))

confusion_matrix((D(d1) <1.81),target)
# -


