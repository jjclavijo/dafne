# Training snapshots

The snapshots of the models and its evaluation, saved during trained, are saved
with the following naming scheme:

- `r-...` folder have apache parquet files with the results of evaluating the models for each epcoch `---`  on the train dataset(`---.pq` files), the test dataset (`t---.pq` files) and the validation dataset (`v---.pq` files)
- The `r-.../l-...` file has information on the loss functions during all the training process
- Checkpoints of the discriminator model are saved in the `c-...-D` folder
- Checkpoints of the generator model are saved in the `c-...-G` folder

When the `config.json` file is generated using the provided script, the `...`
part of the name is generated with the first 8 characters of a hash of the
configuration options, to garantee uniqueness of the names for
crossvalidation runs.

# Map reports

Maps are generated from json files, trained models are needed, when running on
containers, the models used for the paper can be mounted as volumes:

```
podman run -d --rm\
           --mount 'type=volume,src=modelG,dst=/trained/01/c-663026d5-G,ro=true' \
           --mount 'type=volume,src=modelD,dst=/trained/01/c-663026d5-D,ro=true' \
           dafne/data:artifacts /bin/sleep 1

podman run --rm --pod dafne \
          -v $(realpath .):/wdir -w=/wdir \
          --mount 'type=volume,src=modelG,dst=/wdir/c-663026d5-G,ro=true' \
          --mount 'type=volume,src=modelD,dst=/wdir/c-663026d5-D,ro=true' \
          dafne/models:latest 00-Map_event.py \
          --rutaD c-663026d5-D/0999.ckpt --rutaG c-663026d5-G/0999.ckpt
          --fname output_map.svg --base=False \
          --scale ... --scalediff ... --factorl ... --factors ... \
          --jsonfile <JSON event file>
```

`c-...-D/G` folders are generated during training, containing checkpoints. In
the above example we used the last checkpoint, numbered as 0999 because the
epochs are 0-indexed.

If the flag `--base` is set to `True`, the resulting `.svg` includes a basemap.
To prepare the publication maps colors were further alterated using imagemagik.

```
grep -a1 image\/png output_map.svg |\
     sed 's/.*href=\"\(.*\)/\1/;s/\" .*\/>$//' |\
     magick inline:- -fuzz 5% \
                     -fill '#d5e9c6' -opaque white \
                     -fill '#add8e6' -opaque black inline:- | sed 's/.*base64,//' > outputmap_colorimg.base64
sed '/data:image/{
r outputmap_colorimg.base64
n
s/[^\"]*\"/\"/
}' output_map.svg > output_map_color.svg
```

This script is now included inline in the python script, but requires to have imagemagick installed.

The event json files can be generated with the `query/data` container image.
Note that this container querys the database container, which needs to be runinng
in the same pod. Files for the published figures are provided as `evt....json`

```
podman run --rm --pod dafne \
           dafne/query:data-20.01 \
           -e <event identifyer> --dist <distance in m> --days 30 > evt1693.json
```

The event identifyer is the `id` field on the geojson file downloaded from usgs.

## Parameters for maps on the publication

Muisne:

```
# podman run ... dafne/query:data-20.01
-e us20005j32 --dist 1000000 --days 30
```

```
# podman run ... dafne/models 00-Map_event.py
--rutaD c-663026d5-D/0999.ckpt --rutaG c-663026d5-G/0999.ckpt
--fname Muisne.svg --base=True
--scale 0.01 --scalediff 1 --factors 1.5
--jsonfile evtMuisne.json
```

Iquique:

```
# podman run ... dafne/query:data-20.01
-e usc000nzvd --dist 750000
```

```
# podman run ... dafne/models 00-Map_event.py
--rutaD c-663026d5-D/0999.ckpt --rutaG c-663026d5-G/0999.ckpt
--fname Iquique.svg --base=True
--scale 0.05 --scalediff 5  -factors 0.87
--jsonfile evtIquique.json
```

Maule:

```
# podman run dafne/query:data-20.01
-e official20100227063411530_30 --dist 1500000 --days 30
```

```
# podman run ... dafne/models 00-Map_event.py
--rutaD c-663026d5-D/0999.ckpt --rutaG c-663026d5-G/0999.ckpt
--fname Maule.svg
--base=True
--scale 0.2 --scalediff 10 --factors 0.678
--json evtMaule.json
```

The scale parameters refers to the scale of the vectors, the factors
parameter is a multiplier to the scales of the plot and the symbols.
The black arrow on the lower left corner of the frame represents 1 `scale` offset,
red arrows represents offsets `scalediff` times bigger

# Boxplot of Histograms, Figs 3 and 13.

The boxpotes.py script is feed with a folder with snapshots taken over a selected dataset during training. For the paper a snapshot was captured each training epoch. (parameter REPORT_PERIOD:1, default is 10)

Figure 3 is generated with:
```
python boxplotes.py r-663026d5 50 100 100 150 950 1000
```

Figure 13 with:
```
python boxplotes.py r-56ab2a35 150 200 300 350 700 750
```

# 2d histograms of Suppelementary figures (comparing with average based estimator).

```
python one_meanplot.py r-663026d5 999 EN fig4.svg
python one_meanplot.py r-663026d5 999 U fig5.svg
python one_meanplot.py r-f867f4fb 999 U fig11.svg
```

Comparison with Hector estimations were made with two scripts:
    - One that computes Hector Estimations.
    - One that creates the plots.

The reason for this is that the Hector estimation process is rather slow.

# Density plots of discriminator output, Figs 6 and 14

```
python vp_hist.py r-663026d5 fig6.svg
python vp_hist.py r-56ab2a35 fig14.svg
```

# Visualize Show Dataset Figure 10

```
python report_saltos_escala r-663026d5 999
```
