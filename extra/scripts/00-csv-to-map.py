# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.13.5
#   kernelspec:
#     display_name: Python 3 (ipykernel)
#     language: python
#     name: python3
# ---

import click

### Parametros

# tipo Variante del modelo
# rutaD ruta a los checkpoints
# rutaG
# eq = 4830
# dist = 1500
# fname


def ll_grid(*limits):

    import shapely.geometry as geom
    from math import ceil,floor
    import numpy as np
    import geopandas as gpd

    min_lon, max_lon, min_lat, max_lat = limits

    max_lon = floor(max_lon/5)*5
    min_lon = ceil(min_lon/5)*5
    max_lat = floor(max_lat/5)*5
    min_lat = ceil(min_lat/5)*5

    mer_lon = np.arange(min_lon,max_lon+1,5)
    mer_lat = np.linspace(min_lat-5,max_lat+5,100)

    par_lat = np.arange(min_lat,max_lat+1,5)
    par_lon = np.linspace(min_lon-5,max_lon+5,100)

    Mlo,Mla = np.meshgrid(mer_lon,mer_lat)
    Plo,Pla = np.meshgrid(par_lon,par_lat)

    merid = [*zip(Mlo.T,Mla.T)]
    par = [*zip(Plo,Pla)]

    geom_m = [*map(geom.LineString,[[*zip(*i)] for i in merid])]
    geom_p = [*map(geom.LineString,[[*zip(*i)] for i in par])]

    paralelos = gpd.GeoSeries(geom_p, crs='EPSG:4326')
    meridianos = gpd.GeoSeries(geom_m, crs='EPSG:4326')

    return paralelos,meridianos


@click.command()
@click.option('--tipo',default=1)
@click.option('--eq',type=str)
@click.option('--csv',default='')
@click.option('--fname',default='out.png')
@click.option('--scale',default=0.1)
@click.option('--base',default=False)
@click.option('--scalediff',default=5)
@click.option('--mapzoom',default=5)
@click.option('--limites',default='')
@click.option('--factor',default=1.0)
@click.option('--factorl',default=1.0)
@click.option('--factors',default=1.0)

def main(tipo,eq,csv,fname,scale,base,scalediff,mapzoom,limites,factor,factorl,factors):
    import psycopg2 as pg
    import pandas as pd
    import geopandas as gpd
    import numpy as np
    import json
    import contextily as ctx
    import subprocess as sp
    import matplotlib.pyplot as plt
    import shapely.geometry as geom
    from math import ceil,floor
    from pyproj import CRS

    conn = pg.connect(host='localhost',user='postgres',database='sismoident')

    data = pd.read_csv(csv,delimiter='[,;\s]+',engine='python')
    data = gpd.GeoDataFrame(data,
            geometry=data.T.apply(
                lambda x:geom.Point(x.Longitude,x.Latitude)),crs='EPSG:4326')

    # sitios = jdata['puntos']
    # series = jdata['instances']

    data_eq = gpd.read_postgis('SELECT * FROM terremotos WHERE ogc_fid = %s',conn,params=[eq],geom_col='geog')

    # +
    basico = {'proj':'stere',
              'ellips':'GRS80',
              'x_0':0,
              'y_0':0}

    crs = CRS.from_dict({**basico,'lat_0':data_eq.geog[0].y,'lon_0':data_eq.geog[0].x})
    # -


    if limites:
        print('pasé por acá')
        max_lat,max_lon,min_lat,min_lon = [int(i) for i in limites.split()]
    else:
        print('no pasé por acá')
        max_lon = ceil(max(data.geometry.x))
        min_lon = floor(min(data.geometry.x))
        max_lat = ceil(max(data.geometry.y))
        min_lat = floor(min(data.geometry.y))

    lslong = np.linspace(min_lon,max_lon,100)
    lslat = np.linspace(min_lat,max_lat,100)

    print(max_lat,max_lon,min_lat,min_lon)
    borde1 = [*zip(lslong,[min_lat]*100)]
    borde2 = [*zip(lslong,[max_lat]*100)][::-1]
    borde3 = [*zip([max_lon]*100,lslat)]
    borde4 = [*zip([min_lon]*100,lslat)][::-1]

    paralelos, meridianos = ll_grid(min_lon,max_lon,min_lat,max_lat)

    borde = gpd.GeoSeries([*map(geom.LineString,
        [borde1,borde2,borde3,borde4])],crs='EPSG:4326')

    bordeC = gpd.GeoSeries([geom.Polygon(
        [*borde1,*borde3,*borde2,*borde4])],crs='EPSG:4326')

    borde_merc = borde.to_crs(crs)
    bordeC_merc = bordeC.to_crs(crs)


    data_merc = data.loc[
                  data.geometry.map(bordeC[0].contains)
                  ].to_crs(crs)

    ref = gpd.GeoDataFrame({'geometry':geom.Point((min_lon,min_lat))},index=[0],crs='EPSG:4326')
    ref = ref.to_crs(crs)
    ref['X'] = ref['geometry'].x
    ref['Y'] = ref['geometry'].y
    ref['U'] = scale
    ref['V'] = 0

    paralelos = paralelos.to_crs(crs)
    meridianos = meridianos.to_crs(crs)

    # +
    geo_df = data_merc[data_merc.isin]#.set_index('id')

    geo_df['X'] = geo_df['geometry'].x
    geo_df['Y'] = geo_df['geometry'].y

    l = len(geo_df)

    geo_df = geo_df.rename(columns={'EW(m)':'U','NS(m)':'V'})

    #geo_df['U'] = -s[:,0]
    #geo_df['V'] = -s[:,1]

    # -

    # +
    #geo_df = geo_df[geo_df.lon < 180]

    fig,ax = plt.subplots(figsize=(10,10),tight_layout=True)

    ax = geo_df.plot(marker='^',facecolor='none',edgecolor='black',ax=ax,markersize=36*factors**2)
    borde_merc.plot(ax=ax)
    bordeC_merc.plot(ax=ax)

    paralelos.plot(ax=ax,color='k')
    meridianos.plot(ax=ax,color='k')

    ax.scatter([0],[0],[1000*factors**2],marker='*',facecolor='orange',edgecolor='black')

    mask = (geo_df['U']**2 + geo_df['V']**2 ) ** 0.5 > (2 * scale)

    grandes = geo_df[mask.values]
    chicas = geo_df[~mask.values]

    ax.quiver(chicas['X'],
              chicas['Y'],
              chicas['U']*factorl,
              chicas['V']*factorl,
             color='black',scale=scale,units='inches',
             width=factors*0.05/2.5,headwidth=0,headlength=0)

    ax.quiver(grandes['X'],
              grandes['Y'],
              grandes['U']*factorl,
              grandes['V']*factorl,
             color='red',scale=scalediff*scale,units='inches',
             width=factors*0.1/2.5,headwidth=0,headlength=0)

    ax.quiver(ref['X'],
              ref['Y'],
              ref['U']*factorl,
              ref['V']*factorl,
             color='black',scale=scale,units='inches',
             width=factors*0.05/2.5,headwidth=0,headlength=0)

    x0,x1 = ax.get_xlim()
    y0,y1 = ax.get_ylim()

    #ax.set_xlim(x0-6e5,x1)

    source = ctx.providers.Stamen.TonerBackground
    if base:
        ctx.add_basemap(ax,source=source,crs=crs,zoom=mapzoom)
    else:
        lims = [x0-10e5,x1+10e5]
        mlim = sum(lims)/2
        dlim = mlim-lims[0]
        ax.set_xlim(mlim-dlim*factor,mlim+dlim*factor)
        #ax.set_ylim(y0-10e5,y1+10e5)

    plt.savefig(fname)

if __name__ == '__main__':
    exit(main())

