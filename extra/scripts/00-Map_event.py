
# Hide Tensorflow Annoing warnings and info mesages.
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

from tensorflow.compat.v1 import logging as tflogging
tflogging.set_verbosity(tflogging.ERROR)

import click

import pandas as pd
import geopandas as gpd
import numpy as np
import json
import contextily as ctx
import matplotlib.pyplot as plt
from dfmodels.tf.fun import apply_step as S
import shapely.geometry as geom
from math import ceil, floor
from pyproj import CRS

import subprocess as sp

colorize_bash_script = """
#!/bin/bash

grep -a1 image\/png $1 |\\
     sed 's/.*href=\\"\(.*\)/\\1/;s/\\" .*\/>$//' |\\
     magick inline:- -fuzz 5% \\
                     -fill '#d5e9c6' -opaque white \\
                     -fill '#add8e6' -opaque black inline:- | sed 's/.*base64,//' > colorimg.base64
sed '/data:image/{
r colorimg.base64
n
s/[^\\"]*\\"/\\"/
}' $1 > ${1%.*}_color.svg
rm colorimg.base64
"""


### Parametros


def loadModel(tipo, rutaG):
    if tipo == 1:
        import dfmodels.models.model1 as model
    if tipo == 2:
        import dfmodels.models.model2labels as model
    if tipo == 25:
        import dfmodels.models.model2 as model
    if tipo == 3:
        import dfmodels.models.model3 as model

    G = model.deceiv()
    G.load_weights(rutaG)

    return G


def ll_grid(*limits):

    import shapely.geometry as geom
    from math import ceil, floor
    import numpy as np
    import geopandas as gpd

    min_lon, max_lon, min_lat, max_lat = limits

    max_lon = floor(max_lon / 5) * 5
    min_lon = ceil(min_lon / 5) * 5
    max_lat = floor(max_lat / 5) * 5
    min_lat = ceil(min_lat / 5) * 5

    mer_lon = np.arange(min_lon, max_lon + 1, 5)
    mer_lat = np.linspace(min_lat - 5, max_lat + 5, 100)

    par_lat = np.arange(min_lat, max_lat + 1, 5)
    par_lon = np.linspace(min_lon - 5, max_lon + 5, 100)

    Mlo, Mla = np.meshgrid(mer_lon, mer_lat)
    Plo, Pla = np.meshgrid(par_lon, par_lat)

    merid = [*zip(Mlo.T, Mla.T)]
    par = [*zip(Plo, Pla)]

    geom_m = [*map(geom.LineString, [[*zip(*i)] for i in merid])]
    geom_p = [*map(geom.LineString, [[*zip(*i)] for i in par])]

    paralelos = gpd.GeoSeries(geom_p, crs="EPSG:4326")
    meridianos = gpd.GeoSeries(geom_m, crs="EPSG:4326")

    return paralelos, meridianos


def recapply(model, datos, N):
    from dfmodels.tf.fun import apply_step as S

    a_hat = model(datos)
    for i in range(N):
        a_hat += model(S(datos, a_hat))
    return a_hat.numpy()


def gen_map(data,s=None,crs_extra_params={},
             limites="",factorl=1.0,factors=1.0,base=False,mapzoom=5,
            scale=0.05,scalediff=5,epicenter=False,fname='out.png'):

    basico = {"proj": "stere", "ellips": "GRS80", "x_0": 0, "y_0": 0}

    crs = CRS.from_dict({**basico, **crs_extra_params})

    if limites:
        print("Using provided Limits")
        max_lat, max_lon, min_lat, min_lon = [int(i) for i in limites.split()]
    else:
        print("Computing limits")
        max_lon = ceil(max(data.geometry.x))
        min_lon = floor(min(data.geometry.x))
        max_lat = ceil(max(data.geometry.y))
        min_lat = floor(min(data.geometry.y))
        print('Using Limits:',max_lat, max_lon, min_lat, min_lon)

    # Compute borders
    lslong = np.linspace(min_lon, max_lon, 100)
    lslat = np.linspace(min_lat, max_lat, 100)

    borde1 = [*zip(lslong, [min_lat] * 100)]
    borde2 = [*zip(lslong, [max_lat] * 100)][::-1]
    borde3 = [*zip([max_lon] * 100, lslat)]
    borde4 = [*zip([min_lon] * 100, lslat)][::-1]

    borde = gpd.GeoSeries(
        [*map(geom.LineString, [borde1, borde2, borde3, borde4])], crs="EPSG:4326"
    )

    bordeC = gpd.GeoSeries(
        [geom.Polygon([*borde1, *borde3, *borde2, *borde4])], crs="EPSG:4326"
    )

    # Compute grid
    paralelos, meridianos = ll_grid(min_lon, max_lon, min_lat, max_lat)

    # Transform CRS
    borde_merc = borde.to_crs(crs)
    bordeC_merc = bordeC.to_crs(crs)
    data_merc = data.to_crs(crs)

    paralelos = paralelos.to_crs(crs)
    meridianos = meridianos.to_crs(crs)

    # Filter data
    if "id" in data_merc.columns:
        geo_df = data_merc.set_index("id")
    else:
        geo_df = data_merc

    geo_df = geo_df[geo_df.intersects(bordeC_merc[0])].copy()

    # Create columns for matplotlib.
    geo_df["X"] = geo_df["geometry"].x
    geo_df["Y"] = geo_df["geometry"].y

    l = len(geo_df)

    if not s is None:
        geo_df["U"] = -s[:, 0]
        geo_df["V"] = -s[:, 1]
    else:
        geo_df = geo_df.rename(columns={"EW(m)": "U", "NS(m)": "V"})
        # Evaluate Other replacements ?
        s = geo_df.loc[:, ["U", "V"]].values
        s = np.stack([*s.T, np.zeros(len(s))]).T

    # --- Start Plotting
    fig, ax = plt.subplots(figsize=(10, 10), tight_layout=True)

    # Plot sites
    ax = geo_df.plot(
        marker="^",
        facecolor="none",
        edgecolor="black",
        ax=ax,
        markersize=36 * factors**2,
    )

    # Plot border
    borde_merc.plot(ax=ax)

    # Plot grid
    paralelos.plot(ax=ax, color="k")
    meridianos.plot(ax=ax, color="k")

    # Plot Epicenter (assumed at lat_0,lon_0)
    if epicenter:
        ax.scatter(
            [0],
            [0],
            [1000 * factors**2],
            marker="*",
            facecolor="orange",
            edgecolor="black",
        )

    # Separate Black and Red Arrows
    if scalediff <= 1:
        mask = np.zeros_like(s[:, 0]).astype(bool)
    else:
        mask = (s[:, 0] ** 2 + s[:, 1] ** 2) ** 0.5 > (2 * scale)

    grandes = geo_df[mask]
    chicas = geo_df[~mask]

    # Prepare Labels
    def mag2label(mag):
        if mag > 0.5:
            return f"{mag:.1f}m"
        elif mag > 0.01:
            return f"{mag*100:.1f}cm"
        else:
            return f"{mag*1000:.1f}mm"

    listaMag = [(u**2 + v**2) ** 0.5 for u, v in zip(chicas["U"], chicas["V"])]
    listaIdx = np.argsort(listaMag)
    listaXYLC = zip(
        *map(
            lambda x: np.array(x)[listaIdx],
            [chicas["X"].values, chicas["Y"].values, [*map(mag2label, listaMag)]],
        )
    )

    listaMag = [(u**2 + v**2) ** 0.5 for u, v in zip(grandes["U"], grandes["V"])]
    listaIdx = np.argsort(listaMag)
    listaXYLG = zip(
        *map(
            lambda x: np.array(x)[listaIdx],
            [grandes["X"].values, grandes["Y"].values, [*map(mag2label, listaMag)]],
        )
    )

    # Plot black arrows
    Qc = ax.quiver(
        chicas["X"],
        chicas["Y"],
        chicas["U"] * factorl,
        chicas["V"] * factorl,
        color="black",
        scale=scale,
        units="inches",
        width=factors * 0.05 / 2.5,
        headwidth=0,
        headlength=0,
    )

    # Plot red arrows
    Qg = ax.quiver(
        grandes["X"],
        grandes["Y"],
        grandes["U"] * factorl,
        grandes["V"] * factorl,
        color="red",
        scale=scalediff * scale,
        units="inches",
        width=factors * 0.1 / 2.5,
        headwidth=0,
        headlength=0,
    )

    # Plot black arrow key
    ax.quiverkey(
        Qc,
        0.4,
        0.1,
        scale * factorl,
        mag2label(scale),
        labelpos="N",
        coordinates="figure",
    )

    # Plot red arrow key
    ax.quiverkey(
        Qg,
        0.6,
        0.1,
        scalediff * scale * factorl,
        mag2label(scale * scalediff),
        labelpos="N",
        coordinates="figure",
    )

    # Hide plot borders
    ax.axis("off")

    # Plot labels (Black)
    for x, y, l in listaXYLC:
        ax.text(x, y, l)

    # Plot labels (Red)
    for x, y, l in listaXYLG:
        ax.text(x, y, l, color="red")

    # x0, x1 = ax.get_xlim()
    # y0, y1 = ax.get_ylim()
    # ax.set_xlim(x0-6e5,x1)

    # Plot basemap if needed
    source = ctx.providers.Stamen.TonerBackground
    if base:
        print('Downloading BaseMap')
        ctx.add_basemap(ax, source=source, crs=crs, zoom=mapzoom)
    else:
        pass
        #lims = [x0 - 10e5, x1 + 10e5]
        #mlim = sum(lims) / 2
        #dlim = mlim - lims[0]
        #ax.set_xlim(mlim - dlim * factor, mlim + dlim * factor)
        #ax.set_ylim(y0-10e5,y1+10e5)

    # Save Figure
    plt.savefig(fname)

    if base:
        process = sp.check_output(['bash','-c',colorize_bash_script,'inline_script',fname])



@click.command()
@click.option("--tipo", default=1)
@click.option("--rutaG", default="")
@click.option("--fname", default="out.png")
@click.option("--scale", default=0.1,
              help="base scale for black arrows")
@click.option("--base", default=False)
@click.option("--scalediff", default=5,
              help="scale factor between red and black arrows")
@click.option("--mapzoom", default=5)
@click.option("--limites", default="")
#@click.option("--factor", default=1.0)
@click.option("--factorl", default=1.0, help="Factor to scale lengths")
@click.option("--factors", default=1.0,
              help="factor to scale symbols and arrow widths")
@click.option("--jsonfile", default="")
@click.option("--csvin", default="")
@click.option("--lat_0", default=-9999.0)
@click.option("--lon_0", default=-9999.0)
@click.option("--recursive", default=1)
def main(
    tipo,
    rutag,
    fname,
    scale,
    base,
    scalediff,
    mapzoom,
    limites,
    #factor,
    factorl,
    factors,
    jsonfile,
    csvin,
    lat_0,
    lon_0,
    recursive,
):


    if csvin == "" and jsonfile == "":
        click.echo("One of --csvin or --jsonfile are mandatory")
        return 1

    if csvin != "":
        data = pd.read_csv(csvin, delimiter="[,;\s]+", engine="python")
        data = gpd.GeoDataFrame(
            data,
            geometry=data.T.apply(lambda x: geom.Point(x.Longitude, x.Latitude)),
            crs="EPSG:4326",
        )
        s = None
    else:
        print('Computing Offsets')
        G = loadModel(tipo, rutag)

        with open(jsonfile) as f:
            jdata = json.load(f)

        sitios_series, data, data_eq = jdata
        sitios = sitios_series["puntos"]
        series = sitios_series["instances"]

        series = [[[i if i != "NaN" else np.nan for i in j] for j in k] for k in series]
        seriesS = pd.Series(series, index=sitios)
        seriesS = seriesS.map(np.array).map(lambda x: x - np.nanmean(x, axis=0))

        d = np.stack(seriesS.values)

        if recursive > 1:
            s = recapply(G, d, recursive)
        else:
            s = G(d).numpy()

        data = gpd.GeoDataFrame(
            data,
            geometry=[*map(lambda x: geom.Point(x["long"], x["lat"]), data)],
            crs="EPSG:4326",
        )

        data = data.rename(columns={"estacion": "id"})

    epicenter = True
    if jsonfile == "":
        if lat_0 != -9999 and lon_0 != -9999:
            crs_extra_params = {"lat_0": lat_0, "lon_0": lon_0}
        else:
            epicenter = False
            crs_extra_params = {
                    "lat_0": data.Latitude.mean(),
                    "lon_0": data.Longitude.mean(),
                }
    else:
        crs_extra_params = {
                "lat_0": data_eq["latitude"],
                "lon_0": data_eq["longitude"]
                }

    print('Start Plotting')
    gen_map(data,s=s,crs_extra_params=crs_extra_params,
             limites=limites,factorl=factorl,factors=factors,base=base,
             mapzoom=mapzoom,
             scale=scale,scalediff=scalediff,epicenter=epicenter,fname=fname)



if __name__ == "__main__":
    exit(main())
