# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.13.5
#   kernelspec:
#     display_name: Python 3 (ipykernel)
#     language: python
#     name: python3
# ---

import sys

rpt = sys.argv[1]

import matplotlib.pyplot as plt
import json

with open(f'{rpt}/l-{rpt[2:]}','r') as f:
    data = json.load(f)

losses = data

fig, (ax1,ax2) = plt.subplots(2)
# +
ax1.plot([i[0][1] for i in losses],'x',label='TR-G',color='gray')
ax1.plot([i[1][1] for i in losses],'.',label='TS-G',color='k')
#plt.ylim(-1,0)
ax1.legend()

ax2.plot([i[0][0] for i in losses],'x',label='TR-D',color='gray')
ax2.plot([i[1][0] for i in losses],'.',label='TS-D',color='k')
#plt.ylim(-1,0)

ax2.legend()

plt.savefig('losses.svg')
# -


