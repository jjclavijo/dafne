import sys
import matplotlib.pyplot as plt
import numpy as np
from pyarrow import parquet as pq

import seaborn as sbs
import numpy as np
import pandas as pd


rpt = sys.argv[1]
start1 = int(sys.argv[2])
end1 = int(sys.argv[3])

start2 = int(sys.argv[4])
end2 = int(sys.argv[5])

start3 = int(sys.argv[6])
end3 = int(sys.argv[7])

BUFFER = int(2e6)
BS = int(256)

nbins = 40
nbins = 10**np.linspace(-3,np.log10(3),nbins + 1)
nbins[0] = 1e-3 - (nbins[1] - 1e-3)

print(nbins)

plasma = plt.get_cmap('plasma')

def smooth(x,y):

    from scipy.interpolate import interp1d

    x = np.log(x+0.01)
    y = np.log(y+0.01)

    X_Y_Spline = interp1d(x, y, kind = "quadratic")

    X_ = np.linspace(x.min(), x.max(), 500)

    Y_ = X_Y_Spline(X_)

    return np.exp(X_)-0.01,np.exp(Y_)-0.01


x = nbins
x = x[1:]-(x[1]-x[0])/2

def plot_bp(ax,start,end):
    lista = []
    for i in np.arange(start,end):

        S = pq.read_table(f'{rpt}/v{i:d}.pq').to_pandas().steps.values
        S = np.stack(S)
        S = S.reshape(-1)
        d,_ = np.histogram(np.abs(S),nbins,range=(0,1))

        lista.append(d)

    listaT = np.stack(lista).T

    w = nbins[:1]-nbins[:-1]

    ddd = ax.boxplot([*listaT],positions=x,widths=w)
    bx = ddd['boxes']
    for box,x1,x2 in zip(bx,nbins[:-1],nbins[1:]):
        a,b = box.get_data()
        a = (x2,x1,x1,x2,x2)
        box.set_data((a,b))
    for wis,x1,x2 in zip(ddd['whiskers'][1::2],nbins[:-1],nbins[1:]):

        a,b = wis.get_data()
        xm = 10**((np.log10(x1)+np.log10(x2))/2)
        a = (xm,xm)
        wis.set_data((a,b))
    for wis,x1,x2 in zip(ddd['whiskers'][::2],nbins[:-1],nbins[1:]):
        a,b = wis.get_data()
        xm = 10**((np.log10(x1)+np.log10(x2))/2)
        a = (xm,xm)
        wis.set_data((a,b))
    for cap,x1,x2 in zip(ddd['caps'][::2],nbins[:-1],nbins[1:]):

        a,b = cap.get_data()
        a = (x1,x2)
        cap.set_data((a,b))
    for cap,x1,x2 in zip(ddd['caps'][1::2],nbins[:-1],nbins[1:]):
        a,b = cap.get_data()
        a = (x1,x2)
        cap.set_data((a,b))
    for med,x1,x2 in zip(ddd['medians'],nbins[:-1],nbins[1:]):
        a,b = med.get_data()
        a = (x1,x2)
        med.set_data((a,b))
    for fli,x1,x2 in zip(ddd['fliers'],nbins[:-1],nbins[1:]):
        a,b = fli.get_data()
        xm = 10**((np.log10(x1)+np.log10(x2))/2)
        a = tuple([xm]*len(a))
        fli.set_data((a,b))

    ax.set_xscale('log')
    ax.set_yscale('log')

    ms = [i.get_data()[1][0] for i in ddd['medians']]

    ax.fill_between(x,ms,color='orange',alpha=0.5)

fix,(ax1,ax2,ax3) = plt.subplots(figsize=(5,8),nrows=3,tight_layout=True,sharey=True,sharex=True)

plot_bp(ax1,start1,end1)
plot_bp(ax2,start2,end2)
plot_bp(ax3,start3,end3)

ax1.grid(which='major',axis = 'both',ls='--')
ax2.grid(which='major',axis = 'both',ls='--')
ax3.grid(which='major',axis = 'both',ls='--')

ax1.grid(which='minor',axis = 'both',ls='dotted')
ax2.grid(which='minor',axis = 'both',ls='dotted')
ax3.grid(which='minor',axis = 'both',ls='dotted')

plt.savefig('histograms.svg')
plt.close()
#plt.show()
