import sys

rpt = sys.argv[1]
ep = sys.argv[2]

import tensorflow as tf

BUFFER = int(2e6)
BS = int(256)

import matplotlib.pyplot as plt
import matplotlib as mpl
from dfmodels.tf.fun import apply_step
import numpy as np
from pyarrow import parquet as pq

show_dset= tf.data.experimental.load('ds/show').batch(BS).take(1).get_single_element()
show_dset = (show_dset[0],show_dset[1][:,0])


#for i in np.arange(9,1000,10):
#    S = pq.read_table(f'{rpt}/t{i:d}.pq').to_pandas().steps.values
#    S = np.stack(S)
#    plt.plot(S,'.k')
#    plt.savefig(f'{rpt}/t{(i-9)//10:d}.png')
#    plt.close()

S = pq.read_table(f'{rpt}/{ep}.pq').to_pandas().steps.values
S = np.stack(S)

n_canales = 2

def gen_plot(fname=None):

    fig,axs = plt.subplots(16,6,figsize=(18/2.54,14/2.54),gridspec_kw={'hspace': 0.0,'wspace': 0.0},tight_layout=True)

    muestra=0

    dcved = apply_step(show_dset[0],tf.constant(S,dtype=tf.float32))

    data = show_dset[0].numpy()
    maxima = np.nanmax(data,axis=-2)
    minima = np.nanmin(data,axis=-2)

    diff = (maxima - minima)[:,:-1].max(axis=-1)

    orden = np.argsort(diff)

    print(len(orden))

    axs = axs.T

    for nr,row in enumerate(axs):
        for i,ax in enumerate(row):
            canal = i % n_canales
            if canal < n_canales:
                m = muestra + i // n_canales

                print(f'muestra {m}, canal {canal}, row {nr}, col {i}')
                m = orden[m]
                print(f'muestra {m}, canal {canal}, row {nr}, col {i}')

                t = show_dset[0][m].numpy()
                #t[30,:] = np.nan

                f = dcved[m].numpy()
                f[30,:] = np.nan

                ax.plot(t[:,canal],color='gray')
                ax.plot(f[:,canal],color='k')

                maximo = max( [*t[:,canal].flatten(),*f[:,canal].flatten()] )

                if i % n_canales == 0:
                    x_min,x_max = ax.get_xlim()

                    y_max = np.nanmax(t[:,:n_canales])
                    y_min = np.nanmin(t[:,:n_canales])

                    #y_min,y_max = ax.get_ylim()

                ax.set_xlim(x_min,x_max)

                #ymean = (y_max + y_min)/2

                #print(y_max-y_min)
                #if y_max - y_min < 0.015:
                #    y_max = 0.0075
                #    y_min = -0.0075
                #elif y_max - y_min < 0.10:
                #    y_max = 0.05
                #    y_min = -0.05

                #ax.set_ylim(y_min,y_max)

                ax.set_ylim(y_min,y_max)

                """
                ts = ax.transData
                mx = ts.get_matrix()
                mx0 = mx[0,0]
                mx1 = mx[1,1]
                #ax_h, ax_w = ax.bbox.height, ax.bbox.width
                #print(mx0,mx1,ax_h,ax_w)

                l = ax.plot([0,0.01*(mx1)/(mx0)],[maximo/2,maximo/2],
                            color='r',linewidth=4,alpha=0.4)
                """

                dt = ax.transData

                circ = mpl.patches.Circle([0,0],1,facecolor='steelblue',
                        alpha=0.4)#,linestyle=':',edgecolor='k')
                c = ax.add_patch(circ)
                tr0 = c._transform
                pt0 = c.get_patch_transform()

                m = dt.get_matrix()
                m0 = m[0,0]
                m1 = m[1,1]

                radio = 0.005

                altura = np.mean(ax.get_ylim())

                m = [[radio*m1/m0,0,30],
                     [0,radio,altura],
                     [0,0,1]]

                tr = mpl.transforms.Affine2D(m)

                tr0.transform([0,0])

                c.set_transform(tr+tr0)


                #ax.set_xticklabels([])
                #ax.set_yticklabels([])
                ax.axis('off')
            """
            else:
                m = muestra + (i-3) // 3
                t = show_dset[0][m].numpy()
                #t[30,:] = np.nan

                f = dcved[m].numpy()
                f[30,:] = 0

                #ax.plot(t[:,canal],color='gray')
                ax.plot(f[:,canal-3],color='k')
                #ax.set_xticklabels([])
                #ax.set_yticklabels([])
                ax.axis('off')
            """

        muestra += len(row) // n_canales


    #if fname is None:
    #    plt.show()
    #else:
    if True:
        plt.savefig('Saltos.svg')
        plt.close(fig)

gen_plot()
