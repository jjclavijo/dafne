import sys
from pyarrow import parquet as pq
import matplotlib.pyplot as plt
import matplotlib as mpl
from tensorflow.data.experimental import load as tfload
import numpy as np
import subprocess as sp
import tempfile as tmp

plt.rcParams['mathtext.fontset'] = 'custom'
plt.rcParams['mathtext.it'] = 'Montserrat:italic'
plt.rcParams['mathtext.rm'] = 'Montserrat'
plt.rcParams['mathtext.sf'] = 'Montserrat'
plt.rcParams['mathtext.cal'] = 'jsMath\-cmsy10'

rpt = sys.argv[1]
ep = int(sys.argv[2])
chans = sys.argv[3]
try:
    fname = sys.argv[4]
except IndexError:
    fname = ''

channels = {'E':0,'e':0,'N':1,'n':1,'U':2,'u':2}

tst = tfload("ds_soft/test").batch(int(2e6))
l = tst.take(1).get_single_element()[1][:, 0].numpy().astype(int)
d = tst.take(1).get_single_element()[0].numpy().astype(float)

avg1 = np.mean(d[:,25:30,:],axis=1)
avg2 = np.mean(d[:,32:37,:],axis=1)
saltos_m = avg1 - avg2



def preprocess_s(S):

    return np.stack(S.steps.values)


if True:
        S = pq.read_table(f"{rpt}/t{ep:d}.pq").to_pandas()
        saltos = preprocess_s(S)

        saltos = np.abs(saltos)#[:,2]
        saltos_m = np.abs(saltos_m)#[:,2]

        fig,ax1 = plt.subplots(tight_layout=True,figsize=(9/2.54,9/2.54))

        #mask = l.astype(bool)

        #ax1.plot(saltos_m[mask].flatten(),saltos[mask].flatten(),'.',color='k',alpha=0.2)
        #ax1.set_xscale('log')
        #ax1.set_xlim(10**-4,10)
        #ax1.set_yscale('log')
        #ax1.set_ylim(10**-4,10)
        #ax1.set_aspect('equal')

        #ax1.plot((10**-4,10),(10**-4,10),color='gray')

        #ax1.grid('both','both',linestyle=':')

        #xlab = ax1.set_xlabel('Mean-based estimator')
        #ylab = ax1.set_ylabel('$\hat{A}$')
        #ylab.set_usetex('True')
        #ylab.set_fontsize(11)
        #xlab.set_fontsize(11)

        #plt.show()
        #plt.close()

        ch = [channels[i] for i in chans]

        x = np.log10(saltos_m[:,ch])
        y = np.log10(saltos[:,ch])
        mascara = ( x > -4 ) & ( np.abs(y) > -4 )
        x = x[mascara]
        y = y[mascara]

        heatmap, xedges, yedges = np.histogram2d(x, y, bins=100)
        extent = [xedges[0], xedges[-1], yedges[0], yedges[-1]]

        #plt.clf()
        mm = mpl.colormaps.get('magma_r').colors
        mm[0] = [1,1,1]
        mm = mpl.colors.ListedColormap(mm)

        plt.imshow(heatmap.T, extent=extent, origin='lower', cmap=mm)
        ax1.set_aspect('equal')
        ax1.set_xlim(-4,1)
        ax1.set_ylim(-4,1)

        ticks = []
        i=-4
        while i < plt.xlim()[1]:
            ticks.extend([*np.log10(np.linspace(10**i,10**(i+1),10))])
            i += 1

        ax1.xaxis.set_ticks(ticks,minor=True)
        ax1.yaxis.set_ticks(ticks,minor=True)

        ticks = [-4,-3,-2,-1,0,1]
        ax1.xaxis.set_ticks(ticks,[f'$10^{{{i}}}$' for i in ticks],minor=False)
        ax1.yaxis.set_ticks(ticks,[f'$10^{{{i}}}$' for i in ticks],minor=False)

        plt.plot([-4,1],[-4,1],':k',alpha=0.25)
        plt.plot([-2,-2,-4],[-4,-2,-2],'--r')
        plt.grid('both','minor',linestyle='-',color='lightgray',alpha=0.25)
        plt.grid('both','major',linestyle='-',color='gray',alpha=0.5)

        xlab = ax1.set_xlabel('Mean-based estimator')
        ylab = ax1.set_ylabel('$\hat{A}$')
        ylab.set_usetex('True')
        ylab.set_fontsize(11)
        xlab.set_fontsize(11)

        plt.colorbar()

        if fname:
            plt.savefig(fname,dpi=500)
        plt.show()

