# Crossvalidation and Reporting

For completion, we include scripts to exemplify the scheme used in crossvalidating
the hyperparameters and all the scripts used in producing reports for the paper.

The scripts are divided on two folders `01` and `02`, corresponding to the
two alternative models. Many of them ar fairly simmilar, but some parameters,
as axis limits may variate.

Some reports are produced from snapshots saved during the training process,
other are produced by running the trined models. The snapshots used for the paper
figures are published in the artifacts container.

The scripts on reviewplots produces the final figures that appears in the revised
version of the paper.

The csv file in the reviewplots folder is produced by the hectorEstimations.py
script, that makes use of hectorpp, a slightly modified version of Machiel Bos's
hectorp package. The repository for hectorpp is https://gitlab.com/jjclavijo/hectorpp
