from plot_funcs import *
import os.path
from matplotlib.collections import LineCollection


dfsaltos = pd.read_csv('./hectorJumps_ds_validation.csv',index_col=[0])
saltos = dfsaltos.values

data_p = get_p_data(get_data_path('ds_soft/validation/'))

G = load_G_model_1('663026d5',999)

#
# --- Figure 04
#

plot_hist2d(-G(data_p).numpy(),dfsaltos.values,chans='EN',
            truncate_map=10,nbins=70,
            imshow_opts={'vmax':45,'interpolation':'none'},
            fname='figs_r1/04.Hector66EN_n.png')

#
# --- Figure 05
#

plot_hist2d(-G(data_p).numpy(),dfsaltos.values,chans='U',
            truncate_map=10,nbins=70,
            imshow_opts={'vmax':10,'interpolation':'none'},
            fname='figs_r1/05.Hector66U_n.png')

plot_hist2d(-recapply(G,data_p,50).numpy(),dfsaltos.values,
            chans='U',truncate_map=10,nbins=70,
            imshow_opts={'vmax':10,'interpolation':'none'},
            fname='figs_s1/05.Hector66Urec_n.png')

#
# --- Figure 11
#

G = load_G_model_1('f867f4fb',999)
plot_hist2d(-G(data_p).numpy(),dfsaltos.values,chans='U',
            truncate_map=10,nbins=70,
            imshow_opts={'vmax':10,'interpolation':'none'},
            fname='figs_r1/11.HectorU_f8_n.png')

data_ps_p,saltos_ps_p = get_p_data_jumps(get_data_path('dsynth/validation/'))

#
# --- Figure 06
#

data_ps_p,saltos_ps_p = get_p_data_jumps(get_data_path('dsynth_hist/positive/'))
data_nops_p,saltos_nops_p = get_p_data_jumps(get_data_path('dsynth_hist_no_ps/positive/'))

G = load_G_model_1('663026d5',999)
plot_synth_hists(get_data_path('dsynth_hist/positive/'),
                 get_data_path('dsynth_hist_no_ps/positive/'),G,
                 fname='figs_r1/06.Synth66EN_n.png')
plt.close()

#
# --- Figure S05
#

plot_synth_hists(get_data_path('dsynth_hist/positive/'),
                 get_data_path('dsynth_hist_no_ps/positive/'),
                 lambda x: recapply(G,x,50),fname='figs_s1/S05_histogram_recursive.png')
plt.close()

data_ps_p,saltos_ps_p = get_p_data_jumps(get_data_path('dsynth/validation/'))
data_nops_p,saltos_nops_p = get_p_data_jumps(get_data_path('dsynth_no_ps/validation/'))

#
# --- Figure S09
#

_66x100models = [ load_G_model_1("663026d5",i*10+9) for i in range(99) ]

rmss_66_dict = rmss_split_rec(_66x100models,50,data_ps_p,saltos_ps_p)
rmss_66_dict_nops = rmss_split_rec(_66x100models,50,data_nops_p,saltos_nops_p)

rmss_66_nops = rmss_66_dict_nops['not_recursive']
rmss_66_ps = rmss_66_dict['not_recursive']

fig,[axU,axM,axB] = plot_rmms_split_2groups(rmss_66_ps,rmss_66_nops,'Data with transient','Data without transient')
plt.savefig('figs_s1/S09_MSE_real_ps_nops.png',dpi=500)
plt.close()

serrs_ps, img_ps = rmss_vs_rec(_66x100models,50,0,1,data_ps_p,saltos_ps_p)
serrs_nops, img_nops = rmss_vs_rec(_66x100models,50,0,1,data_nops_p,saltos_nops_p)

fig,axes = plt.subplots(nrows=2,sharex=True,figsize=(9/2.54,9/2.54),gridspec_kw=dict(top=0.97,right=0.90,left=0.18,bottom=0.12,hspace=0.02))

t = np.arange(0,50) # your "time" variable

steps = np.array([i*10.+9 for i in range(99)])

x = np.array([np.linspace(i,i+30,50) for i in steps]).T

for row, ax, img in zip([0,1],axes,(img_ps,img_nops)):

    y = np.array(img).T
    # set up a list of (x,y) points

    points = np.array([x,y]).transpose(2,1,0)

    # set up a list of segments
    segs = np.concatenate([points[:,:-1,:],points[:,1:,:]],axis=-1)

    for seg in segs[::-1]:
        # make the collection of segments
        lc = LineCollection(seg.reshape(-1,2,2), cmap='viridis_r')
        lc.set_array(t) # color the segments by our parameter
        # plot the collection
        ax.add_collection(lc) # add the collection to the plot

    for seg in segs[::-1]:
        pc = ax.scatter(*seg.reshape(-1,2,2)[:,0,:].T,marker='.',c=t[:-1],cmap='viridis_r')
        pc.set_zorder(3)

    #plt.xlim(x.min(), x.max()) # line collections don't auto-scale the plot
    #plt.ylim(y.min(), y.max())
    ax.set_yscale('log')
    axb = ax.twinx()
    axb.set_yticks([])
    if row == 1:
        ax.set_xlabel('Train Step')
        axb.set_ylabel('with postseismic',color='darkgray')

    ax.set_ylabel('rMSE [m]')
    ax.tick_params(axis='both', labelsize=8)
    if row == 0:
        axb.set_ylabel('without postseismic',color='darkgray')
        ax.set_xticklabels([])

        cbaxes = inset_axes(ax,width="40%",height="5%", loc=1,borderpad=1.5)
        cbaxes.tick_params(axis='both', labelsize=8)
        cbaxes.set_title('pass n°',fontsize=10)
        plt.colorbar(pc,cax=cbaxes,orientation='horizontal')

plt.savefig('figs_s1/S10_rmse_vs_step_vs_recursive.png',dpi=500)
plt.close()


#
# --- Figure S16
#

_39x100models = [ load_G_model_1("39b792da",i*10+9) for i in range(99) ]
_abx100models = [ load_G_model_1("ab889008",i*10+9) for i in range(99) ]

rmss_39_dict = rmss_split_rec(_39x100models,50,data_ps_p,saltos_ps_p)
rmss_39_dict_nops = rmss_split_rec(_39x100models,50,data_nops_p,saltos_nops_p)
rmss_ab_dict = rmss_split_rec(_abx100models,50,data_ps_p,saltos_ps_p)
rmss_ab_dict_nops = rmss_split_rec(_abx100models,50,data_nops_p,saltos_nops_p)

rmss_39_ps = rmss_39_dict['not_recursive']
rmss_39_nops = rmss_39_dict_nops['not_recursive']
rmss_ab_ps = rmss_ab_dict['not_recursive']
rmss_ab_nops = rmss_ab_dict_nops['not_recursive']

fig,ax = plt.subplots(ncols=2,nrows=3,sharex=True,sharey='row',
         gridspec_kw=dict(hspace=0.04,wspace=0.04,top=0.90,
         right=0.95,left=0.15))
plot_rmms_split_2groups(rmss_39_ps,rmss_39_nops,'Data with transient','Data without transient',ax=ax[:,0])
plot_rmms_split_2groups(rmss_ab_ps,rmss_ab_nops,'Data with transient','Data without transient',ax=ax[:,1])

allax = fig.get_axes()
allax[0].legend_.set_visible(False)
for i,j in zip(allax[6:9],allax[::2]):
    i.set_ylim(j.get_ylim())
for i,j in zip(allax[9:],allax[1::2]):
    i.set_ylim(j.get_ylim())
for i in [3,6,7,8]:
    allax[i].set_ylabel('')

allax[1].set_title('Trained without transients',color='darkgray',fontsize=10)
allax[0].set_title('Trained with transients',color='darkgray',fontsize=10)

plt.savefig('figs_s1/S16.rMSE_Synth_vs_Synth.png',dpi=500)
plt.close()

#
# --- Figure S17
#

G = load_G_model_1('39b792da',999)
f1,(ax1,ax2),cb1 = plot_synth_hists(get_data_path('dsynth_hist/positive/'),get_data_path('dsynth_hist_no_ps/positive/'),G,chans='ENU',fname='return')
G = load_G_model_1('ab889008',999)
f2,(ax3,ax4),cb2 = plot_synth_hists(get_data_path('dsynth_hist/positive/'),get_data_path('dsynth_hist_no_ps/positive/'),G,chans='ENU',fname='return')

f3 = plt.figure(figsize=(25/2.54,13/2.54))

for i in f1.axes:
    i.remove()
    i.figure = f3
    i.set_aspect('auto')
    f3.axes.append(i)
    f3.add_axes(i)
    i.set_position(np.array(i.get_position().bounds) * [1.,1.,1.15,1.] )

for i in f2.axes:
    i.remove()
    i.figure = f3
    i.set_aspect('auto')
    f3.axes.append(i)
    f3.add_axes(i)
    i.set_position(np.array(i.get_position().bounds) * [1.,1.,1.1,1.] +[0.90,0.,0.,0.] )

ax1.sharey(ax3)
ax2.sharey(ax4)

plt.setp(ax3.get_yticklabels(),color="none")
plt.setp(ax4.get_yticklabels(),color="none")

f3.axes[3].remove()
f3.axes[3].remove()
#f3.axes[3].remove()

cb2.ax.remove()
cb1.ax.remove()

cbaxes = inset_axes(ax2, width="60%", height="8%", loc=3,borderpad=2)

cb = plt.colorbar(cb1.mappable,cax=cbaxes,orientation='horizontal')
cb.ax.text(cb.ax.get_xlim()[1],0.5,"frequency",ha='right',va='center',size=7,color='white')

plt.close(f1)
plt.close(f2)

f3.axes[3].set_ylabel('')

ax1.set_title('Trained with transients',fontsize=10)
ax3.set_title('Trained without transients',fontsize=10)

plt.draw_all()

plt.savefig('figs_s1/S17.hist_Synth_vs_Synth.svg')

plt.show()

#
# --- Figure S18
#

G = load_G_model_1('39b792da',999)
f1,(ax1,ax2),cb1 = plot_synth_hists(get_data_path('dsynth_hist/positive/'),get_data_path('dsynth_hist_no_ps/positive/'),lambda x:recapply(G,x,50),chans='ENU',fname='return')
G = load_G_model_1('ab889008',999)
f2,(ax3,ax4),cb2 = plot_synth_hists(get_data_path('dsynth_hist/positive/'),get_data_path('dsynth_hist_no_ps/positive/'),lambda x:recapply(G,x,50),chans='ENU',fname='return')

f3 = plt.figure(figsize=(25/2.54,13/2.54))

for i in f1.axes:
    i.remove()
    i.figure = f3
    i.set_aspect('auto')
    f3.axes.append(i)
    f3.add_axes(i)
    i.set_position(np.array(i.get_position().bounds) * [1.,1.,1.15,1.] )

for i in f2.axes:
    i.remove()
    i.figure = f3
    i.set_aspect('auto')
    f3.axes.append(i)
    f3.add_axes(i)
    i.set_position(np.array(i.get_position().bounds) * [1.,1.,1.1,1.] +[0.90,0.,0.,0.] )

ax1.sharey(ax3)
ax2.sharey(ax4)

plt.setp(ax3.get_yticklabels(),color="none")
plt.setp(ax4.get_yticklabels(),color="none")

f3.axes[3].remove()
f3.axes[3].remove()

cb2.ax.remove()
cb1.ax.remove()

cbaxes = inset_axes(ax2, width="60%", height="8%", loc=3,borderpad=2)

cb = plt.colorbar(cb1.mappable,cax=cbaxes,orientation='horizontal')
cb.ax.text(cb.ax.get_xlim()[1],0.5,"frequency",ha='right',va='center',size=7,color='white')

plt.close(f1)
plt.close(f2)

f3.axes[3].set_ylabel('')

ax1.set_title('Trained with transients',fontsize=10)
ax3.set_title('Trained without transients',fontsize=10)

plt.savefig('figs_s1/S18.hist_Synth_vs_Synth_rec.svg',dpi=500)

plt.show()
