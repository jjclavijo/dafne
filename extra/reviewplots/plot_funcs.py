import subprocess as sp
from io import StringIO
import os
import json
import importlib
import importlib.util
from xml.dom import minidom
import pickle


import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1.inset_locator import inset_axes

import pandas as pd
import tensorflow as tf

from scipy.stats import rankdata

from dfmodels.models import model2labels
from dfmodels.tf.fun import apply_step as S

import glob

_MODELBASE = ['./',
              '/home/javier/proy/dafne.pub/paperdata/trained/01',
              '/home/javier/proy/dafne.pub/paperdata/trained/02',
              '/home/javier/proy/dafne.pub/test/artifacts/trained/01',
              '/home/javier/proy/dafne.pub/test/paperRev2/',
              '/home/javier/proy/dafne.pub/test/paperRev/',
              '/home/javier/proy/dafne.pub/test/artifacts/trained/02']

MODELSTRING = "{base}/c-{hash}-{type}/{step:04d}.ckpt"

_loaded_models = {}

def load_G_model_1(model_hash, step, model=None):
    for base in _MODELBASE:
        if glob.glob('c-{}-*'.format(model_hash),root_dir=base):
            basepath = base
            break
    else:
        return None

    if model is None:
        if "dfmodels.models.model1" in _loaded_models:
            module = _loaded_models["dfmodels.models.model1"]
        else:
            # Import the module for the deceiver isolated from sys.modules
            spec = importlib.util.find_spec("dfmodels.models.model1")
            importlib.util.find_spec("dfmodels.models.model1")
            module = importlib.util.module_from_spec(spec)
            spec.loader.exec_module(module)
            _loaded_models["dfmodels.models.model1"] = module

        G = module.deceiv()
    else:
        G = model
    G.load_weights(MODELSTRING.format(base=basepath,hash=model_hash, step=step, type="G"))

    return G

_DATASETBASE = ['/home/javier/proy/dafne.pub/paperdata',
                './','/home/javier/proy/dafne.pub/data/scripts',
                '/home/javier/proy/dafne.pub/test/artifacts']

def get_data_path(datapath):
    for base in _DATASETBASE:
        if glob.glob(datapath,root_dir=base):
            return os.path.join(base,datapath)
    else:
        raise ValueError('Dataset not found')

def load_G_model_2(model_hash, step, model=None):
    for base in _MODELBASE:
        if glob.glob('c-{}-*'.format(model_hash),root_dir=base):
            basepath = base
            break
    else:
        return None

    if model is None:
        if "dfmodels.models.model2labels" in _loaded_models:
            module = _loaded_models["dfmodels.models.model2labels"]
        else:
            # Import the module for the deceiver isolated from sys.modules
            spec = importlib.util.find_spec("dfmodels.models.model2labels")
            importlib.util.find_spec("dfmodels.models.model2labels")
            module = importlib.util.module_from_spec(spec)
            spec.loader.exec_module(module)
            _loaded_models["dfmodels.models.model2labels"] = module

        G = module.deceiv()
    else:
        G = model
    G.load_weights(MODELSTRING.format(base=basepath,hash=model_hash, step=step, type="G"))

    return G

# Plot timeseries raw and processed by G
def gen_plot(G,dset_path='ds/show',fname=None):

    intervalomag = -5 # Impossible
    maglabels = {-3:'1mm',-2:'1cm',-1:'10cm',0:'1m'}
    # letra = 'abcdefghijklmnopqrstuvwxyz'
    BUFFER = int(2e6)
    BS = int(256)

    show_dset = tf.data.Dataset.load(dset_path)
    show_dset = show_dset.batch(BS).take(1).get_single_element()
    show_dset = (show_dset[0], show_dset[1][:, 0]) #data, label

    fig, axs = plt.subplots(
        16,
        3, #6, ?
        figsize=(18 / 2.54, 15 / 2.54),
        gridspec_kw={"hspace": 0.0, "wspace": 0.0},
        tight_layout=True,
    )

    muestra = 0

    #dcved = S(show_dset[0], tf.constant(computed_steps, dtype=tf.float32))
    dcved = S(d:= show_dset[0], G(d))

    data = show_dset[0].numpy()
    maxima = np.nanmax(data, axis=-2)
    minima = np.nanmin(data, axis=-2)

    diff = (maxima - minima)[:, :-1].max(axis=-1)

    orden = np.argsort(diff)

    for nr, row in enumerate(axs):
        maximo = 0
        for i, ax in enumerate(row):
            for canal in range(n_canales):
                m = i + nr * 3

                # print(f"muestra {m}, canal {canal}, row {nr}, col {i}")
                m = orden[m]
                # print(f"muestra {m}, canal {canal}, row {nr}, col {i}")

                t = show_dset[0][m].numpy()
                # t[30,:] = np.nan

                f = dcved[m].numpy()
                f[30, :] = np.nan

                ax.plot(np.arange(-61,0)+(canal-1)*70,t[:, canal], color="#6ece58ff")
                ax.plot(np.arange(-61,0)+(canal-1)*70,f[:, canal], color="#482374ff")

                maximo = np.nanmax([maximo,
                              *np.abs(t[:, canal].flatten()),
                              *np.abs(f[:, canal].flatten())])

                ax.spines['left'].set_position('center')
                ax.spines['left'].set_color('red')
                ax.spines['bottom'].set_color('none')
                ax.spines['right'].set_color('none')
                ax.spines['top'].set_color('none')

                ax.xaxis.set_visible(False)

                if (canal == n_canales - 1) and                    i == 2:
                    x_min, x_max = ax.get_xlim()

                    intervalomagp = intervalomag
                    for axi in row:

                        axi.set_xlim(x_min, x_max)

                        axi.set_ylim(-maximo, +maximo)

                        intervalo = 2 * maximo
                        intervalomag = np.floor(np.log10(intervalo))
                        tick = 10**intervalomag/2
                        axi.set_yticks([-tick,tick],color='red')
                        if intervalomag != intervalomagp:
                            axi.set_yticklabels(['',maglabels[intervalomag]],fontsize=7,color='red')
                        else:
                            axi.set_yticklabels(['',''])
                        axi.spines['left'].set_bounds(-tick,tick)



    if fname is None:
        plt.show()
    else:
        plt.savefig(fname)
        plt.close(fig)

# def model(args):
#     return G((args,np.array([[1.]]*len(args))))

#
# --- Plot comparison histograms
#

def plot_hist2d(saltos, saltos_m, chans="ENU", fname=False, nbins=100,
                xtext= "hector ML estimator", ytext="$\hat{A}$",
                truncate_map=0,imshow_opts={}):

    fig, ax1 = plt.subplots(tight_layout=True, figsize=(9 / 2.54, 9 / 2.54))

    channels = {"E": 0, "e": 0, "N": 1, "n": 1, "U": 2, "u": 2}

    ch = [channels[i] for i in chans]

    x = np.log10(np.abs(saltos_m[:, ch]))
    y = np.log10(np.abs(saltos[:, ch]))

    mascara = (x > -4) & (y > -4) \
              & (np.sign(saltos[:, ch]) == np.sign(saltos_m[:, ch]))

    x = x[mascara]
    y = y[mascara]

    heatmap, xedges, yedges = np.histogram2d(x, y, bins=nbins)
    extent = [xedges[0], xedges[-1], yedges[0], yedges[-1]]

    # plt.clf()
    mm = mpl.colormaps.get("magma_r").colors
    #Truncate Colormap.
    mm = mm[truncate_map:]
    mm[0] = [1, 1, 1]
    mm = mpl.colors.ListedColormap(mm)

    plt.imshow(heatmap.T, extent=extent, origin="lower", cmap=mm,**imshow_opts)
    print(np.max(heatmap))

    ax1.set_aspect("equal")
    ax1.set_xlim(-4, 1)
    ax1.set_ylim(-4, 1)

    ticks = []
    i = -4
    while i < plt.xlim()[1]:
        ticks.extend([*np.log10(np.linspace(10**i, 10 ** (i + 1), 10))])
        i += 1

    ax1.xaxis.set_ticks(ticks, minor=True)
    ax1.yaxis.set_ticks(ticks, minor=True)

    ticks = [-4, -3, -2, -1, 0, 1]
    ax1.xaxis.set_ticks(ticks, [f"$10^{{{i}}}$" for i in ticks], minor=False)
    ax1.yaxis.set_ticks(ticks, [f"$10^{{{i}}}$" for i in ticks], minor=False)

    plt.plot([-4, 1], [-4, 1], ":k", alpha=0.25)
    plt.plot([-2, -2, -4], [-4, -2, -2], "--r")
    plt.grid("both", "minor", linestyle="-", color="lightgray", alpha=0.25)
    plt.grid("both", "major", linestyle="-", color="gray", alpha=0.5)

    xlab = ax1.set_xlabel(xtext)
    ylab = ax1.set_ylabel(ytext)
    ylab.set_usetex("True")
    ylab.set_fontsize(11)
    xlab.set_fontsize(11)

    cbar = plt.colorbar(shrink=0.6,aspect=15,location='bottom')
    cbar.ax.text(cbar.ax.get_xlim()[1],0.5,"frequency",ha='right',va='center',size=7,color='white')


    if fname:
        iostream = StringIO()
        plt.savefig(iostream, format='svg')

        iostream.seek(0)

        svgfile = minidom.parseString(iostream.read())
        iostream.close()

        imgs = svgfile.getElementsByTagName('image')
        l = [len(i.getAttribute('xlink:href')) for i in imgs]

        idx_myimg = max(range(len(l)), key=lambda i:l[i])

        myimg = imgs[idx_myimg]

        inimg = myimg.getAttribute('xlink:href')

        outimg = sp.check_output(['magick','inline:-',
                                  '-fuzz','1%',
                                  '-fill','#00000000',
                                  '-opaque','white',
                                  'inline:-'],input=inimg.encode()).decode()

        myimg.setAttribute('xlink:href',outimg)
        mygroup = myimg.parentNode

        svgroot = svgfile.getElementsByTagName('svg')[0]

        svgroot.appendChild(mygroup)


        if fname.endswith('.svg'):
            with open(fname,'w') as f:
                svgfile.writexml(f)

        else:
            iostream = StringIO()

            svgfile.writexml(iostream)
            iostream.seek(0)

            out = sp.check_output(['magick','-density','500','-',
                                   fname],input=iostream.read().encode())
    plt.show()


# --- Plot residual hitograms

def plot_cont2d(saltos, saltos_m, chans="ENU", fname=False, nbins=100,
                xtext= "Synthetic Offet Amplitude $|A|$",
                ytext="$(\hat{A}-A) sign(A)$",satura=None,ymax=2,ymin=2,xmax=1,xmin=-4,ax=None):

    channels = {"E": 0, "e": 0, "N": 1, "n": 1, "U": 2, "u": 2}

    ch = [channels[i] for i in chans]

    x = np.log10(np.abs(saltos_m[:, ch])) # histogram x axis is amplitude,
                                          # direction doesnt matter
    y = (saltos[:, ch] - saltos_m[:, ch]) * np.sign(saltos_m[:, ch])
    # We want to plot under/over estimation, we multiply by the sign of the
    # true jump to discount the effect of direction.

    mascara = (x > xmin) & (y > ymin) & (y < ymax) & (x < xmax)

    x = x[mascara]
    y = y[mascara]

    #y = np.log10(np.abs(y))

    heatmap, xedges, yedges = np.histogram2d(x, y, bins=nbins)
    extent = [xedges[0], xedges[-1], yedges[0], yedges[-1]]


    # plt.clf()
    mm = mpl.colormaps.get("magma_r").colors
    mm = mm[10:]
    mm[0] = [1, 1, 1]
    mm = mpl.colors.ListedColormap(mm)

    if ax is None:
        mapable = plt.imshow(heatmap.T, extent=extent, origin="lower", cmap=mm, vmax=satura)
    else:
        mapable = ax.imshow(heatmap.T, extent=extent, origin="lower", cmap=mm, vmax=satura)
        plt.sca(ax)
    #plt.contour(median_filter(np.log(heatmap.T),size=5), extent=extent, origin="lower", cmap=mm,interpolation='gaussian')
    if ax is None:
        ax1 = plt.gca()
    else:
        ax1 = ax

    ax1.set_aspect("auto")
    ax1.set_xlim(-4, 1)
    #ax1.set_ylim(-4, 1)

    ticks = []
    i = -4
    while i < plt.xlim()[1]:
        ticks.extend([*np.log10(np.linspace(10**i, 10 ** (i + 1), 10))])
        i += 1

    ax1.xaxis.set_ticks(ticks, minor=True)
    #ax1.yaxis.set_ticks(ticks, minor=True)

    ticks = [-4, -3, -2, -1, 0, 1]
    ax1.xaxis.set_ticks(ticks, [f"$10^{{{i}}}$" for i in ticks], minor=False)
    #ax1.yaxis.set_ticks(ticks, [f"$10^{{{i}}}$" for i in ticks], minor=False)

    #plt.plot([-4, 1], [-4, 1], ":k", alpha=0.25)
    #plt.plot([-2, -2, -4], [-4, -2, -2], "--r")
    plt.grid("both", "minor", linestyle="-", color="lightgray", alpha=0.25)
    plt.grid("both", "major", linestyle="-", color="gray", alpha=0.5)

    xlab = ax1.set_xlabel(xtext)
    ylab = ax1.set_ylabel(ytext)
    ylab.set_usetex("True")
    ylab.set_fontsize(11)
    xlab.set_fontsize(11)

    return mapable

def plot_synth_hists(ps_dset_path,nops_dset_path,G,fname=None,chans='EN'):

    data_ps_p,saltos_ps_p = get_p_data_jumps(ps_dset_path)

    data_nops_p,saltos_nops_p = get_p_data_jumps(nops_dset_path)

    fig = plt.figure(figsize=(9 / 2.54, 9 / 2.54))
    ax = fig.add_subplot(111)    # The big subplot
    ax1 = fig.add_subplot(211)
    ax2 = fig.add_subplot(212)

    #fig, (ax1,ax2) = plt.subplots(nrows=2,tight_layout=True, figsize=(9 / 2.54, 9 / 2.54), sharex=True)
    plot_cont2d(-G(data_ps_p).numpy(),saltos_ps_p,chans=chans,nbins=200,xtext='',ytext='',ymax=0.5,ymin=-1.2,ax=ax2,satura=375)
    mapable = plot_cont2d(-G(data_nops_p).numpy(),saltos_nops_p,chans=chans,nbins=200,xtext='',ytext='',ymax=0.5,ymin=-1.2,ax=ax1,satura=375)

    ax.set_ylabel(r'$\left(\hat{A}-A\right)\,sgn(A)$',labelpad=9)
    ax.set_xlabel(r'Generation Offset $|A|$')

    # Turn off axis lines and ticks of the big subplot
    ax.spines['top'].set_color('none')
    ax.spines['bottom'].set_color('none')
    ax.spines['left'].set_color('none')
    ax.spines['right'].set_color('none')
    ax.tick_params(labelcolor='w', top=False, bottom=False, left=False, right=False)

    ax1b = ax1.twinx()
    ax1b.set_yticks([])
    ax1b.set_ylabel('Without PostSeismic',color='darkgray')

    ax2b = ax2.twinx()
    ax2b.set_yticks([])
    ax2b.set_ylabel('With PostSeismic',color='darkgray')

    ax1.set_xticklabels([])

    ax1.set_ylim(-1.2,0.5)
    ax2.set_ylim(-1.2,0.5)

    cbaxes = inset_axes(ax2, width="60%", height="8%", loc=3,borderpad=2)
    cb = plt.colorbar(mapable,cax=cbaxes,orientation='horizontal')

    # TODO: calibrate
    cb.ax.text(cb.ax.get_xlim()[1],0.5,"frequency",ha='right',va='center',size=7,color='white')


    fig.subplots_adjust(hspace=0.03,top=0.95,bottom=0.15,left=0.20,right=0.92)

    if fname is None:
        plt.show()
    elif fname == 'return':
        return fig,(ax1,ax2),cb
    else:
        plt.savefig(fname,dpi=500)
        plt.close()

#
# --- Comparar Evolución a medida que aumenta la cantidad de recursiones.
#

def get_p_data_jumps(dset_path):
    ds_sy = tf.data.Dataset.load(dset_path)
    data, lab, saltos = ds_sy.batch(int(2e6)).get_single_element()
    data_p = data.numpy()[lab.numpy()[:,0].astype(bool)]
    saltos_p = saltos.numpy()[lab.numpy()[:,0].astype(bool)]

    return data_p,saltos_p

def get_p_data(dset_path):
    ds_sy = tf.data.Dataset.load(dset_path)
    data, lab, *_ = ds_sy.batch(int(2e6)).get_single_element()
    data_p = data.numpy()[lab.numpy()[:,0].astype(bool)]

    return data_p

def get_recursive_evolution(G,data_p,steps=100):
    offsets = []
    offsets_i = G(data_p)
    for i in range(steps):
        offsets.append(offsets_i.numpy())
        # plot_cont2d(saltos_test.numpy(),saltos_p,chans='EN',nbins=200,xtext='Synthetic Offset',ymax=0.5,ymin=-1.2,satura=375)
        # plt.show()
        offsets_i += G(S(data_p,offsets_i))
    return offsets

def plot_recursive_convergence(G=None,offsets=None,dset_path=None):
    if offsets is None:
        if (G is None) or (dset_path is None):
            raise ValueError('one of G or offsets is mandatory')

        data_p,saltos_p = get_p_data_jumps(dset_path)

        offsets = get_recursive_evolution(G,data_p,100)

    difs = np.array([i.flatten() for i in offsets])
    difs = difs[:-1] - difs[1:]
    plt.plot(np.mean(difs**2,axis=-1)**0.5)

    plt.show()

def plot_recursive_converging_rms(dset_path,G=None,offsets=None):
    data_p,saltos_p = get_p_data_jumps(dset_path)

    if offsets is None:
        if (G is None):
            raise ValueError('one of G or offsets is mandatory')

        offsets = get_recursive_evolution(G,data_p,100)

    rms = [np.mean((i + saltos_p)[np.abs(saltos_p) < 0.5]**2)**0.5 for i in offsets]
    plt.plot(rms)
    plt.show()

def recapply(model,datos,N):
    a_hat = model(datos)
    for i in range(N):
        a_hat += model(S(datos,a_hat))
    return a_hat

def recapplysave(model,data,N):
    result = []
    step_i = model(data)
    for i in range(N):
        result.append(step_i)
        step_i += model(S(data,step_i))
    return result

def get_A_all_steps( data_p, modelhash ):
    saltos = []
    for i in range(100):
        print(i)
        G = load_G_model_1(modelhash,i*10+9,G)
        saltos.append(G(data_p))

    return saltos

def get_recursive_A_all_steps( data_p, modelhash, N ):
    saltos_rec = []
    for i in range(100):
        print(i)
        G = load_G_model_1(modelhash,i*10+9,G)
        saltos_rec.append(recapply(G,data_p,N))

    return saltos_rec

def get_rmss_ranges(offsets,ref_offsets,limits = [0.1,0.5,1,5,10]):
    rmss = []
    vs = [(i.numpy() + ref_offsets) for i in offsets]
    for top in limits:
        mask = np.mean(ref_offsets**2, axis=-1)**0.5 < top
        vs_filtered = [v[mask] for v in vs]

        mses = [np.mean(v_f[:,:2]**2) for v_f in vs_filtered]
        # Note that only EN channels are kept in this case.

        rmss.append([mse**0.5 for mse in mses])

    return rmss

def plot_compare_rec_PS(ps_dataset_path,nops_dataset_path,modelhash,N):

    data_ps_p,saltos_ps_p = get_p_data_jumps(ps_dset_path)

    offsets_ps = get_A_all_steps( data_ps_p, modelhash )
    offsets_ps_rec = get_recursive_A_all_steps( data_ps_p, modelhash, N )

    data_nops_p,saltos_nops_p = get_p_data_jumps(nops_dset_path)

    offsets_nops = get_A_all_steps( data_ps_p, modelhash )
    offsets_nops_rec = get_recursive_A_all_steps( data_ps_p, modelhash, N )

    rmss_ps = get_rmss_ranges(offsets_ps,saltos_ps_p,limits = [0.5,5])
    rmss_nops = get_rmss_ranges(offsets_nops,saltos_nops_p,limits = [0.5,5])
    rmss_ps_rec = get_rmss_ranges(offsets_ps_rec,saltos_ps_p,limits = [0.5,5])
    rmss_nops_rec = get_rmss_ranges(offsets_nops_rec,saltos_nops_p,limits = [0.5,5])

    l = plt.plot([*zip(*rmss_ps_rec)],'x',label=[r'RMSE$|_{A < 0.5m}$ Recursive',r'RMSE$|_{A < 5m}$ Recursive'])
    l2 = plt.plot([*zip(*rmss_nops_rec)],'.',label=[r'RMSE$|_{A < 0.5m}$ Recursive no PS',r'RMSE$|_{A < 5m}$ Recursive no PS'])
    l3 = plt.plot([*zip(*rmss_ps)],'-',label=[r'RMSE$|_{A < 0.5m}$',r'RMSE$|_{A < 5m}$'])
    l4 = plt.plot([*zip(*rmss_nops)],'--',label=[r'RMSE$|_{A < 0.5m}$ no PS',r'RMSE$|_{A < 5m}$ no PS'])

    for ix,linea in enumerate(l2): linea.set_color(l[ix].get_color())
    for ix,linea in enumerate(l3): linea.set_color(l[ix].get_color())
    for ix,linea in enumerate(l4): linea.set_color(l[ix].get_color())

    plt.gca().set_yscale('log')
    plt.gca().set_xscale('log')
    plt.legend()
    plt.show()

def rmss_split(agent_list,n_rec,data,truth,axis=0):

    amp = (truth**2).sum(axis=-1)**0.5

    reduce_rms = lambda x: np.mean( ( x )**2,axis=axis)**0.5

    print('0 - 0.1 not Recursive')
    rms_i1_minor = [reduce_rms( agent(data[amp < 0.1]) + truth[amp < 0.1] ) for agent in agent_list]
    print('0.1 - 0.5 not Recursive')
    rms_i1_medior = [reduce_rms( agent(data[(amp < 0.5) & (amp > 0.1)]) + truth[(amp > 0.1) & (amp < 0.5)] ) for agent in agent_list]
    print('0.5 - 10 not Recursive')
    rms_i1_maior = [reduce_rms( agent(data[amp > 0.5]) + truth[amp > 0.5]) for agent in agent_list]

    return [ rms_i1_minor , rms_i1_medior , rms_i1_maior ]


def to_units(fl):
    if np.log10(fl) < -2:
        return '{:.1f}mm'.format(fl*1000)

    elif np.log10(fl) < 0:
        return '{:.1f}cm'.format(fl*100)

    else:
        return '{:.1f}m'.format(fl)

def plot_rmms_split_2groups(rmss_25_ps,rmss_25_nops,g1label,g2label,ax=None):
    """
    para comparar dos grupos de rmms, por ejemplo recursivo vs no recursivo
    o con q sin q
    """
    if ax is None:
        fig,ax = plt.subplots(nrows=3,sharex=True,sharey=False,
                 gridspec_kw=dict(hspace=0.04,top=0.97,right=0.95,left=0.15))
    else:
        fig = None

    for axi,[x1,y1,z1],[x2,y2,z2],ylab in zip(ax,[np.array(i).T for i in rmss_25_ps],
                                              [np.array(i).T for i in rmss_25_nops],
                                              [r'$0.0 < \|a\| < 0.1$' ,r'$0.1 < \|a\| < 0.5$',r'$0.5 < \|a\|$']):

        axi.plot(((x1**2+y1**2)/2)**0.5,'k-',label=g1label)
        axi.plot(((x2**2+y2**2)/2)**0.5,'k--',label=g2label)
        axi.set_yscale('log')
        axib = axi.twinx()
        axib.get_yaxis().set_tick_params(which='both',direction='in',pad=-40)
        #axib.set_xticks([])
        axib.set_yscale('log')
        axib.set_ylim(axi.get_ylim())
        ticks = [min(((x1**2+y1**2)/2)**0.5),min(((x2**2+y2**2)/2)**0.5)]
        ticklabs = [to_units(i) for i in ticks]
        axib.set_yticks(ticks,ticklabs,minor=False,color='brown')
        axib.set_yticks([],minor=True)
        axib.grid('y',color='red',linestyle='dotted')
        axib.set_ylabel(ylab,color='gray')
        plt.setp(axib.get_yticklabels(),backgroundcolor='#ff7f0eaa')

    ax[1].set_ylabel('RMSE [m]')
    ax[2].set_xlabel('Training step')
    ax[2].set_xticks([20,40,60,80,100])
    ax[2].set_xticklabels([200,400,600,800,1000])
    ax[0].legend()

    return fig,ax


def rmss_split_rec(agent_list,n_rec,data,truth,axis=0):

    amp = (truth**2).sum(axis=-1)**0.5

    reduce_rms = lambda x: np.mean( ( x )**2,axis=axis)**0.5

    print('0 - 0.1 not Recursive')
    rms_i1_minor = [reduce_rms( agent(data[amp < 0.1]) + truth[amp < 0.1] ) for agent in agent_list]
    print('0.1 - 0.5 not Recursive')
    rms_i1_medior = [reduce_rms( agent(data[(amp < 0.5) & (amp > 0.1)]) + truth[(amp > 0.1) & (amp < 0.5)] ) for agent in agent_list]
    print('0.5 - 10 not Recursive')
    rms_i1_maior = [reduce_rms( agent(data[amp > 0.5]) + truth[amp > 0.5]) for agent in agent_list]

    print('0 - 0.1 Recursive')
    rms_i1_minor_r = [reduce_rms( recapply(agent,data[amp < 0.1],n_rec) + truth[amp < 0.1] ) for agent in agent_list]
    print('0.1 - 0.5 Recursive')
    rms_i1_medior_r = [reduce_rms( recapply(agent,data[(amp < 0.5) & (amp > 0.1)],n_rec) + truth[(amp > 0.1) & (amp < 0.5)]) for agent in agent_list]
    print('0.5 - 10 Recursive')
    rms_i1_maior_r = [reduce_rms( recapply(agent,data[amp > 0.5],n_rec) + truth[amp > 0.5] ) for agent in agent_list]

    return {'not_recursive': [ rms_i1_minor , rms_i1_medior , rms_i1_maior ],
            'recursive': [ rms_i1_minor_r , rms_i1_medior_r , rms_i1_maior_r ]}

def rmss_vs_rec(agent_list,top_rec,amin,amax,data,truth):

    amp = (truth**2).sum(axis=-1)**0.5

    mask = (amp < amax) & (amp > amin)

    serrs = [ ( np.array([i.numpy() for i in recapplysave(agent,data[mask],top_rec)]) \
                            + [truth[mask]] )**2 for agent in agent_list ]

    img = [i.reshape(50,-1).mean(axis=-1)**0.5 for i in serrs]

    return serrs, img

def plot_split_rec(rmss):

    fig,ax = plt.subplots(figsize=(9/2.54,9/2.54),
                          gridspec_kw=dict(top=0.98,right=0.98,bottom=0.13,left=0.19))

    lines = []

    for group,color in zip(['not_recursive','recursive'],['black','red']):
        for case,style in zip(rmss[group],('-','dashed','dashdot')):
            lines.extend( plt.plot(case,linestyle=style,color=color)       )

    plt.gca().set_yscale('log')

    plt.legend(lines,
               ['','','','$\Delta < 0.1cm$','$0.1cm < \Delta < 0.5cm$','$0.5cm < \Delta$'],
               alignment='left',fontsize=7,ncols=2,
               title=' N=0     N=50', title_fontsize=7)

    plt.xlabel('Training Step')
    plt.ylabel('RMSE [m]')

    plt.show()

#
# --- funciones de costo.
#

from ast import literal_eval

def read_loss_1(fname):
    with open(fname,'r') as f:
        data = f.read()

    data = np.array(literal_eval(data)).reshape(-1,6)
    df = pd.DataFrame(data,columns=['V-train','G-train','_regTrain','V-test','G-test','_regTest'])

    return df

def read_loss_2(fname):
    with open(fname,'r') as f:
        data = f.read()

    data = np.array(literal_eval(data)).reshape(-1,10)
    df = pd.DataFrame(data,columns=['V-prov-train','V-lab-train','G-prov-train','G-lab-train','_reg-train','V-prov-test','V-lab-test','G-prov-test','G-lab-test','_reg-test'])

    return df


if __name__ == '__no_main__':

    dfsaltos = pd.read_csv('/home/javier/proy/dafne.pub/test/hectorJumps_ds_validation.csv',index_col=[0])
    saltos = dfsaltos.values

    data_p = get_p_data('/home/javier/proy/dafne.pub/test/artifacts/ds_soft/validation/')

    G = load_G_model_1('663026d5',999)

    # Recursive Application
    #plot_hist2d(-recapply(G,data_p,50).numpy(),dfsaltos.values,chans='U',truncate_map=10,nbins=70,
    #            imshow_opts={'vmax':10,'interpolation':'none'})

    #plot_hist2d(-G(data_p).numpy(),dfsaltos.values,chans='EN',truncate_map=10,nbins=70,
    #            imshow_opts={'vmax':45,'interpolation':'none'},
    #            )#fname='/home/javier/proy/dafne.pub/paperdata/manuscrito/review1/Fig-4.svg')
    plot_hist2d(-G(data_p).numpy(),dfsaltos.values,chans='EN',truncate_map=10,nbins=70,
                imshow_opts={'vmax':45,'interpolation':'none'},
                )#fname='/home/javier/proy/dafne.pub/paperdata/manuscrito/review1/figs_r1/04.Hector66EN_n.png')
    plot_hist2d(-G(data_p).numpy(),dfsaltos.values,chans='U',truncate_map=10,nbins=70,
                imshow_opts={'vmax':10,'interpolation':'none'},
                )#fname='/home/javier/proy/dafne.pub/paperdata/manuscrito/review1/figs_r1/05.Hector66U_n.png')
    plot_hist2d(-recapply(G,data_p,50).numpy(),dfsaltos.values,chans='U',truncate_map=10,nbins=70,
                imshow_opts={'vmax':10,'interpolation':'none'},
                )#fname='/home/javier/proy/dafne.pub/paperdata/manuscrito/review1/figs_s1/05.Hector66Urec_n.png')
    #plot_hist2d(-G(data_p).numpy(),dfsaltos.values,chans='U',truncate_map=10,nbins=70,
    #            imshow_opts={'vmax':10,'interpolation':'none'},
    #            )#fname='/home/javier/proy/dafne.pub/paperdata/manuscrito/review1/Fig-5.svg')

    G = load_G_model_1('f867f4fb',999)
    plot_hist2d(-G(data_p).numpy(),dfsaltos.values,chans='U',truncate_map=10,nbins=70,
                imshow_opts={'vmax':10,'interpolation':'none'},
                )#fname='/home/javier/proy/dafne.pub/paperdata/manuscrito/review1/Fig-5b.svg')
    plot_hist2d(-G(data_p).numpy(),dfsaltos.values,chans='U',truncate_map=10,nbins=70,
                imshow_opts={'vmax':10,'interpolation':'none'},
                )#fname='/home/javier/proy/dafne.pub/paperdata/manuscrito/review1/figs_r1/11.HectorU_f8_n.png')


    data_ps_p,saltos_ps_p = get_p_data_jumps('/home/javier/proy/dafne.pub/data/scripts/dsynth/validation/')

    _100models = [ load_G_model_1("25915960",i*10+18) for i in range(99) ]

    rmss = rmss_split_rec(_100models,50,data_ps_p,saltos_ps_p)

    plot_split_rec(rmss)


    serrs, img = rmss_vs_rec(_100models,50,0.0,0.1,data_ps_p,saltos_ps_p)

    plt.imshow(np.log10(np.array(img[:11])))
    plt.show()
    plt.imshow(np.log10(np.array(img[11:])))
    plt.show()

    serrs, img = rmss_vs_rec(_100models,50,0.1,0.5,data_ps_p,saltos_ps_p)

    plt.imshow(np.log10(np.array(img[:20])))
    plt.show()
    plt.imshow(np.log10(np.array(img[20:])))
    plt.show()

    serrs, img = rmss_vs_rec(_100models,50,0.5,100,data_ps_p,saltos_ps_p)

    plt.imshow(np.log10(np.array(img)))
    plt.show()


"""
if __name__ == '__main__':

    plt.rcParams["mathtext.fontset"] = "custom"
    plt.rcParams["mathtext.it"] = "Montserrat:italic"
    plt.rcParams["mathtext.rm"] = "Montserrat"
    plt.rcParams["mathtext.sf"] = "Montserrat"
    plt.rcParams["mathtext.cal"] = "jsMath\-cmsy10"

    ds = tf.data.Dataset.load("/home/javier/proy/dafne.pub/test/artifacts/ds_soft/validation/")

    a,*_ = ds.batch(100000).take(1)

    ix = a[1][:,0].numpy().astype(bool)
    a_p = a[0][ix]

    mmss = []
    j = 0
    for dat in a_p:
        print('dato {}'.format( (j := j+1) ))
        data1 = pd.DataFrame(dat[:,0].numpy(),columns=['obs'],index=[i+1 for i in range(61)])
        data2 = pd.DataFrame(dat[:,1].numpy(),columns=['obs'],index=[i+1 for i in range(61)])
        data3 = pd.DataFrame(dat[:,2].numpy(),columns=['obs'],index=[i+1 for i in range(61)])
        data1.obs[31] = np.nan
        data2.obs[31] = np.nan
        data3.obs[31] = np.nan
        mms = me.test_models([data1,data2])
        mmss.append(mms)

    df = pd.DataFrame([rankdata([m.fun for m in mms]) for mms in mmss])

    winmodels = df.idxmin(axis=1).values

    lista = []
    for dat,mms,m in zip(a_p,mmss,winmodels):
        listita = []
        for data in dat.numpy().T:
            data = pd.DataFrame(data,columns=['obs'],index=[i+1 for i in range(61)])
            data.obs[31] = np.nan
            mock, new_data = me._estimate_trend(data,
                            NoiseModels={'White':{},'Powerlaw':{}},
                            ssetanh = [],
                            useRMLE = False,
                            periodicsignals = [],
                            sampling_period = 1,
                            offsets=[31] ,
                            myio=None,
                            **me.models[m](*(np.abs(mms[m].x)+2)))
                            # the +2 comes from an obscure section on
                            # me._estimate_trend
            listita.append(mock)
        lista.append(listita)
        print(len(lista))

    saltos = [*map(lambda y: [*map(lambda x: x['jump_sizes'][0],y)],lista)]
    saltos = np.array(saltos)

    data_path = "/home/javier/proy/dafne.pub/test/artifacts/ds/show/"

    G = model2labels.deceiv()
    G = load_G_model_2("56ab2a35",999,G)

    _MODELBASE = "/home/javier/proy/dafne.pub/test/artifacts/trained/01"
    G = load_G_model_1("663026d5", 299)

    dfsaltos = pd.DataFrame(saltos,columns=['east','north','up'])
    dfsaltos.to_csv('/home/javier/proy/dafne.pub/test/hectorJumps_ds_validation.csv')

    with open('/home/javier/proy/dafne.pub/test/hectorModel_selection_ds_validation.pickle','w') as f:
        pickle.dump(mmss,file=f)
    with open('/home/javier/proy/dafne.pub/test/hectorModel_selection_ds_validation.pickle','wb') as f:
        pickle.dump(mmss,file=f)

    gen_plot(model,dset_path=data_path,fname='/home/javier/proy/dafne.pub/paperdata/manuscrito/review1/Fig-S11.svg')

    # CHequear saturacion y eso
    plot_hist2d(saltos_g66,saltos,chans='U',nbins=50,fname='/home/javier/proy/dafne.pub/paperdata/manuscrito/review1/Fig-5.svg')
    plot_hist2d(saltos_gf8,saltos,chans='U',nbins=50,fname='/home/javier/proy/dafne.pub/paperdata/manuscrito/review1/Fig-5b.svg')
    plot_hist2d(saltos_g,saltos,chans='EN',nbins=70,fname='/home/javier/proy/dafne.pub/paperdata/manuscrito/review1/Fig-4.svg')

"""
