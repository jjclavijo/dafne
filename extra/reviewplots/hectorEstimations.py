import os.path
import glob

import tensorflow as tf
import pandas as pd
import numpy as np
from scipy.stats import rankdata

from hectorpp import mock_estimatrend as me

_DATASETBASE = ['./',
                '/home/javier/proy/dafne.pub/paperdata/',
                '/home/javier/proy/dafne.pub/data/scripts',
                '/home/javier/proy/dafne.pub/test/artifacts']

def get_data_path(datapath):
    for base in _DATASETBASE:
        if glob.glob('datapath',root_dir=base):
            return os.path.join(base,datapath)
    else:
        raise ValueError('Dataset not found')

if __name__ == '__main__':

    ds = tf.data.Dataset.load(get_data_path("ds_soft/validation/"))

    a,*_ = ds.batch(100000).take(1)

    ix = a[1][:,0].numpy().astype(bool)
    a_p = a[0][ix]

    mmss = []
    j = 0
    for dat in a_p:
        print('dato {}'.format( (j := j+1) ))
        data1 = pd.DataFrame(dat[:,0].numpy(),
                             columns=['obs'],index=[i+1 for i in range(61)])
        data2 = pd.DataFrame(dat[:,1].numpy(),
                             columns=['obs'],index=[i+1 for i in range(61)])
        data3 = pd.DataFrame(dat[:,2].numpy(),
                             columns=['obs'],index=[i+1 for i in range(61)])
        data1.obs[31] = np.nan
        data2.obs[31] = np.nan
        data3.obs[31] = np.nan

        mms = me.test_models([data1,data2])
        mmss.append(mms)

    df = pd.DataFrame([rankdata([m.fun for m in mms]) for mms in mmss])

    winmodels = df.idxmin(axis=1).values

    lista = []
    for dat,mms,m in zip(a_p,mmss,winmodels):
        listita = []
        for data in dat.numpy().T:
            data = pd.DataFrame(data,columns=['obs'],index=[i+1 for i in range(61)])
            data.obs[31] = np.nan
            mock, new_data = me._estimate_trend(data,
                            NoiseModels={'White':{},'Powerlaw':{}},
                            ssetanh = [],
                            useRMLE = False,
                            periodicsignals = [],
                            sampling_period = 1,
                            offsets=[31] ,
                            myio=None,
                            **me.models[m](*(np.abs(mms[m].x)+2)))
                            # the +2 comes from an obscure section on
                            # me._estimate_trend
            listita.append(mock)
        lista.append(listita)
        print(len(lista))

    saltos = [*map(lambda y: [*map(lambda x: x['jump_sizes'][0],y)],lista)]
    saltos = np.array(saltos)

    dfsaltos = pd.DataFrame(saltos,columns=['east','north','up'])
    dfsaltos.to_csv('/home/javier/proy/dafne.pub/test/hectorJumps_ds_validation.csv')

    with open('/home/javier/proy/dafne.pub/test/hectorModel_selection_ds_validation.pickle','w') as f:
        pickle.dump(mmss,file=f)
    with open('/home/javier/proy/dafne.pub/test/hectorModel_selection_ds_validation.pickle','wb') as f:
        pickle.dump(mmss,file=f)
