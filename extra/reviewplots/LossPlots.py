import matplotlib.pylab as plt
from plothistograms_raw import read_loss_1, read_loss_2

df = read_loss_1('/home/javier/proy/dafne.pub/paperdata/trained/01/r-663026d5/l-663026d5')

fig,(ax1,ax2) = plt.subplots(nrows=2,figsize=(13/2.54,9/2.54),sharex=True,
                             gridspec_kw={'top':0.98,'bottom':0.13,'left':0.17,
                                          'right':0.98,'hspace':0.03})

df.filter(like='G').filter(like='train').iloc[:,0]\
        .plot(marker='x',color='gray',label='Train loss',ax=ax1,linestyle='none')
df.filter(like='G').filter(like='test').iloc[:,0]\
        .plot(marker='.',color='k',label='Test loss',ax=ax1,linestyle='none')

df.filter(like='V').filter(like='train').iloc[:,0]\
        .plot(marker='x',color='gray',label='Train loss',ax=ax2,linestyle='none')
df.filter(like='V').filter(like='test').iloc[:,0]\
        .plot(marker='.',color='k',label='Test loss',ax=ax2,linestyle='none')

ax1.legend()
ax1.set_ylabel(r'$G(\mathcal{D}^{*,1})$')
ax2.set_ylabel(r'$V(\mathcal{D})$')
ax2.set_xlabel('Train step')
plt.savefig('/home/javier/proy/dafne.pub/paperdata/manuscrito/review1/figs_s1/S01-ModelLoss.png',dpi=500)
plt.close()

df = read_loss_1('/home/javier/proy/dafne.pub/paperdata/trained/01/r-f867f4fb/l-f867f4fb')

fig,(ax1,ax2) = plt.subplots(nrows=2,figsize=(13/2.54,9/2.54),sharex=True,
                             gridspec_kw={'top':0.98,'bottom':0.13,'left':0.17,
                                          'right':0.98,'hspace':0.03})

df.filter(like='G').filter(like='train').iloc[:,0]\
        .plot(marker='x',color='gray',label='Train loss',ax=ax1,linestyle='none')
df.filter(like='G').filter(like='test').iloc[:,0]\
        .plot(marker='.',color='k',label='Test loss',ax=ax1,linestyle='none')

df.filter(like='V').filter(like='train').iloc[:,0]\
        .plot(marker='x',color='gray',label='Train loss',ax=ax2,linestyle='none')
df.filter(like='V').filter(like='test').iloc[:,0]\
        .plot(marker='.',color='k',label='Test loss',ax=ax2,linestyle='none')

ax1.set_ylabel(r'$G(\mathcal{D}^{*,1})$')
ax1.legend()
ax2.set_ylabel(r'$V(\mathcal{D})$')
ax2.set_xlabel('Train step')
plt.savefig('/home/javier/proy/dafne.pub/paperdata/manuscrito/review1/figs_s1/S02-ModelAugLoss.png',dpi=500)
plt.close()

df = read_loss_2('/home/javier/proy/dafne.pub/paperdata/trained/02/r-56ab2a35/l-56ab2a35')

fig,((ax1,ax2),(ax3,ax4)) = plt.subplots(nrows=2,ncols=2,figsize=(18/2.54,9/2.54),sharex=True,sharey='row',
                            gridspec_kw={'top':0.92,'bottom':0.12,'left':0.10,'right':0.92,'hspace':0.03,'wspace':0.03})

df.filter(like='prov').filter(like='G').filter(like='train').iloc[:,0].plot(marker='x',color='gray',label='Train loss',ax=ax1,linestyle='none')
df.filter(like='prov').filter(like='G').filter(like='test').iloc[:,0].plot(marker='.',color='k',label='Test loss',ax=ax1,linestyle='none')
df.filter(like='prov').filter(like='V').filter(like='train').iloc[:,0].plot(marker='x',color='gray',label='Train loss',ax=ax2,linestyle='none')
df.filter(like='prov').filter(like='V').filter(like='test').iloc[:,0].plot(marker='.',color='k',label='Test loss',ax=ax2,linestyle='none')


df.filter(like='lab').filter(like='G').filter(like='train').iloc[:,0].plot(marker='x',color='gray',label='Train loss',ax=ax3,linestyle='none')
df.filter(like='lab').filter(like='G').filter(like='test').iloc[:,0].plot(marker='.',color='k',label='Test loss',ax=ax3,linestyle='none')
df.filter(like='lab').filter(like='V').filter(like='train').iloc[:,0].plot(marker='x',color='gray',label='Train loss',ax=ax4,linestyle='none')
df.filter(like='lab').filter(like='V').filter(like='test').iloc[:,0].plot(marker='.',color='k',label='Test loss',ax=ax4,linestyle='none')

ax1.set_title(r'$G(\mathcal{D})$',fontsize=10)
ax4.legend()
ax2.set_title(r'$V(\mathcal{D})$',fontsize=10)
#ax3.set_xlabel('Train step')
#ax3.set_ylabel('loss')
#ax4.set_xlabel('Train step')
#ax1.set_ylabel('loss')

ax2b = ax2.twinx()
ax2b.set_yticks([])
ax2b.set_ylabel('Provenance output')
ax4b = ax4.twinx()
ax4b.set_yticks([])
ax4b.set_ylabel('Label output')

ax = fig.add_subplot(111)
# Turn off axis lines and ticks of the big subplot
ax.spines['top'].set_color('none')
ax.spines['bottom'].set_color('none')
ax.spines['left'].set_color('none')
ax.spines['right'].set_color('none')
ax.tick_params(labelcolor='none', top=False, bottom=False, left=False, right=False)
ax.set_facecolor('none')

ax.set_xlabel('Train step')
ax.set_ylabel('loss')
position = [*ax.get_position().bounds]
position[1] = 0.12
ax.set_position(position)

plt.savefig('/home/javier/proy/dafne.pub/paperdata/manuscrito/review1/figs_s1/S03-AlternativeModelLoss.png',dpi=500)
plt.close()

fig, ax= plt.subplots(figsize=(13/2.54,7/2.54),sharex=True,sharey='row', gridspec_kw={'top':0.92,'bottom':0.18,'left':0.13,'right':0.98,'hspace':0.03,'wspace':0.03})

df.filter(like='reg').filter(like='train').iloc[:,0].plot(marker='x',color='gray',label='Train loss',ax=ax,linestyle='none')

df.filter(like='reg').filter(like='test').iloc[:,0].plot(marker='.',color='k',label='Test loss',ax=ax,linestyle='none')

ax.set_ylabel(r'$\|a_\theta(\mathcal{D})-a_\theta[g_\theta(\mathcal{D})]\|_{l2}$',fontsize=10)
ax.legend()
ax.set_xlabel('Train step')

plt.savefig('/home/javier/proy/dafne.pub/paperdata/manuscrito/review1/figs_s1/S04-AlternativeModelRegularization.png',dpi=500)
plt.close()
