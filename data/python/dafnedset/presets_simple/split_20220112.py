"""
A pre-set for positive samples

- Query samples
- split in Train, Test, Val, Show

"""

from .. import datasets_simple as defaultQuerys
from .. import fun_ops as fop
from .. import fun_transformations as ftr
from os import path

def read_db():
    rawP = fop.read_db(defaultQuerys.CASES)

    rawP = fop.map(rawP,ftr.label_batch([1]))
    rawP = fop.map(rawP,ftr.scale())

    # Tremendo side efect!
    rawP.options.batch_size = 200

    partsP = fop.split(rawP,[0.7,0.15,0.145,0.005]) # Train, Test, Val, show

    """
    A preset for negative samples

    - Query samples
    - splits in train test val and show

    - For training, drops epochs according to nan_distriburion

    """

    rawN = fop.read_db(defaultQuerys.NO_CASES)

    rawN = fop.map(rawN,ftr.label_batch([0]))
    rawN = fop.map(rawN,ftr.scale())

    # Tremendo side efect!
    rawN.options.batch_size = 200

    partsN = fop.split(rawN,[0.7,0.15,0.145,0.005]) # Train, Test, Val, show

    """
    # nr of nan    0     1     2     3     4     5     6     7     8     9
    nan_dist = [5726,  524,  185,  117,   86,   71,   40,   44,   22,   12]
    drop = ftr.drop_epochs(distribution=nan_dist)

    data = fop.map(parts[0],drop)

    data = fop.map( data,ftr.scale() )
    """
    return partsP,partsN

def read_pos(directorio):
    files = ['train+.pq',
             'test+.pq',
             'val+.pq',
             'show+.pq']
    parts = []
    for fname in files:
        parts.append(fop.read_parquet(path.join(directorio,fname)))

    return parts

def read_neg(directorio):
    files = ['train-.pq',
             'test-.pq',
             'val-.pq',
             'show-.pq']
    parts = []
    for fname in files:
        parts.append(fop.read_parquet(path.join(directorio,fname)))

    return parts

if __name__ == "__main__":
    import pyarrow as pa

    partsP,partsN = read_db()

    files = {'train+.pq':partsP[0],
             'test+.pq':partsP[1],
             'val+.pq':partsP[2],
             'show+.pq':partsP[3],
             'train-.pq':partsN[0],
             'test-.pq':partsN[1],
             'val-.pq':partsN[2],
             'show-.pq':partsN[3]}

    for k,v in files.items():
        tabla = pa.Table.from_batches(v)
        pa.parquet.write_table(tabla,k)


