import pyarrow as pa
from tfx_bsl.public import tfxio
from tensorflow_metadata.proto.v0.schema_pb2 import TensorRepresentation, FixedShape
from tensorflow import stack

# Para test-ear
# import pyarrow as pa
# import numpy as np
# import pandas as pd
# pd.DataFrame({'norte':[np.random.rand(61) for i in range(9)]})
# df = pd.DataFrame({'norte':[np.random.rand(61) for i in range(9)]})
# pa.RecordBatch.from_pandas(df)
# rb = pa.RecordBatch.from_pandas(df)

dim = FixedShape(dim=[FixedShape.Dim(size=61)])
dim_l = FixedShape(dim=[FixedShape.Dim(size=2)])
dim_2 = FixedShape(dim=[FixedShape.Dim(size=3)])

norte = TensorRepresentation.DenseTensor(column_name='norte',shape=dim)
este = TensorRepresentation.DenseTensor(column_name='este',shape=dim)
altura = TensorRepresentation.DenseTensor(column_name='altura',shape=dim)
etiqueta = TensorRepresentation.DenseTensor(column_name='etiqueta',shape=dim_l)
saltos = TensorRepresentation.DenseTensor(column_name='saltos',shape=dim_2)

rep_etiquetadas_saltos =  {'norte':TensorRepresentation(dense_tensor =
                TensorRepresentation.DenseTensor(column_name='norte',shape=dim)),
                    'este':TensorRepresentation(dense_tensor =
                TensorRepresentation.DenseTensor(column_name='este',shape=dim)),
                    'altura':TensorRepresentation(dense_tensor =
                TensorRepresentation.DenseTensor(column_name='altura',shape=dim)),
                    'etiqueta':TensorRepresentation(dense_tensor =
                TensorRepresentation.DenseTensor(column_name='etiqueta',shape=dim_l)),
                    'saltos':TensorRepresentation(dense_tensor =
                TensorRepresentation.DenseTensor(column_name='saltos',shape=dim_2))}

rep_etiquetadas =  {'norte':TensorRepresentation(dense_tensor =
                TensorRepresentation.DenseTensor(column_name='norte',shape=dim)),
                    'este':TensorRepresentation(dense_tensor =
                TensorRepresentation.DenseTensor(column_name='este',shape=dim)),
                    'altura':TensorRepresentation(dense_tensor =
                TensorRepresentation.DenseTensor(column_name='altura',shape=dim)),
                    'etiqueta':TensorRepresentation(dense_tensor =
                TensorRepresentation.DenseTensor(column_name='etiqueta',shape=dim_l))
                    }

rep_crudas =       {'norte':TensorRepresentation(dense_tensor =
                TensorRepresentation.DenseTensor(column_name='norte',shape=dim)),
                    'este':TensorRepresentation(dense_tensor =
                TensorRepresentation.DenseTensor(column_name='este',shape=dim)),
                    'altura':TensorRepresentation(dense_tensor =
                TensorRepresentation.DenseTensor(column_name='altura',shape=dim))
                    }

def adapter(arrowSchema: pa.Schema) -> tfxio.TensorAdapter:
    if ('etiqueta' in arrowSchema.names) and ('saltos' in arrowSchema.names):
        ta = tfxio.TensorAdapterConfig(arrowSchema,rep_etiquetadas_saltos)
        transformer = lambda d: (stack((d['norte'],d['este'],d['altura']),axis=-1),d['etiqueta'],d['saltos'])
    elif 'etiqueta' in arrowSchema.names:
        ta = tfxio.TensorAdapterConfig(arrowSchema,rep_etiquetadas)
        transformer = lambda d: (stack((d['norte'],d['este'],d['altura']),axis=-1),d['etiqueta'])
    else:
        ta = tfxio.TensorAdapterConfig(arrowSchema,rep_crudas)
        transformer = lambda d: (stack((d['norte'],d['este'],d['altura']),axis=-1),)

    return tfxio.TensorAdapter(ta), transformer
