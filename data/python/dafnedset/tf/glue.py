"""
Funcione para convertir un dataset a tensorflow
"""

from .dataspec import adapter as tfAdapter
from ..base_simple import FunBuffer
from tensorflow.data import Dataset
from tensorflow import stack

PRACTICAMENTE_IFTO = 1e10

def dafneFbToTf(p: FunBuffer) -> Dataset:

    #p = neg10.parts[0] + pos10.parts[0]

    p.options.batch_size = PRACTICAMENTE_IFTO

    for ix,i in enumerate(p):
        # Como lo vamos a Iterar, le tomamos el schema de ahí
        if ix == 0:
            schema = i.schema
        else:
            assert False # with batchsize 1e10 this must be not reached

        adp,transformer = tfAdapter(i.schema)
        data = adp.ToBatchTensors(i)

    dset = Dataset.from_tensor_slices(data).map(transformer)#.shuffle(int(1e7))

    return dset

