#!/bin/zsh

DATA_VER=20.01

install_haskell_program () { CONT=$1

  buildah run $CONT apt-get update
  buildah run $CONT apt-get -y -qq install $(cat ../haskell/artifacts/required.list)

  MPOINT=$(buildah mount $CONT)

  mkdir -p $MPOINT/opt/bin

  cp ../haskell/build/bin/* $MPOINT/opt/bin/
  #cp -r hs.readdata/data $MPOINT/opt/

  buildah umount $CONT
}

copy_datafiles () { CONT=$1

  MPOINT=$(buildah mount $CONT)
  # Container with the data files
  CONTDATA=$(buildah from --pull registry.gitlab.com/jjclavijo/dafne/data:${DATA_VER})
  MOUNTDATA=$(buildah mount $CONTDATA)

  # Copy db scripts
  cp -r $MOUNTDATA/data/http-nevada $MPOINT/data

  buildah umount $CONT
  buildah umount $CONTDATA
  buildah rm $CONTDATA
}

#CONT=$(buildah from docker.io/debian:bullseye-slim)
#
#install_haskell_program $CONT
#
#buildah commit $CONT dafne/query:base
#buildah rm $CONT
#
#CONT=$(buildah from dafne/query:base)

#copy_datafiles $CONT

CONT=$(buildah from dafne/data:$DATA_VER)
install_haskell_program $CONT

buildah run $CONT ln -s /dat/http-nevada /data

buildah config --cmd "" $CONT
buildah config --entrypoint '["/opt/bin/proy"]' $CONT

buildah commit $CONT dafne/query:data-${DATA_VER}
buildah rm $CONT
