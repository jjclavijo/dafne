#!/bin/zsh

install_haskell_program () { CONT=$1

  buildah run $CONT apt-get update
  buildah run $CONT apt-get -y -qq install $(cat ../haskell/artifacts/required.list)

  MPOINT=$(buildah mount $CONT)

  mkdir -p $MPOINT/opt/bin

  cp ../haskell/build/bin/* $MPOINT/opt/bin/
  #cp -r hs.readdata/data $MPOINT/opt/

  buildah umount $CONT
}

CONT=$(buildah from docker.io/debian:bullseye-slim)

install_haskell_program $CONT

buildah commit $CONT dafne/query:base
buildah rm $CONT
