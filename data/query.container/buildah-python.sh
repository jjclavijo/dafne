#!/bin/zsh

install_haskell_program () { CONT=$1

  buildah run $CONT apt-get update
  buildah run $CONT apt-get -y -qq install $(cat ../haskell/artifacts/required.list)

  MPOINT=$(buildah mount $CONT)

  mkdir -p $MPOINT/opt/bin

  cp ../haskell/build/bin/* $MPOINT/opt/bin/
  #cp -r hs.readdata/data $MPOINT/opt/

  buildah umount $CONT
}

install_python_dafnedset () { CONT=$1

  TMP_CONT=$(buildah from docker.io/python:3.7-slim)
  TMP_MPOINT=$(buildah mount $TMP_CONT)

  buildah run $TMP_CONT python -m venv /opt/venv

  #Use devpi container
  #cp pip.conf $TMP_MPOINT/opt/venv

  buildah run $TMP_CONT /opt/venv/bin/python -m pip install -U pip wheel
  buildah run $TMP_CONT /opt/venv/bin/python -m pip install flit tensorflow tfx_bsl

  cp -r ../python $TMP_MPOINT/opt/py.dset

  buildah run $TMP_CONT sh -c 'cd /opt/py.dset ;env FLIT_ROOT_INSTALL=1 /opt/venv/bin/python -m flit install -s'
  
  # buildah commit $TMP_CONT dafne/tmppython
  # TMP_CONT=$(buildah from dafne/tmppython)
  # TMP_MPOINT=$(buildah mount $TMP_CONT)

  # Transfer virtualenv in chunks to generate separate layers
  # this is only to overcome connection problems when pushing large
  # layers to the container registry
  
  # This block could be replaced by something like
  # rsync -ar $TMP_MPOINT/opt/venv/ $MPOINT/opt/venv 
  
  tmpdir=$(mktemp -d)
  pushd $tmpdir 
  {cd $TMP_MPOINT/opt/ ;\
   fd -0 -tf -tl -H '' venv |\
   xargs -0 -n1 stat -c '%n|%s'} |\
   awk -F\| 'BEGIN{bloque=0};
        {i=i+$2/(1024^2);
         if(int(i / 200) != bloque){print "FIN DE BLOQUE";
                                    bloque=int(i / 200)};
         sub(/^venv\//,"",$1)
         print $1;
         if($2/(1024^2) > 200){print "FIN DE BLOQUE"};
        }' |\
   csplit --suppress-matched - "/FIN DE BLOQUE/" '{*}'

  for i in x*
  do
    MPOINT=$(buildah mount $CONT)
    rsync -0 -a --files-from=<(cat $i | tr '\n' '\0') $TMP_MPOINT/opt/venv/ $MPOINT/opt/venv
    buildah umount $CONT
    buildah commit $CONT dafne/query:python
    buildah rm $CONT
    CONT=$(buildah from dafne/query:python)
  done
  popd

  # End transfering venv


  MPOINT=$(buildah mount $CONT)
  cp -r ../python $MPOINT/opt/py.dset

  buildah umount $TMP_CONT
  buildah rm $TMP_CONT
}

CONT=$(buildah from docker.io/python:3.7-slim)

#install_haskell_program $CONT
install_python_dafnedset $CONT

buildah config --cmd "" $CONT
buildah config --entrypoint '["/opt/venv/bin/python"]' $CONT

buildah commit $CONT dafne/query:python
buildah rm $CONT
