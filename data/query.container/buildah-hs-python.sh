#!/bin/zsh

install_haskell_program () { CONT=$1

  buildah run $CONT apt-get update
  buildah run $CONT apt-get -y -qq install $(cat ../haskell/artifacts/required.list)

  MPOINT=$(buildah mount $CONT)

  mkdir -p $MPOINT/opt/bin

  cp ../haskell/build/bin/* $MPOINT/opt/bin/
  #cp -r hs.readdata/data $MPOINT/opt/

  buildah umount $CONT
}

install_python_dafnedset () { CONT=$1

  TMP_CONT=$(buildah from docker.io/python:3.7-slim)
  TMP_MPOINT=$(buildah mount $TMP_CONT)
  MPOINT=$(buildah mount $CONT)

  buildah run $TMP_CONT python -m venv /opt/venv
  buildah run $TMP_CONT /opt/venv/bin/python -m pip install -U pip wheel
  buildah run $TMP_CONT /opt/venv/bin/python -m pip install flit tensorflow tfx_bsl

  cp -r ../python $TMP_MPOINT/opt/py.dset

  buildah run $TMP_CONT sh -c 'cd /opt/py.dset ;env FLIT_ROOT_INSTALL=1 /opt/venv/bin/python -m flit install -s'

  MPOINT=$(buildah mount $CONT)

  cp -r --preserve=all $TMP_MPOINT/opt/venv $MPOINT/opt/
  cp -r ../python $MPOINT/opt/py.dset

  buildah umount $CONT
  buildah umount $TMP_CONT

  buildah rm $TMP_CONT
}

CONT=$(buildah from docker.io/python:3.7-slim)

install_haskell_program $CONT
install_python_dafnedset $CONT

buildah config --cmd "" $CONT
buildah config --entrypoint '["/opt/venv/bin/python"]' $CONT

buildah commit $CONT dafne/query:all
buildah rm $CONT
