#!/bin/zsh

#Prepare haskell environment
#Nothing depends on sources here

CONT=$(buildah from docker.io/debian:bullseye-slim)

buildah run $CONT apt-get update

MPOINT=$(buildah mount $CONT)

BASE_URL='https://downloads.haskell.org/debian/pool/main'

GHC_VER='9.0.1'
GHC_REL='9.0.1-10~deb10_amd64'

CABAL_VER='3.6'
CABAL_REL='3.6%2Bgit20220303.2.6046687-6~deb10_amd64'


curl -J -O "${BASE_URL}/g/ghc-${GHC_VER}/ghc-${GHC_VER}_${GHC_REL}.deb"
curl -J -O "${BASE_URL}/c/cabal-install-${CABAL_VER}/cabal-install-${CABAL_VER}_${CABAL_REL}.deb"

mv "ghc-${GHC_VER}_${GHC_REL}.deb"\
   "cabal-install-${CABAL_VER}_${CABAL_REL}.deb" \
   $MPOINT/

buildah run $CONT apt -q -y install \
  "./ghc-${GHC_VER}_${GHC_REL}.deb" \
  "./cabal-install-${CABAL_VER}_${CABAL_REL}.deb" \
  --no-install-recommends

rm "$MPOINT/ghc-${GHC_VER}_${GHC_REL}.deb" 
   "$MPOINT/cabal-install-${CABAL_VER}_${CABAL_REL}.deb" 

pushd $MPOINT
ln -s /opt/cabal/bin/cabal bin/cabal
ln -s /opt/ghc/bin/ghc bin/ghc
popd

buildah commit $CONT dafne/haskell:base

buildah umount $CONT
buildah rm $CONT
