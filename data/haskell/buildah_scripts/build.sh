#!/bin/zsh

second_step () {
    # Installing required C Libs and etceteras.
    # No static building possible because of postgres.
    
    CONT=$(buildah from dafne/haskell:base)

    POSTGRES_VER='13'

    #TODO: Versioning postgres server and liblzma

    buildah run $CONT apt-get -qq -y update
    buildah run $CONT apt-get -qq -y install liblzma-dev postgresql-server-dev-$POSTGRES_VER

    buildah commit $CONT dafne/haskell:libs

    buildah rm $CONT
  }

third_step () {
    # Actual Building 
    
    CONT=$(buildah from dafne/haskell:libs)
    MPOINT=$(buildah mount $CONT)

    mkdir -p $MPOINT/opt/proyect/

    cp -r app src proy.cabal $MPOINT/opt/proyect/

    buildah run $CONT sh -c "cabal update; cd /opt/proyect/; cabal new-build"

    mkdir -p build/bin

    cp $(find $MPOINT/opt/proyect/dist-newstyle -type f -name "chunk" -exec ls -l {} \; | awk '($1 ~ /x/){print $9}') build/bin/
    cp $(find $MPOINT/opt/proyect/dist-newstyle -type f -name "proy" -exec ls -l {} \; | awk '($1 ~ /x/){print $9}') build/bin/

    buildah commit $CONT dafne/haskell:cache
    buildah umount $CONT
    buildah rm $CONT
  }

fourth_step () {
    # Clean the proyect Cache, but keep all haskell stuff for recompilation
    
    CONT=$(buildah from dafne/haskell:cache)
    MPOINT=$(buildah mount $CONT)

    rm -r $MPOINT/opt/proyect/

    buildah commit $CONT dafne/haskell:cache-clean
    buildah umount $CONT
    buildah rm $CONT
  }

echo -------------- 1 --------------
#second_step

echo -------------- 2 --------------
third_step

echo -------------- 3 --------------
fourth_step
