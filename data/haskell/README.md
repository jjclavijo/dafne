This application read the xz compressed tenv3 files and extract chunks from
them.

It also communicates with the database of time availability, and builds querys for
seismic events (for maps).

It was done in haskell both as an excersize and because I/O is slightly faster
than python's. (at-least in the tests i made)

No operations are performed on the data besides reading and reformatting.

Usually i run the application from a container, thats why the data folder is
hardcoded to /data, example call:

```
podman run --rm --pod dafne -v "$(realpath data)":/data dafnepython:venv /opt/bin/proy -e 1096 --dist 1000000 --days 1
```

Compilation is done on containers too, using the scripts on buildah_scripts.

As long as the runtime containers are based on the same images, and the needed
libraries are installed, it should run.

In order to know what packages are needed on apt based distributions, the build
process produces a required.list file of packages.
