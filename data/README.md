# Dataset preparation

All data managing is in this folder.

For inspecting the internals of the data managing, a possible starting point are the
scripts for building the containers.

- Building of the geospacial database used for indexing of the data, and spatio-temporal selection of samples, [docker-compatible Container, using buildah](db.containers/buildah.init-index.sh)
- data loading of the samples, [inside a docker-compatible Container, using buildah](db.containers/buildah.dataload.sh)
- [Python module](python) used for dataset building and augmentation
- [Python code](scripts) used for dataset building and augmentation
- [Haskell program](haskell) to read data from the raw files into a json that can be used
  to feed the trained models.
- [A container](query.container) prepared with the data, haskell and python programs installed

The raw data files are in general also loaded from a container image, this is
merely due to the convenience of overlay filesystems for working on temporal
experiments. See the preparation of that container in [data.container](data.container)

**For indexing the data, the scripts used are in [db.containers/scripts.indexing/](db.containers/scripts.indexing/)**

For data loading, the scripts used are in [db.containers/scripts.dataload/](db.containers/scripts.dataload/)

The python module has many features that allowed experimentation with several
data augmentation schemes. However, most of the code in this module has no
effect in the final augmentation schemes. The testing of scripts of each part
of the code were not well organized and thus are not published, pleas don't
hesitate in contacting us if testing of some section is needed.

The haskell code consists mainly on text parsing and querying of the database,
we used haskell instead of shell scripts for performance issues
(decompression and parsing in one step) and easy containerization of the
resulting binary.

For rebuilding the containers locally, we recomend first downloading the data
container

```
podman pull registry.gitlab.com/jjclavijo/dafne/data:20.01
```

And then running the scripts using `buildah unshare` from `db.containers`

```
buildah unshare ./buildah.dataload.sh
buildah unshare ./buildah.init-index.sh
```

If new data must be incorpored, one can donwload the new tenv files, rebuild the
data container [with the provided scripts](data.container), and edit the
buildings scripts to use this new image.
