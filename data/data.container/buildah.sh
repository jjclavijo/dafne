#!/bin/zsh

DATA_VER=20.01

CONT=$(buildah from docker.io/debian:bullseye-slim)
MPOINT=$(buildah mount $CONT)

echo Copying Files
mkdir -p $MPOINT/dat/http-nevada

while read a
do
  echo $a | tr ' ' '\n' | xargs -I{} cp {} $MPOINT/dat/http-nevada
  buildah commit $CONT dafne/data:$DATA_VER
  buildah rm $CONT
  CONT=$(buildah from dafne/data:$DATA_VER)
  MPOINT=$(buildah mount $CONT)
done < <(find http-nevada/ | xargs -n 1000)

cp -r terremotos $MPOINT/dat/terremotos
cp -r puntos-http.lonlath $MPOINT/dat/

buildah umount $CONT
echo buildah commit $CONT dafne/data:$DATA_VER
buildah commit $CONT dafne/data:$DATA_VER
buildah rm $CONT
