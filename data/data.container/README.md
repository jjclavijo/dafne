# Dafne-db data

## Downloading data and building container from http://geodesy.unr.edu

Image with data up to 2020-01-17 is online, and is the one used on the paper.

The data downloading process can be launched with the provided makefile.

Be responsable and don't overload the servers.
The actual download lines are commented to enforce consciousnes.

La imagen es una imagen debian:buster-slim

Se le agregan en el directorio data/http-nevada los archivos .tenv3 descargados
para todas las estaciones.

También se agrega en el directorio data/terremotos un archivo gejson que
contiene los sismos mayores a 5.5 Mw hasta la fecha misma de los datos.

El makefile tiene todo lo necesario para descargar los datos:

```bash
make data
make terremotos/5.5.pgdump
```

Usar con cuidado, la descarga implica algunos GB, y no se debe cargar al
servidor sin motivo

## Cita a los datos:
Blewitt, G., W. C. Hammond, and C. Kreemer (2018), Harnessing the GPS data explosion for interdisciplinary science, Eos, 99, https://doi.org/10.1029/2018EO104623.
