# Data preparation scripts

The real data dataset for the paper was prepared with the script
`00-Dataset_prep_soft.py`, the dataset with destructive data augmentation was
prepared with the script  `01-Dataset_prep.py`, and synthetic datasets with the
script `02-Synthetic_Data.py`

This script needs access to the  db with the data loaded.
See [this section](/#pre-processing-and-dataset-building) for an explanation of
how to do this on podman.

After starting the db container in a pod named dafne, we run for example:

```
podman run -t -i --pod dafne --rm -v $(realpath .):/wdir -w=/wdir dafne/query:all 00-Dataset_prep.py
```

Note that some operations rely on tfx_bsl, which is unavailable for python 3.10,
this is the only portion of the paper code that has version conflicts and thus
we only run inside containers.

The script `02-Synthetic_Data.py` has options for creating a dataset with or
without postseismic transients (`--postseismic T` or `--postseismic F`) and to
use a jump distribution that mimic the real data (`--jumps_pdf real_data`), a
uniform distribution or one that produces a plain histogram when the bins are
in logarithmic scale (`--jumps_pdf log_plain`)

Options for each synthetic dataset generated are:

```
python 02-Synthetic_Data.py --base_out_dir dsynth \
                            --nsamples 100000 --postseismic T
python 02-Synthetic_Data.py --base_out_dir dsynth_no_ps\
                            --nsamples 100000 --postseismic F
python 02-Synthetic_Data.py --base_out_dir dsynth_hist --jumps_pdf log_plain\
                            --nsamples 100000 --postseismic T --only_positive T
python 02-Synthetic_Data.py --base_out_dir dsynth_hist_no_ps --jumps_pdf log_plain\
                            --nsamples 100000 --postseismic F --only_positive T
```
