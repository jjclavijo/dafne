# Hide Tensorflow Annoing warnings and info mesages.
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

from tensorflow.compat.v1 import logging as tflogging
tflogging.set_verbosity(tflogging.ERROR)

import logging

logger = logging.getLogger("DataGeneration")

BUFFER = int(2e6)
BS = int(4096)

# Constants:

TRAY_SIGMA = 0.01
ANNUAL_SIGMA = 0.01
SA_SIGMA = 0.01
ANNUAL_SIGMA_U = 0.03
SA_SIGMA_U = 0.02
NOISE_RANGE = [0.005,0.03]
MAX_PS_LONG_DECAY = 100
MAX_PS_SHORT_DECAY = 10

import numpy as np

import dafnedset.tf.glue as glue
from dafnedset import fun_ops as fop
from dafnedset import fun_transformations as ftr

from dafnedset.helpers import FuncGen
import dafnedset.datasets_simple as ds


import hectorpp.simulatetrajectory as hpt
from itertools import repeat

from types import SimpleNamespace

# --- Read sample length (for each group) from commandline
#try:
#    from sys import argv
#    Nsamples = int(argv[1])
#except IndexError:
#    Nsamples = 100000
#except ValueError:
#    raise ValueError("Wrong argument from CLI")


# --- default Samplers for common parameters

# Noise amplitude (combined)
sigma_sampler = lambda: np.random.uniform(*NOISE_RANGE)

# Trayectory signals amplitudes (linear, annual and semiannual)
ts = lambda: np.random.normal(0, TRAY_SIGMA)
ans = lambda: np.random.normal(0, ANNUAL_SIGMA)
sas = lambda: np.random.normal(0, SA_SIGMA)
# up component has heavyer seasonal signals.
ans_u = lambda: np.random.normal(0, ANNUAL_SIGMA_U)
sas_u = lambda: np.random.normal(0, SA_SIGMA_U)

# --- negative samples

def sample_negatives(N):
    samples_e_n = [
        hpt.sample_tray(61, trend=ts, annual=ans, sannual=sas)
        + hpt.sample_noise(61, sigma=sigma_sampler)
        for i in range(2*N)
    ]

    samples_u = [
        hpt.sample_tray(61, trend=ts, annual=ans_u, sannual=sas_u)
        + hpt.sample_noise(61, sigma=sigma_sampler)
        for i in range(N)
    ]

    jumps_ = repeat(np.zeros(3), N)

    iterator = zip((it := iter(samples_e_n)), it, samples_u, jumps_).__next__

    samplesFb_neg = ds.DefaultQuerys.fake(
        iterator, N, BS, ["norte", "este", "altura", "saltos"], repeat(list)
    )

    return samplesFb_neg

# --- Positive Samples

# Jumps are sampled with an aproximate histogram shape.

# First create a function defining the histogram shape
# this is, a peak on a few milimeters and a log(y) = a * log(x) + b
# with a variation of 2 orders of magnitud of variation from 1cm to 10m.
real_data_pdf = lambda x: 10**(-(5*(np.minimum(np.log10(x),-2.5)+2.5)**2) + (25/16)- (9/16)*np.log10(x))

def sample_jumps(N,n_overflow = 2,pdf = real_data_pdf):
    """
    Sample jumps with an histogram symilar to pdf
    will sample n_overflow * N samples,
    one is meant to keep only Nsamples after that.

    return a list of samples
    """

    # Create the pdf function.

    #Compute number of samples per bin (unnormalized)
    bins = 10**np.linspace(-4,1,100)
    centros = (bins[1:]+bins[:-1])/2
    n_samps = [*map(pdf,centros)]

    # normalize the histogram to be a discrete distribution.
    n_samps /= np.sum(n_samps)

    # Sample uniformly inside each bin.
    j_samples = []
    for x,y,n in zip(bins[:-1],bins[1:],n_samps):
        j_samples.extend(np.random.rand(int(n_overflow*np.ceil(n*N)))*(y-x)+x)

    # Shuffle the sampled jumps
    np.random.shuffle(j_samples)

    return j_samples

def sample_positives(N, jumps_sampler=sample_jumps,
                     postseismic=True):
    """
    Get positive samples

    N: number of samples
    jumps_sampler: function Int -> List(float)[N] :: to sample jump amplitudes
    postseismic: bool :: include postseismic relaxation term.
    """

    # Sample Amplitudes
    j_samples = jumps_sampler(3*N)

    # Sample Signs
    j_samples = j_samples[:3*N] * np.random.choice([-1,1],size=3*N)

    jumps_sampler = iter(j_samples).__next__

    if postseismic:
        # Parameters for exponential decay. Given the ammount os displacement of an
        # earthquake, use a percent of that value and sample a decay

        percent_sampler = lambda: np.random.uniform(0.05, 0.5) * np.sign(
            np.random.uniform(-1, 1)
        )

        # Example of short decay
        s_decay_sampler = lambda: np.random.uniform(1, MAX_PS_SHORT_DECAY)
        # Example of long decay
        l_decay_sampler = lambda: np.random.uniform(MAX_PS_SHORT_DECAY, MAX_PS_LONG_DECAY)

    samples_e_n = [
        hpt.sample_step(61, 31, (amp := jumps_sampler()))
        +( hpt.sample_post_seismic(61, 31, amp, percent_sampler, l_decay_sampler)\
          +hpt.sample_post_seismic(61, 31, amp, percent_sampler, s_decay_sampler)
          if postseismic else 0)
        + hpt.sample_tray(61, trend=ts, annual=ans, sannual=sas)
        + hpt.sample_noise(61, sigma=sigma_sampler)
        for i in range(2*N)
    ]

    samples_u = [
        hpt.sample_tray(61, trend=ts, annual=ans, sannual=sas)
        + hpt.sample_step(61, 31, (amp := jumps_sampler()))
        +( hpt.sample_post_seismic(61, 31, amp, percent_sampler, l_decay_sampler)\
          + hpt.sample_post_seismic(61, 31, amp, percent_sampler, s_decay_sampler)
          if postseismic else 0)
        + hpt.sample_noise(61, sigma=sigma_sampler)
        for i in range(N)
    ]

    jumps_ = map(np.array, zip((it := iter(j_samples)), it, j_samples[-N:]))

    iterator = zip((it := iter(samples_e_n)), it, samples_u, jumps_).__next__

    samplesFb_pos = ds.DefaultQuerys.fake(
        iterator, N, BS, ["norte", "este", "altura", "saltos"], repeat(list)
    )

    return samplesFb_pos

import click
@click.command
@click.option('--nsamples', default = 100000)
@click.option('--postseismic', default = True)
@click.option('--one_part', default = False)
@click.option('--only_positive', default = False)
@click.option('--base_out_dir', default = './')
@click.option('--jumps_pdf', default = 'real_data',
              type=click.Choice(['real_data', 'log_plain', 'plain'],
              case_sensitive=False))
def main(nsamples,postseismic,one_part,only_positive,base_out_dir,jumps_pdf):
    """
    Program to generate sample datasets.

    CLI options are meant to generate the variants used on the paper.

    --one_part T ::  generates a single raw_samples dataset of the desired size
    --one_part F ::  (default) generates a dataset parted in
                     train,test,validate,show. The train dataset is augmented.
    --only_positive T :: (implies one_part) only generate positive samples,
                         used for some graphics on the paper

    --jumps_pdf real_data/log_plain/plain :: sample jumps with different
                                             histogram shape, for
                                             plotting histograms use plain or
                                             log_plain, for traing use
                                             real_data.

    --postseismic T/F :: enable/disable postseismic transient functions.
    """

    if only_positive:
        if not one_part:
            one_part = True
            logger.warning("only_positive option implies one_part")

    if jumps_pdf == 'real_data':
        raw = sample_positives(nsamples, postseismic=postseismic)
    elif jumps_pdf == 'log_plain':
        js = lambda N: 10**np.random.uniform(-4,1,N)
        raw = sample_positives(nsamples, postseismic=postseismic,
                               jumps_sampler=js)
    elif jumps_pdf == 'plain':
        js = lambda N: np.random.uniform(10**(-4),10**1,N)
        raw = sample_positives(nsamples, postseismic=postseismic,
                               jumps_sampler=js)

    raw = fop.map(raw, ftr.label_batch([1.0, 0.0]))
    raw = fop.map(raw, ftr.scale())

    # Tremendo side efect!
    raw.options.batch_size = 200

    if one_part:
        dset = raw
        if only_positive:
            dset = glue.dafneFbToTf(dset).cache()  # .batch(BUFFER)
            dset.save(os.path.join(base_out_dir,'positive'))
            return 0
    else:
        pos10 = SimpleNamespace()

        pos10.parts = fop.split(raw, [0.7, 0.15, 0.145, 0.005])  # Train, Test, Val, show

    raw = sample_negatives(nsamples)

    raw = fop.map(raw, ftr.label_batch([0.0, 1.0]))
    raw = fop.map(raw, ftr.scale())

    # Tremendo side efect!
    raw.options.batch_size = 200

    if one_part:
        dset += raw
        dset = glue.dafneFbToTf(dset).cache() # .batch(BUFFER)
        dset.save(os.path.join(base_out_dir,'raw_samples'))
        return 0

    assert not one_part
    assert not only_positive

    neg10 = SimpleNamespace()

    neg10.parts = fop.split(raw, [0.7, 0.15, 0.145, 0.005])
    #                           Train, Test, Val  , Show

    # --- Start Data augmentation.

    todo_train = pos10.parts[0] + neg10.parts[0]
    dset_train = glue.dafneFbToTf(todo_train).cache()  # .batch(BUFFER)

    # load test dataset

    todo_tst = pos10.parts[1] + neg10.parts[1]
    dset_tst = glue.dafneFbToTf(todo_tst).cache()  # .batch(BUFFER)

    todo_val = pos10.parts[2] + neg10.parts[2]
    dset_val = glue.dafneFbToTf(todo_val).cache()  # .batch(BUFFER)
    # +
    # Load and augment train dataset

    algo = pos10.parts[0] * 18  # + neg10.parts[0] * 4

    algo.options.batch_size = 1e5
    aumenta = fop.map(algo, ftr.random_rot_dir())

    algo = neg10.parts[0] * 18
    aumenta += fop.map(algo, ftr.random_rot_dir())

    todo = pos10.parts[0] + neg10.parts[0]
    todo = todo + aumenta

    todo.options.columns = set(["este", "norte", "altura", "etiqueta"])

    dset = glue.dafneFbToTf(todo).cache().shuffle(BUFFER)

    p = pos10.parts[3]

    show_dset = glue.dafneFbToTf(p)  # .batch(BS).take(1).get_single_element()

    dset.save(os.path.join(base_out_dir,"train"))
    dset_train.save(os.path.join(base_out_dir,"raw_train"))
    dset_tst.save(os.path.join(base_out_dir,"test"))
    dset_val.save(os.path.join(base_out_dir,"validation"))
    show_dset.save(os.path.join(base_out_dir,"show"))
    return 0

if __name__ == '__main__':
    exit(main())

