# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.13.5
#   kernelspec:
#     display_name: Python 3 (ipykernel)
#     language: python
#     name: python3
# ---

BUFFER = int(2e6)
BS = int(4096)

# +
from dafnedset.presets_simple import pos10,neg10
import dafnedset.tf.glue as glue
from dafnedset import fun_ops as fop
from dafnedset import fun_transformations as ftr

# load test dataset

todo_tst = pos10.parts[1] + neg10.parts[1]
dset_tst = glue.dafneFbToTf(todo_tst).cache()#.batch(BUFFER)

todo_val = pos10.parts[2] + neg10.parts[2]
dset_val = glue.dafneFbToTf(todo_val).cache()#.batch(BUFFER)

# +
# Load and augment train dataset

algo = pos10.parts[0] * 18 #+ neg10.parts[0] * 4

algo.options.batch_size = 1e5
algo = fop.map(algo,ftr.channel_mix_w_relabel(p_true_pos=1,p_true_pos_ch=1))
aumenta = fop.map(algo,ftr.random_rot_dir())

algo = neg10.parts[0] * 18
aumenta += fop.map(algo,ftr.random_rot_dir())

algo = pos10.parts[0] * 6 + neg10.parts[0] * 12
algo.options.batch_size = 1e5
algo = fop.map(algo,ftr.channel_mix_w_relabel(p_true_pos=1,p_true_pos_ch=1))
aumenta += fop.map(aumenta,ftr.random_rot_dir())

todo = (pos10.parts[0] + neg10.parts[0])
todo = todo + aumenta

todo.options.columns = ['este','norte','altura','etiqueta']

dset = glue.dafneFbToTf(todo).cache().shuffle(BUFFER)#.batch(BS)

# +
p = pos10.parts[3]

show_dset = glue.dafneFbToTf(p)#.batch(BS).take(1).get_single_element()
# -

import tensorflow as tf

tf.data.experimental.save(dset,'ds/train')
tf.data.experimental.save(dset_tst,'ds/test')
tf.data.experimental.save(dset_val,'ds/validation')
tf.data.experimental.save(show_dset,'ds/show')
