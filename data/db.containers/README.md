# Database Preparation

Part of the data pre-processing flow is based on a Postgres+PostGIS database.

We used buildah to create two container images for loading and indexing the data
 into a Postgres database.

The raw data consists on individual station files in the format provided by
the Nevada Geodetic laboratory (.tenv3)

The first container (tag `db:init...`), filters the raw files and creates
indexes of available time periods and geographic position of each station.
Then it executes a Query that returns candidates for timeseries on both
groups of the dataset built for the paper, applying the criteria there specifyed.

This first container can be used with other programs of this repository for
querying GNSS data for particular seismic events, as was used in generating
maps for figures on the paper.

The second container (tagged `db:full...`) also loads the coordinate data and prepares
the 61x3 data arrays of each sample. It can be used by the dataset building
python programs that reads from the database and saves the datasets in a
format that tensorflow can use for training. However, if the labeling criteria
is not modified, pre-computed files can be used instead of the database.
