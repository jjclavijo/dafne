#!/bin/zsh

PGDATA=/var/lib/postgresql/data
DATAVER=20.01

#if [ -z ${CONTAINER_KEY+x} ]
#then 
#  echo Buildah no logged in
#else
#  echo Buildah login...
#  buildah login -u $CONTAINER_USR -p $CONTAINER_KEY registry.gitlab.com
#fi

# A disposable container for the database initialization
CONT=$(buildah from --pull dafne/db:init-$DATAVER)
MOUNT=$(buildah mount $CONT)

# Container with the data files
CONTDATA=$(buildah from dafne/data:$DATAVER)
MOUNTDATA=$(buildah mount $CONTDATA)

# Copy db scripts
mkdir -p $MOUNT/var/lib/postgresql/sismodb/datos
mount --bind $MOUNTDATA/dat $MOUNT/var/lib/postgresql/sismodb/datos
cp -r install_scripts $MOUNT/
cp -r scripts.dataload $MOUNT/var/lib/postgresql/sismodb/manejo-datos
cp db-loaddata.sh $MOUNT/

# Set permissions for reading data files
buildah run $CONT -- chown -R postgres:postgres /var/lib/postgresql/sismodb/datos/

# Install postgis
# TODO: install_scripts should not have versions hardcoded
buildah run $CONT -- /install_scripts/install_pgis.sh

cp buildb.sh $MOUNT/docker-entrypoint-initdb.d

# Initialize database
buildah run -e POSTGRES_PASSWORD=password $CONT -- ./db-loaddata.sh postgres

# The real container
CONT2=$(buildah from docker.io/postgres:12)

# We want persistence, so PGDATA should not be a volume anymore
buildah config --volume $PGDATA- $CONT2

# Install postgis and Xzip
MOUNT2=$(buildah mount $CONT2)
cp -r install_scripts $MOUNT2/
buildah run $CONT2 -- /install_scripts/install_pgis.sh
buildah run $CONT2 -- /install_scripts/install_xz-utils.sh

# Commit so layer isn't so big
buildah commit $CONT2 dafne/db:full-$DATAVER
buildah rm $CONT2

CONT2=$(buildah from dafne/db:full-$DATAVER)
MOUNT2=$(buildah mount $CONT2)

# Copy database 
rsync -a $MOUNT/$PGDATA/ $MOUNT2/$PGDATA

# Drop temporal containers
buildah umount $CONT
buildah rm $CONT

buildah umount $CONTDATA
buildah rm $CONTDATA

# Commit image
buildah commit $CONT2 dafne/db:full-$DATAVER

#buildah logout registry.gitlab.com

# delete container
buildah umount $CONT2
buildah rm $CONT2
