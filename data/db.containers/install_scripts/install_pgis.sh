#!/bin/bash

# Bash "strict mode", to help catch problems and bugs in the shell
# script. Every bash script you write should include this. See
# http://redsymbol.net/articles/unofficial-bash-strict-mode/ for
# details.
set -euo pipefail

# Tell apt-get we're never going to be able to give manual
# feedback:
export DEBIAN_FRONTEND=noninteractive

echo Updating Package DB
# Update the package listing, so we know what package exist:
apt-get -qq update

echo Upgrading outdated packages
# Install security updates:
apt-get -qq -y upgrade

CERT_DEPS="gnupg2 wget ca-certificates rpl pwgen"

echo Installing Certificates
# Install a new package, without unnecessary recommended packages:
DEBCONF_NOWARNINGS=yes DEBIAN_FRONTEND=noninteractive apt-get -qq -y install --no-install-recommends $CERT_DEPS > /dev/null

wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc |\
    gpg --dearmor |\
    tee /usr/share/keyrings/postgres.gpg > /dev/null

echo "deb [signed-by=/usr/share/keyrings/postgres.gpg] http://apt.postgresql.org/pub/repos/apt/ bullseye-pgdg main" |\
    tee /etc/apt/sources.list.d/pgdg.list

#sh -c 'echo "deb http://apt.postgresql.org/pub/repos/apt/ bullseye-pgdg main" > /etc/apt/sources.list.d/pgdg.list'

echo Installing Postgis
apt-get -qq update &&\
  DEBCONF_NOWARNINGS=yes DEBIAN_FRONTEND=noninteractive apt-get -qq install \
  -y --no-install-recommends postgresql-12-postgis-3 parallel > /dev/null

echo Cleaning
apt-get -qq -y autoremove $CERT_DEPS > /dev/null

# Delete cached files we don't need anymore:
apt-get -qq clean > /dev/null
rm -rf /var/lib/apt/lists/*
