#!/bin/bash

# Bash "strict mode", to help catch problems and bugs in the shell
# script. Every bash script you write should include this. See
# http://redsymbol.net/articles/unofficial-bash-strict-mode/ for
# details.
set -euo pipefail

# Tell apt-get we're never going to be able to give manual
# feedback:
export DEBIAN_FRONTEND=noninteractive

# Update the package listing, so we know what package exist:
apt-get -qq update > /dev/null

# Install a new package, without unnecessary recommended packages:
DEBCONF_NOWARNINGS=yes DEBIAN_FRONTEND=noninteractive apt-get -qq -y install --no-install-recommends xz-utils >> /dev/null

# Delete cached files we don't need anymore:
DEBCONF_NOWARNINGS=yes DEBIAN_FRONTEND=noninteractive apt-get -qq clean >> /dev/null
rm -rf /var/lib/apt/lists/*
