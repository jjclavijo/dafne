#!/bin/zsh

PGDATA=/var/lib/postgresql/data
DATAVER=20.01
PG_TAG=12

#if [ -z ${CONTAINER_KEY+x} ]
#then 
#  echo Buildah no logged in
#else
#  echo Buildah login...
#  buildah login -u $CONTAINER_USR -p $CONTAINER_KEY registry.gitlab.com
#fi

# A disposable container for the database initialization

echo Initializing PostGres container

CONT=$(buildah from docker.io/postgres:$PG_TAG)
MOUNT=$(buildah mount $CONT)
buildah config --volume $PGDATA- $CONT

echo Initializing data container
# Container with the data files
CONTDATA=$(buildah from --pull dafne/data:$DATAVER)
MOUNTDATA=$(buildah mount $CONTDATA)

# Copy db scripts
mkdir -p $MOUNT/var/lib/postgresql/sismodb/datos

echo -n bind mounting Data Files
mount --bind $MOUNTDATA/dat $MOUNT/var/lib/postgresql/sismodb/datos
echo ... Done

cp -r install_scripts $MOUNT/
cp -r scripts.indexing $MOUNT/var/lib/postgresql/sismodb/manejo-datos
cp db-init+index.sh $MOUNT/

# Set permissions for reading data files
buildah run $CONT -- chown -R postgres:postgres /var/lib/postgresql/sismodb/datos/

echo Installing PosGis
# Install postgis
# TODO: install_scripts should not have versions hardcoded
buildah run $CONT -- /install_scripts/install_pgis.sh
echo Done Installing Postgis

cp buildb.sh $MOUNT/docker-entrypoint-initdb.d

echo Initializing Database
# Initialize database
buildah run -e POSTGRES_PASSWORD=password $CONT -- ./db-init+index.sh postgres

echo Building Clean Container
# The real container
CONT2=$(buildah from docker.io/postgres:$PG_TAG)

# We want persistence, so PGDATA should not be a volume anymore
buildah config --volume $PGDATA- $CONT2

MOUNT2=$(buildah mount $CONT2)

echo Install Dependency
# Install postgis and Xzip
cp -r install_scripts $MOUNT2/
buildah run $CONT2 -- /install_scripts/install_pgis.sh
buildah run $CONT2 -- /install_scripts/install_xz-utils.sh

echo Copying DB data
# Copy database 
rsync -a $MOUNT/$PGDATA/ $MOUNT2/$PGDATA

# Commit image
buildah commit $CONT2 dafne/db:init-$DATAVER

echo Cleaning
# Drop temporal containers

buildah umount $CONT
buildah rm $CONT

buildah umount $CONTDATA
buildah rm $CONTDATA

#buildah logout registry.gitlab.com

# delete container
buildah umount $CONT2
buildah rm $CONT2
