#!/bin/bash

CONT=$(buildah from registry.gitlab.com/jjclavijo/dafne/data:artifacts)
MOUNT=$(buildah mount $CONT)

if [ "$MOUNT" = "" ]
then
  echo Not working
else
  cp -r $MOUNT/* .
fi

buildah rm $CONT
