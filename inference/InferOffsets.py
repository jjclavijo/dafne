# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.13.5
#   kernelspec:
#     display_name: Python 3 (ipykernel)
#     language: python
#     name: python3
# ---

import click

### Parametros

# tipo Variante del modelo
# rutaD ruta a los checkpoints
# rutaG
# eq = 4830
# dist = 1500
# fname

def loadModel(tipo,rutaD,rutaG):
    if tipo == 1:
        import dfmodels.models.model1 as model
    if tipo == 2:
        import dfmodels.models.model2labels as model
    #if tipo == 25:
    #    import dfmodels.models.model2 as model
    #if tipo == 3:
    #    import dfmodels.models.model3 as model

    G = model.deceiv()
    G.load_weights(rutaG)

    D = model.discrim()
    D.load_weights(rutaD)

    return G,D

@click.command()
@click.option('--tipo',default=1)
@click.option('--rutaD',default='')
@click.option('--rutaG',default='')
@click.option('--jsonfile',default='')

def main(tipo,rutad,rutag,jsonfile):

    import psycopg2 as pg
    import pandas as pd
    import geopandas as gpd
    import numpy as np
    import json
    import contextily as ctx
    import subprocess as sp
    import matplotlib.pyplot as plt
    from dfmodels.tf.fun import apply_step as S
    import shapely.geometry as geom
    from math import ceil,floor


    G,D = loadModel(tipo,rutad,rutag)


    with open(jsonfile) as f:
        jdata = json.load(f)

    # jdata = json.loads(output)

    sitios = jdata['puntos']
    series = jdata['instances']

    series = [[[i if i != 'NaN' else np.nan for i in j] for j in k] for k in series]
    seriesS = pd.Series(series,index=sitios)
    seriesS = seriesS.map(np.array).map(lambda x: x - np.nanmean(x,axis=0))

    # +
    d = np.stack(seriesS.values)
    s = G(d)

    d1 = S(d,s)
    s += G(d1)

    d1 = S(d,G(d))
    s += G(d1)

    d1 = S(d,G(d))
    s += G(d1).numpy()

    df = pd.DataFrame(-s.numpy(),index=sitios,columns=['east','north','up'])

    print(df)

if __name__ == '__main__':
    exit(main())

