# Using the trained models

Here we provide two simple scripts to use the traned models for inference.

The input for this scripts is a json file, we provide an example with the
timeseries for the Iquique Earthquake.

Running the models requires using the checkpoints saved during training, in the
example below we use the published containers and volume-mount the checkpoints
from the artifacts containers. The same effect can be achieved copying the
checkpoints to the local filesystem.

```
# Volume Mount the trained models note
podman run -d --rm \
       --mount 'type=volume,src=modelG,dst=/trained/01/c-663026d5-G,ro=true' \
       --mount 'type=volume,src=modelD,dst=/trained/01/c-663026d5-D,ro=true' \
       dafne/data:artifacts /bin/sleep 1

# Run the script, note that we renamed the directoryes
# We use the last checkpoint in this example (0999)
podman run --rm -v $(realpath .):/wdir -w=/wdir \
       --mount 'type=volume,src=modelG,dst=/wdir/c-G,ro=true' \
       --mount 'type=volume,src=modelD,dst=/wdir/c-D,ro=true' \
       dafne/models:latest \
       InferOffsets.py --model=1 --rutaD c-D/0999.ckpt --rutaG c-G/0999.ckpt \
       --jsonfile evt.json

podman volume rm modelG modelD
```
