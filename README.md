# Code for the paper "Adversarial learning of permanent seismic deformation from GNSS coordinate timeseries"

The paper was already submitted for publication on the journal "Computers & Geosciences"

For the moment, please email me (jclavijo) at fi.uba.ar if needed

Short name for the code is **Dafne**, with \<Daf\> standing for "Deformation As a
Feature" and \<ne\> coming from "NEural NEtworks", which is incidentally the
framework used

The Code has three main parts:

- The dataset part.
- The model and training part.
- The experiments and graphics part.

Container images were generated (Using buildah, but should be fully docker-compatible)
for some parts of the process (specially the data pre-processing)

# Reproducing the experiments

## General Usage comments

During development, every part of the code was tested usign OCI-containers with podman
in root-less mode. All relevant containers are published on [this repository's registry](https://gitlab.com/jjclavijo/dafne/container_registry)

All python modules can be installed with flit, and should work with a local python and tensorflow installation.

Haskell binaries for querying compressed `.tenv3` files should work on amd64 linux, but are meant to work
inside a container and conected to a containerized PostGIS database.
This setting is probably an overkill but we mantain it because that was the path selected during the experimentation
with the data.

## Raw Data Files

We packed the exact versions of the datafiles used for the paper in a container image at

```
registry.gitlab.com/jjclavijo/dafne/data:20.01
```

you can inspect the data mounting the image using buildah

```
buildah unshare
CONT=$(buildah from registry.gitlab.com/jjclavijo/dafne/data:20.01)
MOUNT=$(buildah mount $CONT)
cd $MOUNT/data
```

The datafiles were accessed and downloaded on January-2020. For details on
the building of this image see [this section](data/data.container)

In order to garantee reproducibility, all the published pre-processing
scripts starts from this image. For including more data the scripts can
be simply updated to use a new updated version of the files.

## Pre-processing and dataset building

The steps leading to the training dataset are:

- Loading data availavility information into a PostGIS DB.
- Querying candidete cases for both proposed groups.
- Loading dataseries.
- Preprocess and augment the dataset, save in tensorflow format.

Scripts for the first 3 steps are in [this section](data/db.containers).

Two containers can be built with the scripts.
The first container only consists on the index database. It is used in combination with other scripts for querying sections of the data, i.e. when producing maps for a certain event. We call it the dbinit image.

The second container has the data series selected for the dataset loaded in the database. This container has the war data for building the dataset. We call it the `db:full` image

The last part of the dataset building corresponds to the python routines on [this section](data/scripts), that relies on the python code in [the module we called dafnedset](data/python/dafnedset)

We recommend running this program inside the dedicated container we created from [this script](data/query.container/buildah-hs-python.sh), and connecting to a running instance of the `db:full...` container.

We usually create a podman pod with the needed version of the database
```
podman pod create dafne
podman run --pod dafne --detach dafne/db:full-20.01
```

And run the commands in a container, volume-mounting the workdir.
```
podman run --rm --pod dafne -v $(realpath .):/wdir -w=/wdir /opt/venv/bin/python 00-....py
```

The datasets used in the paper are provided in a separate container, and can be downloaded with a script provided under [artifacts](artifacts/getArtifacts.buildah.sh), although can be generated by the scripts in the repository. The split of the samples is random, but the seed of the generator was fixed to ensure reproducibility.

## Model training

The training of the model is conduced over a saved version of the dataset, no db container needed.

It can be perfectly run locally, we used podman containers only to ensure reproducibility.

The module containing the tensorflow models is [this one, that we called dfmodels](models/python/dfmodels), and the training loops can be found under its the submodule [train](models/python/dfmodels/train) they can be run as scripts and has a number of hyperparameters over which we cross-validated as mentioned in the paper.

Look the [models](models) and [Cross Validation](extra/crossValidate) sections for details

The training process can generate snapshots of the training process, as well as checkpoints of the models for the desired train steps. This data can be used to produce reports similar to those on the paper, and, in combination with the database, to infer deformation for any desired event in the db.

## Producing Reports

All the scripts used when producing reports are provided in the [extra](extra) section.

This scripts relies on the snapshots taken during the training process, the snapshots of the training we conduced for
the paper can be obtained from de `data:artifacts` container image.

That scripts, although poorly documented, has multiple helper functions and can be used as a guide for how to use
trained models to produce new results.

## Synthetic data Hector and other extras.

In a Revision of the paper, synthetic data generation was included. For this we used a modified version os Machiel Bos's
hectorp software. This version can be installed from [this repo](https://gitlab.com/jjclavijo/hectorpp)

The scripts for the generation of synthetic datasets are present on the [data](data/scripts) folder, and the models trained
on this datasets are included on the `data:artifacts` container image.

