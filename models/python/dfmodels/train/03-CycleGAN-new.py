# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.13.5
#   kernelspec:
#     display_name: Python 3 (ipykernel)
#     language: python
#     name: python3
# ---

# # Variante 1: GAN con generador condicionado

# +
# #!/opt/venv/bin/python -m pip install pydot matplotlib
# #!apt-get -y -qq install graphviz
# -

## Imports

import tensorflow as tf
import matplotlib.pyplot as plt

import time
import json
import numpy as np
from pyarrow import Table as paTable, parquet as pq

import click

import dfmodels.models.model3 as model
from dfmodels.tf.fun import apply_step

import logging


logging.basicConfig()
log = logging.getLogger('DFTRAIN')
log.setLevel(logging.INFO)


# Constants
BUFFER = int(2e6)

# Hyperparameters

D_DROPOUT = 0.0
D_SN = False
D_LR = 1e-5

G_DROPOUT = 0.05
G_SN = False
G_LR = 1e-5

BS = 256
EPOCHS = 1000

# Regularization
REG_NORM = 'None'
REG_N = False
REG_SELF = False
STEP_ROTATES = False
ALPHA = 0.1
BETA = 0.1

# Files

DATASET='Datasaves'

REPORT='01.GcGAN'
LOSS_FNAME='01.GcGAN.json'
CKPT='01.GcGAN.cp'



# Functional Functions

#Define loss functions, F-GAN: https://www.semanticscholar.org/paper/f-GAN%3A-Training-Generative-Neural-Samplers-using-Nowozin-Cseke/ffdcad14d2f6a12f607b59f88da4a939f4821691

# X^2-Person divergence
def gf(v):
    v = tf.maximum(v,-9.0) # saturar V para evitar errores
    v = tf.minimum(v,21.0) # saturar V para evitar errores

    return v

def fstar(t):
    return 1/4 * t**2 + t
    #return tf.math.exp(t-1)

def d_loss(fake_output, real_output):

    fo = gf(fake_output)
    ro = gf(real_output)

    return tf.reduce_mean(ro) + tf.reduce_mean(-fstar(fo))

def g_loss(fake_output):

    fo = gf(fake_output)
    return tf.reduce_mean(-fstar(fo))

# +
# Start optimizers


@tf.function
def part_pos_neg(data,labels):
    labels = labels[:,0] > 0.5

    data_p = tf.boolean_mask(data,labels)
    data_n = tf.boolean_mask(data,~labels)

    limit = tf.minimum(tf.size(data_p),tf.size(data_n))

    data_p = data_p[:limit]
    data_n = data_n[:limit]

    return data_p,data_n

def part_pn_v2(data,labels):
    labels = labels > 0.5

    data_p = tf.boolean_mask(data,labels)
    data_n = tf.boolean_mask(data,~labels)

    return data_p,data_n


def save_losses(fname,losses):
    with open(fname,'w') as f:
        json.dump([*map(lambda z:[*map(lambda y:[*map(lambda x:float(x.numpy()),y)],z)],losses)],f)

gt = tf.GradientTape
S_ = apply_step

@click.command()
@click.option('--D_DROPOUT', default=D_DROPOUT)
@click.option('--D_SN', default=D_SN)
@click.option('--D_LR', default=D_LR)
@click.option('--G_DROPOUT', default=G_DROPOUT)
@click.option('--G_SN', default=G_SN)
@click.option('--G_LR', default=G_LR)
@click.option('--BS', default=BS)
@click.option('--DATASET', default=DATASET)
@click.option('--REPORT', default=REPORT)
@click.option('--LOSS_FNAME', default=LOSS_FNAME)
@click.option('--CKPT', default=CKPT)
@click.option('--REG_NORM', default= REG_NORM)
@click.option('--REG_N', default= REG_N)
@click.option('--REG_SELF', default= REG_SELF)
@click.option('--ALPHA', default= ALPHA)
@click.option('--EPOCHS', default= EPOCHS)
@click.option('--STEP_ROTATES', default= STEP_ROTATES)
@click.option('--BETA', default= BETA)

def main_loop(
    d_dropout ,
    d_sn ,
    d_lr ,
    g_dropout ,
    g_sn ,
    g_lr ,
    bs ,
    dataset ,
    report ,
    loss_fname ,
    ckpt ,
    reg_norm ,
    reg_n ,
    reg_self ,
    alpha,
    epochs,
    step_rotates,
    beta):

    log.info('Initializing Networks')


    all_options={
            'D Dropout Rate':d_dropout ,
            'D Spectral Norm':d_sn ,
            'D learning rate':d_lr ,
            'G Dropout Rate':g_dropout ,
            'G Spectral Norm':g_sn ,
            'G Learning Rate':g_lr ,
            'Batch Size':bs ,
            'Alpha':alpha ,
            'Dataset Folder':dataset ,
            'Report Folder':report ,
            'Loss File':loss_fname ,
            'Checkpoints Folder':ckpt ,
            'Regularization Self':reg_self ,
            'Regularization Negative':reg_n ,
            'Regularization Norm':reg_norm ,
            'Epochs':epochs,
            'Step Rotates':step_rotates,
            'Beta':beta
            }

    if step_rotates:
        from dfmodels.tf.fun import apply_step_R
        S = apply_step_R
    else:
        S = apply_step

    log.info(all_options)

    PARS_D={'units_feat': [60,60,60,30,30,30],
                   'units_dicc': [30,30,30,30],
                   'dropout_rate': d_dropout
                   }

    PARS_G={'units_feat': [60,60,60,30,30,30],
                   'units_dicc': [30,30,30,30],
                   'dropout_rate': g_dropout
                   }

    log.info('Initializing Networks')

    D = model.discrim(SpectralNorm=d_sn, params = PARS_D)
    G = model.deceiv(SpectralNorm=g_sn, params = PARS_G)

    D2 = model.discrim(SpectralNorm=d_sn, params = PARS_D)
    G2 = model.deceiv(SpectralNorm=g_sn, params = PARS_G)

    def G_(x,l,**kwargs):
        #return G([x,tf.random.uniform([tf.shape(x)[0],3],-1,1),l],**kwargs)
        return G([x,l],**kwargs)

    BS = int(bs)

    # DatasetLoading
    log.info('Dataset Loading')

    dset = tf.data.experimental.load('{}/train'.format(dataset)).batch(BS)
    dset_tst = tf.data.experimental.load('{}/test'.format(dataset)).batch(BUFFER)
    show_dset= tf.data.experimental.load('{}/show'.format(dataset)).batch(BS).take(1).get_single_element()

    show_dset = (show_dset[0],show_dset[1][:,0])

    d,l = dset.take(1).get_single_element()

    # End DatasetLoading

    log.info('Optimizers Inicialization')
    D_optimizer = tf.keras.optimizers.Adam(d_lr)
    G_optimizer = tf.keras.optimizers.Adam(g_lr)

    D2_optimizer = tf.keras.optimizers.Adam(d_lr)
    G2_optimizer = tf.keras.optimizers.Adam(g_lr)

    if reg_norm == 'square':
        log.info('L2 norm regularization:Ok')
        norm = tf.math.square
    elif reg_norm == 'abs':
        log.info('L1 norm regularization:Ok')
        norm = tf.abs
    else:
        norm = lambda x:0

    @tf.function
    def train_step_intB(dp,dn,alpha,beta):
            #dataset_origin, dataset_label
            dso1 = D(dn, training=True)

            s1 = G(dp, training=True)
            fd = S(dp,s1)

            mask = tf.math.greater(tf.abs(s1),0.002)
            mask = tf.reduce_any(mask,axis=-1)

            #tf.print(tf.math.reduce_sum(tf.cast(mask,tf.int32)),end=' ')

            mask = tf.logical_or(tf.math.greater(tf.random.uniform(shape=tf.shape(mask),minval=0,maxval=1),0.9),mask)


            #fakedata_origin, fakedata_label
            fdo1 = D(fd, training=True)

            fdG = tf.boolean_mask(fd,mask)
            dpG = tf.boolean_mask(dp,mask)

            s1 = []
            if reg_self:
                s0 = G(dp, training=True)
                s1 = G(S(dp,s0), training=True)
            if reg_n:
                s1.append(G(dn, training=True))
            if reg_n or reg_self:
                l2pen1 = tf.reduce_mean(norm(tf.concat(s1,axis=0)))
            else:
                l2pen1 = 0.0

            dso2 = D2(dp, training=True)

            fd2 = S(fdG,G2(fdG, training=True))

            #fakedata_origin, fakedata_label
            fdo2 = D2(fd2, training=True)


            s1 = []
            if reg_self:
                s0 = G2(fdG, training=True)
                s1 = G2(S(fdG,s0), training=True)
            if reg_n:
                s1.append(G2(dp, training=True))
            if reg_n or reg_self:
                l2pen2 = tf.reduce_mean(norm(tf.concat(s1,axis=0)))
            else:
                l2pen2 = 0.0

            D_loss = d_loss(fdo1, dso1)
            D2_loss = d_loss(fdo2, dso2)

            _G_loss = g_loss(fdo1)
            _G2_loss = g_loss(fdo2)

            dpG0 = tf.zeros_like(dpG,dtype=tf.float32)
            fd2 = tf.where(tf.math.is_nan(dpG),dpG0,fd2)
            dpG = tf.where(tf.math.is_nan(dpG),dpG0,dpG)

            loopL1 = tf.reduce_mean(norm(tf.math.subtract(dpG,fd2)))

            #loopL1 = tf.cast(loopL1,tf.float32)
            #tf.print(loopL1)

            G_loss = _G_loss + l2pen1 * alpha + loopL1 * beta
            G2_loss = _G2_loss + l2pen2 * alpha + loopL1 * beta

            return G_loss,D_loss,G2_loss,D2_loss


    @tf.function
    def train_step_int(data_p,data_n,alpha,beta):
        with gt() as G_tape, gt() as D_tape, gt() as G2_tape, gt() as D2_tape:

            G_loss,D_loss,G2_loss,D2_loss = train_step_intB(data_p,data_n,alpha,beta)

        NG = G_tape.gradient(G_loss, G.trainable_variables)
        ND = D_tape.gradient(D_loss, D.trainable_variables)
        NG2 = G2_tape.gradient(G2_loss, G2.trainable_variables)
        ND2 = D2_tape.gradient(D2_loss, D2.trainable_variables)


        return NG,ND,NG2,ND2

    @tf.function
    def train_step(data,labels, alpha=0.1, beta=0.1):

        data_p,data_n = part_pos_neg(data,labels)

        dp = tf.cast(data_p, tf.float32)
        dn = tf.cast(data_n, tf.float32)

        NG,ND,NG2,ND2 = train_step_int(dp,dn,alpha, beta)

        G_optimizer.apply_gradients(zip( NG , G.trainable_variables))
        D_optimizer.apply_gradients(zip(map(lambda x:-x,ND), D.trainable_variables))
        G2_optimizer.apply_gradients(zip( NG2 , G2.trainable_variables))
        D2_optimizer.apply_gradients(zip(map(lambda x:-x,ND2), D2.trainable_variables))

    """
    @tf.function
    def train_step_intB(data_p,data_n,d,l,alpha):
            #dataset_origin, dataset_label
            dso, dsl = D(d, training=True)

            fake_data = S(d,G_(d,l, training=True))

            #fakedata_origin, fakedata_label
            fdo, fdl = D(fake_data, training=True)

            # Part label data in + and -
            dslP,dslN = part_pn_v2(dsl,l)
            fdlP,fdlN = part_pn_v2(fdl,l)

            # Join labels from Real and fake data
            lP = tf.concat([dslP,fdlP],axis=-1)
            lN_a = tf.concat([dslN,fdlN],axis=-1)
            # lN_b = tf.concat([dslN],axis=-1)

            s1 = []
            if reg_self:
                s0 = G_(d,l, training=True)
                s1 = G_(S(d,s0),l, training=True)
            if reg_n:
                l_n = tf.ones(tf.shape(data_n)[0])
                s1.append(G_(data_n,l_n, training=True))
            if reg_n or reg_self:
                l2pen = tf.reduce_mean(norm(tf.concat(s1,axis=0)))
            else:
                l2pen = 0

            D_o_loss = d_loss(fdo, dso)# + sim_cost
            D_l_loss = d_loss(lP, lN_a)# + sim_cost

            G_o_loss = g_loss(fdo)
            G_l_loss = g_loss(fdlP)
            #G_l_loss += g_loss(fdlN) ?

            G_loss = G_o_loss + G_l_loss + l2pen * alpha
            D_loss = D_o_loss + D_l_loss

            return G_loss,D_loss

    @tf.function
    def train_step_int(data_p,data_n,d,l,alpha):
        with gt() as G_tape, gt() as D_tape:

            G_loss, D_loss = train_step_intB(data_p,data_n,d,l,alpha)

        NG = G_tape.gradient(G_loss, G.trainable_variables)
        ND = D_tape.gradient(D_loss, D.trainable_variables)

        return NG,ND

    @tf.function
    def train_step(data,labels,alpha=0.1):

        data_p,data_n = part_pos_neg(data,labels)
        labels = labels[:,0]

        d,l = data,labels

        NG,ND = train_step_int(data_p,data_n,d,l,alpha)

        G_optimizer.apply_gradients(zip( NG , G.trainable_variables))

        D_optimizer.apply_gradients(zip(map(lambda x:-x,ND), D.trainable_variables))
    """
    @tf.function
    def print_step_int(dp,dn):
            #dataset_origin, dataset_label
            dso1 = D(dn, training=True)

            fd = S(dp,G(dp, training=True))

            #fakedata_origin, fakedata_label
            fdo1 = D(fd, training=True)

            s1 = []
            if reg_self:
                s0 = G(dp, training=True)
                s1 = G(S(dp,s0), training=True)
            if reg_n:
                s1.append(G(dn, training=True))
            if reg_n or reg_self:
                l2pen1 = tf.reduce_mean(norm(tf.concat(s1,axis=0)))
            else:
                l2pen1 = 0


            dso2 = D2(dp, training=True)

            fd2 = S(fd,G2(fd, training=True))

            #fakedata_origin, fakedata_label
            fdo2 = D2(fd2, training=True)


            s1 = []
            if reg_self:
                s0 = G2(fd, training=True)
                s1 = G2(S(fd,s0), training=True)
            if reg_n:
                s1.append(G2(dp, training=True))
            if reg_n or reg_self:
                l2pen2 = tf.reduce_mean(norm(tf.concat(s1,axis=0)))
            else:
                l2pen2 = 0

            D_loss = d_loss(fdo1, dso1)
            D2_loss = d_loss(fdo2, dso2)

            G_loss = g_loss(fdo1)
            G2_loss = g_loss(fdo2)

            fd2 = tf.where(tf.math.is_nan(dp),tf.zeros_like(dp),fd2)
            dp = tf.where(tf.math.is_nan(dp),tf.zeros_like(dp),dp)

            loopL1 = tf.reduce_mean(norm(tf.math.subtract(dp,fd2)))

            return G_loss,D_loss,G2_loss,D2_loss,l2pen1,l2pen2,loopL1

    @tf.function
    def print_step(data,labels):

        #labels = labels[:,0]

        dp,dn = part_pos_neg(data,labels)

        dp = tf.cast(dp, tf.float32)
        dn = tf.cast(dp, tf.float32)

        G,D,G2,D2,Gn,G2n,N = print_step_int(dp,dn)

        return G,D,G2,D2,Gn,G2n,N

    loss_text = "G:{:.3f} D:{:.3f} G2:{:.3f} D2:{:.3f} Gn:{:.3f} G2n:{:.3f} N:{:.3f}"
    # -

    def gen_report(fname=None,ds=None):

        if ds is None:
            return

        ds,l = ds

        lm = tf.where(l == 1,tf.zeros_like(l),tf.ones_like(l))

        steps = G(ds)
        steped = S_(ds,steps)
        stepsB = G2(steped)
        stepedB = S_(steped,stepsB)

        Dout = D(ds)

        D2out = D2(ds)

        DSGout = D(steped)

        D2SGout = D2(stepedB)

        if fname is None:
            pass
        else:
            with open(fname,'wb') as f:
                di = {'steps':steps.numpy().tolist(),
                      'steps_b':stepsB.numpy().tolist(),
                      'disc_1':Dout.numpy().tolist(),
                      'disc_2':D2out.numpy().tolist(),
                      'gen_o_disc_1':DSGout.numpy().tolist(),
                      'gen_o_disc_2':D2SGout.numpy().tolist()}
                tb = paTable.from_pydict(di)
                pq.write_table(tb,f)

    losses = []


    def train(dataset, epochs, e0=0,datafolder=None):

        log.info('Start Training')

        dd = []
        ll = []

        for data_batch in dataset:
            d,l = data_batch
            dd.append(d)
            ll.append(l)

        dd,ll = [tf.concat(dd,axis=0), tf.concat(ll,axis=0)]

        d,l = dset_tst.take(1).get_single_element()

        for epoch in range(epochs):

            epoch += e0
            start = time.time()

            for data_batch in dataset:
                train_step(*data_batch,beta=beta)

            tr_loss = print_step(dd,ll)

            tst_loss = print_step(d,l)

            # Save the model every 15 epochs
            #if (epoch + 1) % 15 == 0:
            #    checkpoint.save(file_prefix = checkpoint_prefix)

            if (epoch + 1) % 10 == 0:
                print ('{} Time:{:.2f} s; loss Tr -> {} | Tst -> {}'\
                       .format(epoch + 1, time.time()-start,
                               loss_text.format(*tr_loss),
                               loss_text.format(*tst_loss)))

            if datafolder is None:
                pass
            elif (epoch + 1) % 10 == 0:
                gen_report('{}/{}.pq'.format(datafolder,epoch),show_dset)
                gen_report('{}/t{}.pq'.format(datafolder,epoch),[d,l[:,0]])
                print ('reports Generated')

            if ckpt is None:
                pass
            elif (epoch + 1) % 10 == 0:
                checkpoint='{}G/{:04d}.ckpt'.format(ckpt,epoch)
                G.save_weights(checkpoint)
                checkpoint='{}D/{:04d}.ckpt'.format(ckpt,epoch)
                D.save_weights(checkpoint)
                checkpoint='{}G2/{:04d}.ckpt'.format(ckpt,epoch)
                G2.save_weights(checkpoint)
                checkpoint='{}D2/{:04d}.ckpt'.format(ckpt,epoch)
                D2.save_weights(checkpoint)
                print ('checkpoints Generated')

            losses.append([tr_loss,tst_loss])

        return losses

    losses = train(dset,epochs,datafolder=report)

    for data_batch in dset:
        data,labels = data_batch

    save_losses(loss_fname,losses)

    return None

if __name__ == '__main__':
    exit(main_loop())
