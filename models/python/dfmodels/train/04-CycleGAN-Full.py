# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.13.5
#   kernelspec:
#     display_name: Python 3 (ipykernel)
#     language: python
#     name: python3
# ---

# # Variante 2: cGAN

# +
# #!/opt/venv/bin/python -m pip install pydot matplotlib
# #!apt-get -y -qq install graphviz
# -

import matplotlib.pyplot as plt

import tensorflow as tf

# + tags=[]
import dfmodels.models.model3 as model

# +
D = model.discrim(SpectralNorm=True,dropout_rate=0.2)
G = model.deceiv()

D2 = model.discrim(SpectralNorm=True,dropout_rate=0.2)
G2 = model.deceiv()

# +
#tf.keras.utils.plot_model(D,show_shapes=True)
# -

BUFFER = int(2e6)
BS = int(256)


# +
dset = tf.data.experimental.load('Datasaves/train').batch(BS)
dset_tst = tf.data.experimental.load('Datasaves/test').batch(BUFFER)
show_dset= tf.data.experimental.load('Datasaves/show').batch(BS).take(1).get_single_element()

show_dset = (show_dset[0],show_dset[1][:,0])
# -

d,l = dset.take(1).get_single_element()


# +
#Define loss functions

def gf(v):
    v = tf.maximum(v,-9.0) # saturar V para evitar errores
    v = tf.minimum(v,21.0) # saturar V para evitar errores

    return v
    #return -tf.exp(-v)

def fstar(t):
    return 1/4 * t**2 + t
    #return tf.exp(t-1)
    #return -1-tf.math.log(-t)

def d_loss(fake_output, real_output):

    fo = gf(fake_output)
    ro = gf(real_output)

    return tf.reduce_mean(ro) + tf.reduce_mean(-fstar(fo))

def g_loss(fake_output):

    fo = gf(fake_output)

    return tf.reduce_mean(-fstar(fo))



# +
# Start optimizers

D_optimizer = tf.keras.optimizers.Adam(1e-5)
G_optimizer = tf.keras.optimizers.Adam(5e-4)

D2_optimizer = tf.keras.optimizers.Adam(1e-5)
G2_optimizer = tf.keras.optimizers.Adam(5e-4)
# -

# import aux functions
from dfmodels.tf.fun import apply_step


# +
def part_pos_neg(data,labels):
    labels = labels[:,0] > 0.5

    data_p = tf.boolean_mask(data,labels)
    data_n = tf.boolean_mask(data,~labels)

    limit = tf.minimum(tf.size(data_p),tf.size(data_n))

    data_p = data_p[:limit]
    data_n = data_n[:limit]

    return data_p,data_n

def part_pn_v2(data,labels):
    labels = labels > 0.5

    data_p = tf.boolean_mask(data,labels)
    data_n = tf.boolean_mask(data,~labels)

    #limit = tf.minimum(tf.size(data_p),tf.size(data_n))

    #data_p = data_p[:limit]
    #data_n = data_n[:limit]

    return data_p,data_n


# +
# Notice the use of `tf.function`
# This annotation causes the function to be "compiled".

S = apply_step

gt = tf.GradientTape

norm = tf.math.square

@tf.function
def train_step_intB(dp,dn,alpha,beta):
        #dataset_origin, dataset_label
        dso1 = D(dn, training=True)

        fd = S(dp,G(dp, training=True))
        fd1 = fd

        #fakedata_origin, fakedata_label
        fdo1 = D(fd, training=True)

        dso1a = D(dn, training=True)
        fd = S(dn,G2(dn, training=True))
        fd2 = fd

        fdo1a = D(fd, training=True)

        D_loss = d_loss(fdo1, dso1) + d_loss(fdo1a, dso1a)

        # Disriminador 2

        dso2 = D2(dp, training=True)

        fd = S(fd1,G2(fd1, training=True))
        fd_loop_p = fd

        #fakedata_origin, fakedata_label
        fdo2 = D2(fd, training=True)

        dso2a = D2(dn, training=True)

        fd = S(fd2,G(fd2, training=True))
        fd_loop_n = fd

        #fakedata_origin, fakedata_label
        fdo2a = D2(fd, training=True)

        D2_loss = d_loss(fdo2, dso2) + d_loss(fdo2a, dso2a)

        _G_loss = g_loss(fdo1) + g_loss(fdo2a)
        _G2_loss = g_loss(fdo2) + g_loss(fdo1a)


        s1a = G(dp, training=True)
        s1b = G(S(dp,s1a), training=True)
        l2pen1 = tf.reduce_mean(norm(s1b))

        s2a = G2(fd1, training=True)
        s2b = G2(S(fd1,s2a), training=True)
        l2pen2 = tf.reduce_mean(norm(s2b))

        loop = tf.where(tf.math.is_nan(dp),tf.zeros_like(dp),fd_loop_p)
        dp = tf.where(tf.math.is_nan(dp),tf.zeros_like(dp),dp)

        loopL1 = tf.reduce_mean(norm(tf.math.subtract(dp,loop)))

        loop = tf.where(tf.math.is_nan(dn),tf.zeros_like(dn),fd_loop_n)
        dp = tf.where(tf.math.is_nan(dn),tf.zeros_like(dn),dn)

        loopL1 += tf.reduce_mean(norm(tf.math.subtract(dp,loop)))

        G_loss = _G_loss + l2pen1 * alpha + loopL1 * beta
        G2_loss = _G2_loss + l2pen2 * alpha + loopL1 * beta

        return G_loss,D_loss,G2_loss,D2_loss


@tf.function
def train_step_int(data_p,data_n,alpha,beta):
    with gt() as G_tape, gt() as D_tape, gt() as G2_tape, gt() as D2_tape:

        G_loss,D_loss,G2_loss,D2_loss = train_step_intB(data_p,data_n,alpha,beta)

    NG = G_tape.gradient(G_loss, G.trainable_variables)
    ND = D_tape.gradient(D_loss, D.trainable_variables)
    NG2 = G2_tape.gradient(G2_loss, G2.trainable_variables)
    ND2 = D2_tape.gradient(D2_loss, D2.trainable_variables)


    return NG,ND,NG2,ND2

@tf.function
def train_step(data,labels, alpha=0.1, beta=0.1):

    data_p,data_n = part_pos_neg(data,labels)

    dp = tf.cast(data_p, tf.float32)
    dn = tf.cast(data_n, tf.float32)

    NG,ND,NG2,ND2 = train_step_int(dp,dn,alpha, beta)

    G_optimizer.apply_gradients(zip( NG , G.trainable_variables))
    D_optimizer.apply_gradients(zip(map(lambda x:-x,ND), D.trainable_variables))
    G2_optimizer.apply_gradients(zip( NG2 , G2.trainable_variables))
    D2_optimizer.apply_gradients(zip(map(lambda x:-x,ND2), D2.trainable_variables))


"""
@tf.function
def train_D(data,labels,alpha=0):

    labels = labels[:,0]

    with gt() as D_tape:#, gt() as D_tape:

        real_output = D([data,labels], training=True)[:,0]

        fake_output = D([
                        stepfunc(data,G_(data,labels, training=True))
                         ,tf.zeros_like(labels)] , training=True)[:,0]

        D_loss = d_loss(fake_output, real_output)# + sim_cost

    ND = D_tape.gradient(D_loss, D.trainable_variables)
    D_optimizer.apply_gradients(zip(map(lambda x:-x,ND), D.trainable_variables))

@tf.function
def train_G(data,labels,alpha=0):
    labels = labels[:,0]

    with gt() as G_tape:#, gt() as D_tape:
        fake_output = D([
                        stepfunc(data,G_(data,labels, training=True))
                         ,tf.zeros_like(labels)] , training=True)[:,0]

        G_loss = g_loss(fake_output) #+ sim_cost

    NG = G_tape.gradient(G_loss, G.trainable_variables)

    G_optimizer.apply_gradients(zip( NG , G.trainable_variables))
"""

None
# -

tf.math.subtract([2.,3.,4.],[1,2,3])


# +

@tf.function
def print_step_int(dp,dn):
        #dataset_origin, dataset_label
        dso1 = D(dn, training=True)

        fd = S(dp,G(dp, training=True))
        fd1 = fd

        #fakedata_origin, fakedata_label
        fdo1 = D(fd, training=True)

        dso1a = D(dn, training=True)
        fd = S(dn,G2(dn, training=True))
        fd2 = fd

        fdo1a = D(fd, training=True)

        D_loss = d_loss(fdo1, dso1) + d_loss(fdo1a, dso1a)

        # Disriminador 2

        dso2 = D2(dp, training=True)

        fd = S(fd1,G2(fd1, training=True))
        fd_loop_p = fd

        #fakedata_origin, fakedata_label
        fdo2 = D2(fd, training=True)

        dso2a = D2(dn, training=True)

        fd = S(fd2,G(fd2, training=True))
        fd_loop_n = fd

        #fakedata_origin, fakedata_label
        fdo2a = D2(fd, training=True)

        D2_loss = d_loss(fdo2, dso2) + d_loss(fdo2a, dso2a)

        G_loss = g_loss(fdo1) + g_loss(fdo2a)
        G2_loss = g_loss(fdo2) + g_loss(fdo1a)

        s1a = G(dp, training=True)
        s1b = G(S(dp,s1a), training=True)
        l2pen1 = tf.reduce_mean(norm(s1b))

        s2a = G2(fd1, training=True)
        s2b = G2(S(fd1,s2a), training=True)
        l2pen2 = tf.reduce_mean(norm(s2b))

        loop = tf.where(tf.math.is_nan(dp),tf.zeros_like(dp),fd_loop_p)
        dp = tf.where(tf.math.is_nan(dp),tf.zeros_like(dp),dp)

        loopL1 = tf.reduce_mean(norm(tf.math.subtract(dp,loop)))

        loop = tf.where(tf.math.is_nan(dn),tf.zeros_like(dn),fd_loop_n)
        dn = tf.where(tf.math.is_nan(dn),tf.zeros_like(dn),dn)

        loopL1 += tf.reduce_mean(norm(tf.math.subtract(dn,loop)))

        return G_loss,D_loss,G2_loss,D2_loss,l2pen1,l2pen2,loopL1

@tf.function
def print_step(data,labels):

    #labels = labels[:,0]

    dp,dn = part_pos_neg(data,labels)

    dp = tf.cast(dp, tf.float32)
    dn = tf.cast(dp, tf.float32)

    G,D,G2,D2,Gn,G2n,N = print_step_int(dp,dn)

    return G,D,G2,D2,Gn,G2n,N

loss_text = "G:{:.3f} D:{:.3f} G2:{:.3f} D2:{:.3f} Gn:{:.3f} G2n:{:.3f} N:{:.3f}"
# -

import time
import json
import numpy as np
import pyarrow as pa


# +
#show_dset = glue.dafneFbToTf(p).batch(BS).take(1).get_single_element()

def gen_report(fname=None):

    steps = deceiver(show_dset[0])
    discred = discriminator(show_dset[0])
    bothed = discriminator(stepfunc(show_dset[0],steps[:,:3]))

    if fname is None:
        pass
    else:
        with open(fname,'wb') as f:
            di = {'steps':steps.numpy().tolist(),
                  'disc':discred.numpy().tolist(),
                  'gen_o_disc':bothed.numpy().tolist()}
            tb = pa.Table.from_pydict(di)
            pa.parquet.write_table(tb,f)

def gen_plot(fname=None):

    fig,axs = plt.subplots(16,9,figsize=(20,20),tight_layout=True)

    muestra=0
    dcved = apply_step(show_dset[0],G(show_dset[0])[:,:3])
    #dcved = apply_step(show_dset[0],G(show_dset[0])[:,:3])


    for row in axs:
        for i,ax in enumerate(row):
            canal = i % 3
            m = muestra + i // 3
            ax.plot(show_dset[0][m,:,canal])
            ax.plot(dcved[m,:,canal])
            ax.set_xticklabels([])
            ax.set_yticklabels([])

            x,_ = ax.get_xlim()
            y,y2 = ax.get_ylim()

        muestra +=3

    if fname is None:
        plt.show()
    else:
        plt.savefig(fname)
        plt.close(fig)

# -

d,l = dset_tst.take(1).get_single_element()

# +
losses = []

def train(dataset, epochs, e0=0,datafolder=None):

    dd = []
    ll = []

    for data_batch in dataset:
        d,l = data_batch
        dd.append(d)
        ll.append(l)

    dd,ll = [tf.concat(dd,axis=0), tf.concat(ll,axis=0)]

    d,l = dset_tst.take(1).get_single_element()

    for epoch in range(epochs):

        epoch += e0
        start = time.time()

        """
        for k in range(1):
            for data_batch in dataset:
                train_D(*data_batch)

        for k in range(1):
            for data_batch in dataset:
                train_G(*data_batch)
        """
        for data_batch in dataset:
            train_step(*data_batch, beta=1)


        tr_loss = print_step(dd,ll)

        tst_loss = print_step(d,l)

        # Save the model every 15 epochs
        #if (epoch + 1) % 15 == 0:
        #    checkpoint.save(file_prefix = checkpoint_prefix)

        if (epoch + 1) % 10 == 0:
            print ('{} Time:{:.2f} s; loss Tr -> {} | Tst -> {}'\
                   .format(epoch + 1, time.time()-start,
                           loss_text.format(*tr_loss),
                           loss_text.format(*tst_loss)))

        if datafolder is None:
            pass
        elif (epoch + 1) % 10 == 0:
            gen_report('{}/{}.pq'.format(datafolder,epoch))

        losses.append([tr_loss,tst_loss])

    return losses


# +
#tf.keras.utils.plot_model(D,show_shapes=True)

# +
#tf.keras.utils.plot_model(G,show_shapes=True)

# +
#G(d)
assert tf.is_tensor(G(d))
assert tf.is_tensor(D(d))

assert tf.is_tensor(G2(d))
assert tf.is_tensor(D2(d))

# -

losses = train(dset,1000)

for data_batch in dset:
    data,labels = data_batch


def save_losses(fname):
    with open(fname,'w') as f:
        json.dump([*map(lambda z:[*map(lambda y:[*map(lambda x:x.numpy(),y)],z)],losses)])


# + tags=[]
plt.plot([i[0][0] for i in losses][0:],'.',label='TR-Dec',alpha=0.5)
plt.plot([i[1][0] for i in losses][0:],'.',label='TS-Dec',alpha=0.5)
#plt.ylim(-1,0)
plt.legend()
plt.show()

plt.plot([i[0][1] for i in losses][0:],'.',label='TR-Dis',alpha=0.5)
plt.plot([i[1][1] for i in losses][0:],'.',label='TS-Dis',alpha=0.5)
#plt.ylim(-1,0)

plt.legend()
plt.show()

plt.plot([i[0][2] for i in losses][0:],'.',label='TR-Dis',alpha=0.5)
plt.plot([i[1][2] for i in losses][0:],'.',label='TS-Dis',alpha=0.5)
#plt.ylim(-1,0)

plt.legend()
plt.show()

plt.plot([i[0][3] for i in losses][0:],'.',label='TR-Dis',alpha=0.5)
plt.plot([i[1][3] for i in losses][0:],'.',label='TS-Dis',alpha=0.5)
#plt.ylim(-1,0)

plt.legend()
plt.show()

plt.plot([i[0][4] for i in losses][0:],'.',label='TR-Dis',alpha=0.5)
plt.plot([i[1][4] for i in losses][0:],'.',label='TS-Dis',alpha=0.5)
#plt.ylim(-1,0)

plt.legend()
plt.show()

plt.plot([i[0][5] for i in losses][0:],'.',label='TR-Dis',alpha=0.5)
plt.plot([i[1][5] for i in losses][0:],'.',label='TS-Dis',alpha=0.5)
#plt.ylim(-1,0)

plt.legend()
plt.show()

plt.plot([i[0][6] for i in losses][0:],'.',label='TR-Dis',alpha=0.5)
plt.plot([i[1][6] for i in losses][0:],'.',label='TS-Dis',alpha=0.5)
#plt.ylim(-1,0)

plt.legend()
plt.show()
# -

gen_plot()


def gen_plotB(fname=None):

    fig,axs = plt.subplots(16,9,figsize=(20,20),tight_layout=True)

    muestra=0
    dcved = apply_step(show_dset[0],G(show_dset[0]))
    dcved = apply_step(dcved,G2(dcved))

    #dcved = apply_step(show_dset[0],G(show_dset[0])[:,:3])


    for row in axs:
        for i,ax in enumerate(row):
            canal = i % 3
            m = muestra + i // 3
            ax.plot(show_dset[0][m,:,canal])
            ax.plot(dcved[m,:,canal])
            ax.set_xticklabels([])
            ax.set_yticklabels([])

            x,_ = ax.get_xlim()
            y,y2 = ax.get_ylim()

        muestra +=3

    if fname is None:
        plt.show()
    else:
        plt.savefig(fname)
        plt.close(fig)


gen_plotB()

# + active=""
# !mkdir -p ckptsDi
# !mkdir -p ckpts

# + active=""
# checkpoint='ckpts/0.ckpt'
# deceiver.save_weights(checkpoint)
# checkpoint='ckptsDi/0.ckpt'
# discriminator.save_weights(checkpoint)
# -



# ##
