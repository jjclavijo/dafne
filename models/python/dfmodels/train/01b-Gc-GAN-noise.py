# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.13.5
#   kernelspec:
#     display_name: Python 3 (ipykernel)
#     language: python
#     name: python3
# ---

# # Variante 1: GAN con generador condicionado

# +
# #!/opt/venv/bin/python -m pip install pydot matplotlib
# #!apt-get -y -qq install graphviz
# -

import matplotlib.pyplot as plt

# + tags=[]
import dfmodels.models.model1noise as model
# -

D = model.discrim()
G = model.deceiv()


def G_(x,**kwargs):
    return G([x,tf.random.uniform([tf.shape(x)[0],3],-1,1)],**kwargs)


BUFFER = int(2e6)
BS = int(4096)

# +
from dafnedset.presets_simple import pos10,neg10
import dafnedset.tf.glue as glue
from dafnedset import fun_ops as fop
from dafnedset import fun_transformations as ftr

# load test dataset

todo_tst = pos10.parts[1] + neg10.parts[1]
dset_tst = glue.dafneFbToTf(todo_tst).cache().batch(BUFFER)

# +
# Load and augment train dataset

algo = pos10.parts[0] * 18 #+ neg10.parts[0] * 4

algo.options.batch_size = 1e5
algo = fop.map(algo,ftr.channel_mix_w_relabel(p_true_pos=1,p_true_pos_ch=1))
aumenta = fop.map(algo,ftr.random_rot_dir())

algo = neg10.parts[0] * 18
aumenta += fop.map(algo,ftr.random_rot_dir())

algo = pos10.parts[0] * 6 + neg10.parts[0] * 12
algo.options.batch_size = 1e5
algo = fop.map(algo,ftr.channel_mix_w_relabel(p_true_pos=1,p_true_pos_ch=1))
aumenta += fop.map(aumenta,ftr.random_rot_dir())

todo = (pos10.parts[0] + neg10.parts[0])
todo = todo + aumenta

todo.options.columns = ['este','norte','altura','etiqueta']

dset = glue.dafneFbToTf(todo).cache().shuffle(BUFFER).batch(BS)
# -

import tensorflow as tf


# +
#Define loss functions

def gf(v):
    v = tf.maximum(v,-9.0) # saturar V para evitar errores
    v = tf.minimum(v,21.0) # saturar V para evitar errores

    return v

def fstar(t):
    return 1/4 * t**2 + t

#def gf(v):
#    return tf.sigmoid(v)
#def fstar(t):
#    return t

def d_loss(fake_output, real_output):

    fo = gf(fake_output)
    ro = gf(real_output)

    return tf.reduce_mean(ro) + tf.reduce_mean(-fstar(fo))

def g_loss(fake_output):

    fo = gf(fake_output)

    return tf.reduce_mean(-fstar(fo))



# +
# Start optimizers

D_optimizer = tf.keras.optimizers.Adam(1e-5)
G_optimizer = tf.keras.optimizers.Adam(1e-5)

# +
# import aux functions
from dfmodels.tf.fun import apply_step

def part_pos_neg(data,labels):
    labels = labels[:,0] > 0.5

    data_p = tf.boolean_mask(data,labels)
    data_n = tf.boolean_mask(data,~labels)

    limit = tf.minimum(tf.size(data_p),tf.size(data_n))

    data_p = data_p[:limit]
    data_n = data_n[:limit]

    return data_p,data_n


# +
# Notice the use of `tf.function`
# This annotation causes the function to be "compiled".

stepfunc = apply_step

gt = tf.GradientTape

@tf.function
def train_step(data,labels,alpha=0):

    data_p,data_n = part_pos_neg(data,labels)

    with gt() as G_tape, gt() as D_tape:

        real_output = D(data_n, training=True)[:,0]

        fake_output = D(
                        stepfunc(data_p,G_(data_p, training=True))
                        #stepfunc(data_p,G(data_p, training=True))
                            , training=True)[:,0]

        D_loss = d_loss(fake_output, real_output)# + sim_cost
        G_loss = g_loss(fake_output) #+ sim_cost

    NG = G_tape.gradient(G_loss, G.trainable_variables)
    ND = D_tape.gradient(D_loss, D.trainable_variables)

    G_optimizer.apply_gradients(zip( NG , G.trainable_variables))

    D_optimizer.apply_gradients(zip(map(lambda x:-x,ND), D.trainable_variables))

# train_step could return d_loss and g_loss, but compiled functions are weird
def print_step(data,labels):

    data_p,data_n = part_pos_neg(data,labels)

    real_output = D(data_n, training=True)[:,0]

    fake_output = D(
                    stepfunc(data_p,G_(data_p, training=True))
                    #stepfunc(data_p,G(data_p, training=True))
                        , training=True)[:,0]

    D_loss = d_loss(fake_output, real_output)

    G_loss = g_loss(fake_output)

    return D_loss,G_loss


# +
import time

import json

import numpy as np

import pyarrow as pa

# +
p = pos10.parts[3]

show_dset = glue.dafneFbToTf(p).batch(BS).take(1).get_single_element()

def gen_report(fname=None):

    steps = deceiver(show_dset[0])
    discred = discriminator(show_dset[0])
    bothed = discriminator(stepfunc(show_dset[0],steps[:,:3]))

    if fname is None:
        pass
    else:
        with open(fname,'wb') as f:
            di = {'steps':steps.numpy().tolist(),
                  'disc':discred.numpy().tolist(),
                  'gen_o_disc':bothed.numpy().tolist()}
            tb = pa.Table.from_pydict(di)
            pa.parquet.write_table(tb,f)



# +
losses = []

def train(dataset, epochs, e0=0,datafolder=None):

    dd = []
    ll = []

    for data_batch in dataset:
        d,l = data_batch
        dd.append(d)
        ll.append(l)

    dd,ll = [tf.concat(dd,axis=0), tf.concat(ll,axis=0)]

    d,l = dset_tst.take(1).get_single_element()

    for epoch in range(epochs):

        epoch += e0
        start = time.time()

        for data_batch in dataset:
            train_step(*data_batch)

        g_loss,d_loss = print_step(dd,ll)
        tr_loss = [d_loss,g_loss]

        g_loss,d_loss = print_step(d,l)
        tst_loss = [d_loss,g_loss]

        # Save the model every 15 epochs
        #if (epoch + 1) % 15 == 0:
        #    checkpoint.save(file_prefix = checkpoint_prefix)

        if (epoch + 1) % 10 == 0:
            print ('Time for epoch {} is {} sec; loss is {:.4}-{:.4} {:.4}-{:.4}'.format(epoch + 1, time.time()-start, *tr_loss,*tst_loss))

        if datafolder is None:
            pass
        elif (epoch + 1) % 10 == 0:
            gen_report('{}/{}.pq'.format(datafolder,epoch))

        losses.append([tr_loss,tst_loss])

    return losses



# -

d,l = dset_tst.take(1).get_single_element()

tf.keras.utils.plot_model(D,show_shapes=True)

tf.keras.utils.plot_model(G,show_shapes=True)



#G(d)
G_(d)

D(d)

losses = train(dset,1000)

for data_batch in dset:
    data,labels = data_batch


def save_losses(fname):
    with open(fname,'w') as f:
        json.dump([*map(lambda z:[*map(lambda y:[*map(lambda x:x.numpy(),y)],z)],losses)])


# + tags=[]
plt.plot([i[0][1] for i in losses],label='TR-Dec')
plt.plot([i[1][1] for i in losses],label='TS-Dec')
#plt.ylim(-1,0)
plt.legend()
plt.show()

plt.plot([i[0][0] for i in losses],label='TR-Dis')
plt.plot([i[1][0] for i in losses],label='TS-Dis')
#plt.ylim(-1,0)

plt.legend()
plt.show()
# -

show_dset[0].shape


def gen_plot(fname=None):

    fig,axs = plt.subplots(16,9,figsize=(20,20),tight_layout=True)

    muestra=0
    dcved = apply_step(show_dset[0],G_(show_dset[0])[:,:3])
    #dcved = apply_step(show_dset[0],G(show_dset[0])[:,:3])
    discred = D(show_dset[0])
    bothed = D(apply_step(show_dset[0],G_(show_dset[0])[:,:3]))
    #bothed = D(apply_step(show_dset[0],G(show_dset[0])[:,:3]))

    mmax = max(max(discred[:,0]),max(bothed[:,0]))
    mmin = min(min(discred[:,0]),min(bothed[:,0]))

    #mmax1 = max(max(discred[:,1]),max(bothed[:,1]))
    #mmin1 = min(min(discred[:,1]),min(bothed[:,1]))

    d0 = (discred[:,0] - mmin) / (mmax-mmin)
    #d1 = (discred[:,1] - mmin1) / (mmax1-mmin1)

    b0 = (bothed[:,0] - mmin) / (mmax-mmin)
    #b1 = (bothed[:,1] - mmin1) / (mmax1-mmin1)


    for row in axs:
        for i,ax in enumerate(row):
            canal = i % 3
            m = muestra + i // 3
            ax.plot(show_dset[0][m,:,canal])
            ax.plot(dcved[m,:,canal])
            ax.set_xticklabels([])
            ax.set_yticklabels([])

            x,_ = ax.get_xlim()
            y,y2 = ax.get_ylim()
            ax.text(x,y,'{:.2f},{:.2f}'.format(d0[m],b0[m]))
            #ax.text(x,y2,'{:.2f},{:.2f}'.format(b0[m],b1[m]))
            #ax.text(x,y2,'{}'.format(show_dset[1][m]))

        muestra +=3

    if fname is None:
        plt.show()
    else:
        plt.savefig(fname)
        plt.close(fig)


gen_plot()


def gen_plot(fname=None):

    fig,axs = plt.subplots(16,9,figsize=(20,20),tight_layout=True)

    muestra=0

    S = apply_step
    d = show_dset[0]

    dcved = S(d,G_(d))

    dcved = S(dcved,G_(dcved))
    dcved = S(dcved,G_(dcved))
    dcved = S(dcved,G_(dcved))
    dcved = S(dcved,G_(dcved))
    dcved = S(dcved,G_(dcved))

    #dcved = apply_step(show_dset[0],G(show_dset[0])[:,:3])
    discred = D(d)
    bothed = D(S(d,G_(d)))
    #bothed = D(apply_step(show_dset[0],G(show_dset[0])[:,:3]))

    mmax = max(max(discred[:,0]),max(bothed[:,0]))
    mmin = min(min(discred[:,0]),min(bothed[:,0]))

    #mmax1 = max(max(discred[:,1]),max(bothed[:,1]))
    #mmin1 = min(min(discred[:,1]),min(bothed[:,1]))

    d0 = (discred[:,0] - mmin) / (mmax-mmin)
    #d1 = (discred[:,1] - mmin1) / (mmax1-mmin1)

    b0 = (bothed[:,0] - mmin) / (mmax-mmin)
    #b1 = (bothed[:,1] - mmin1) / (mmax1-mmin1)


    for row in axs:
        for i,ax in enumerate(row):
            canal = i % 3
            m = muestra + i // 3
            ax.plot(show_dset[0][m,:,canal])
            ax.plot(dcved[m,:,canal])
            ax.set_xticklabels([])
            ax.set_yticklabels([])

            x,_ = ax.get_xlim()
            y,y2 = ax.get_ylim()
            ax.text(x,y,'{:.2f},{:.2f}'.format(d0[m],b0[m]))
            #ax.text(x,y2,'{:.2f},{:.2f}'.format(b0[m],b1[m]))
            #ax.text(x,y2,'{}'.format(show_dset[1][m]))

        muestra +=3

    if fname is None:
        plt.show()
    else:
        plt.savefig(fname)
        plt.close(fig)


gen_plot()

# !mkdir -p ckptsDi
# !mkdir -p ckpts

checkpoint='ckpts/1.ckpt'
G.save_weights(checkpoint)
checkpoint='ckptsDi/1.ckpt'
D.save_weights(checkpoint)


