# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.13.5
#   kernelspec:
#     display_name: Python 3 (ipykernel)
#     language: python
#     name: python3
# ---

# # Variante 1: GAN con generador condicionado

# +
# #!/opt/venv/bin/python -m pip install pydot matplotlib
# #!apt-get -y -qq install graphviz
# -

## Imports

import tensorflow as tf
import matplotlib.pyplot as plt

import time
import json
import numpy as np
from pyarrow import Table as paTable, parquet as pq

import click

import dfmodels.models.model1 as model
from dfmodels.tf.fun import apply_step

import logging


logging.basicConfig()
log = logging.getLogger('DFTRAIN')
log.setLevel(logging.INFO)


# Constants
BUFFER = int(2e6)

# Hyperparameters

D_DROPOUT = 0.0
D_SN = False
D_LR = 1e-5

G_DROPOUT = 0.05
G_SN = False
G_LR = 1e-5

BS = 256
EPOCHS = 1000

# Regularization
REG_NORM = 'None'
REG_N = False
REG_SELF = False
STEP_ROTATES = False
ALPHA = 0.1

# Files

DATASET='Datasaves'

REPORT='01.GcGAN'
LOSS_FNAME='01.GcGAN.json'
CKPT='01.GcGAN.cp'



# Functional Functions

#Define loss functions, F-GAN: https://www.semanticscholar.org/paper/f-GAN%3A-Training-Generative-Neural-Samplers-using-Nowozin-Cseke/ffdcad14d2f6a12f607b59f88da4a939f4821691

# X^2-Person divergence
def gf(v):
    v = tf.maximum(v,-9.0) # saturar V para evitar errores
    v = tf.minimum(v,21.0) # saturar V para evitar errores

    return v

def fstar(t):
    return 1/4 * t**2 + t
    #return tf.math.exp(t-1)

def d_loss(fake_output, real_output):

    fo = gf(fake_output)
    ro = gf(real_output)

    return tf.reduce_mean(ro) + tf.reduce_mean(-fstar(fo))

def g_loss(fake_output):

    fo = gf(fake_output)
    return tf.reduce_mean(-fstar(fo))

# +
# Start optimizers


@tf.function
def part_pos_neg(data,labels):
    labels = labels[:,0] > 0.5

    data_p = tf.boolean_mask(data,labels)
    data_n = tf.boolean_mask(data,~labels)

    limit = tf.minimum(tf.size(data_p),tf.size(data_n))

    data_p = data_p[:limit]
    data_n = data_n[:limit]

    return data_p,data_n


def save_losses(fname,losses):
    with open(fname,'w') as f:
        json.dump([*map(lambda z:[*map(lambda y:[*map(lambda x:float(x.numpy()),y)],z)],losses)],f)

gt = tf.GradientTape
S_ = apply_step

@click.command()
@click.option('--D_DROPOUT', default=D_DROPOUT)
@click.option('--D_SN', default=D_SN)
@click.option('--D_LR', default=D_LR)
@click.option('--G_DROPOUT', default=G_DROPOUT)
@click.option('--G_SN', default=G_SN)
@click.option('--G_LR', default=G_LR)
@click.option('--BS', default=BS)
@click.option('--DATASET', default=DATASET)
@click.option('--REPORT', default=REPORT)
@click.option('--REPORT_TEST', default=False)
@click.option('--REPORT_VALIDATION', default=False)
@click.option('--REPORT_PERIOD', default=10)
@click.option('--LOSS_FNAME', default=LOSS_FNAME)
@click.option('--CKPT', default=CKPT)
@click.option('--REG_NORM', default=REG_NORM)
@click.option('--REG_N', default=REG_N)
@click.option('--REG_SELF', default=REG_SELF)
@click.option('--ALPHA', default=ALPHA)
@click.option('--EPOCHS', default=EPOCHS)
@click.option('--STEP_ROTATES', default=STEP_ROTATES)

def main_loop(
    d_dropout ,
    d_sn ,
    d_lr ,
    g_dropout ,
    g_sn ,
    g_lr ,
    bs ,
    dataset ,
    report ,
    report_test ,
    report_validation ,
    report_period ,
    loss_fname ,
    ckpt ,
    reg_norm ,
    reg_n ,
    reg_self ,
    alpha,
    epochs,
    step_rotates):

    log.info('Initializing Networks')


    all_options={
            'D Dropout Rate':d_dropout ,
            'D Spectral Norm':d_sn ,
            'D learning rate':d_lr ,
            'G Dropout Rate':g_dropout ,
            'G Spectral Norm':g_sn ,
            'G Learning Rate':g_lr ,
            'Batch Size':bs ,
            'Alpha':alpha ,
            'Dataset Folder':dataset ,
            'Report Folder':report ,
            'Loss File':loss_fname ,
            'Checkpoints Folder':ckpt ,
            'Regularization Self':reg_self ,
            'Regularization Negative':reg_n ,
            'Regularization Norm':reg_norm ,
            'Epochs':epochs,
            'Step Rotates':step_rotates
            }

    if step_rotates:
        from dfmodels.tf.fun import apply_step_R
        S = apply_step_R
    else:
        S = apply_step

    log.info(all_options)

    PARS_D={'units_feat': [60,60,60,30,30,30],
                   'units_dicc': [30,30,30,30],
                   'dropout_rate': d_dropout
                   }

    PARS_G={'units_feat': [60,60,60,30,30,30],
                   'units_dicc': [30,30,30,30],
                   'dropout_rate': g_dropout
                   }

    log.info('Initializing Networks')

    D = model.discrim(SpectralNorm=d_sn, params = PARS_D)
    G = model.deceiv(SpectralNorm=g_sn, params = PARS_G)

    BS = int(bs)

    # DatasetLoading
    log.info('Dataset Loading')

    dset = tf.data.experimental.load('{}/train'.format(dataset)).batch(BS)
    dset_tst = tf.data.experimental.load('{}/test'.format(dataset)).batch(BUFFER)
    dset_val = tf.data.experimental.load('{}/validation'.format(dataset)).batch(BUFFER)
    show_dset= tf.data.experimental.load('{}/show'.format(dataset)).batch(BS).take(1).get_single_element()

    show_dset = (show_dset[0],show_dset[1][:,0])

    d,l,*_ = dset.take(1).get_single_element()

    # End DatasetLoading

    if not report is None:
        import os
        from errno import EEXIST
        try:
            os.makedirs(report)
        except OSError as e:
            if e.errno != EEXIST:
                raise

    log.info('Optimizers Inicialization')
    D_optimizer = tf.keras.optimizers.Adam(d_lr)
    G_optimizer = tf.keras.optimizers.Adam(g_lr)

    if reg_norm == 'square':
        log.info('L2 norm regularization:Ok')
        norm = tf.math.square
    elif reg_norm == 'abs':
        log.info('L1 norm regularization:Ok')
        norm = tf.abs
    else:
        norm = lambda x:0

    @tf.function
    def train_step(data,labels,alpha=0.1):

        data_p,data_n = part_pos_neg(data,labels)

        with gt() as G_tape, gt() as D_tape:

            real_output = D(data_n, training=True)

            fake_output = D(S(data_p,G(data_p, training=True))
                                , training=True)

            # Consistency regularization.
            s1 = []
            if reg_self:
                s0 = G(d, training=True)
                s1.append(G(S(d,s0), training=True))
            if reg_n:
                s1.append(G(data_n, training=True))

            if reg_n or reg_self:
                l2pen = tf.reduce_mean(norm(tf.concat(s1,axis=0)))
            else:
                l2pen = 0

            D_loss = d_loss(fake_output, real_output)# + sim_cost
            G_loss = g_loss(fake_output) + l2pen * alpha

        NG = G_tape.gradient(G_loss, G.trainable_variables)
        ND = D_tape.gradient(D_loss, D.trainable_variables)

        G_optimizer.apply_gradients(zip( NG , G.trainable_variables))

        D_optimizer.apply_gradients(zip(map(lambda x:-x,ND), D.trainable_variables))

    # train_step could return d_loss and g_loss, but compiled functions are weird
    @tf.function
    def print_step(data,labels):

        data_p,data_n = part_pos_neg(data,labels)

        real_output = D(data_n, training=True)
        fake_output = D(
                        S(data_p,G(data_p)))

        # Consistency regularization.
        s1 = []
        if reg_self:
            s0 = G(d, training=True)
            s1.append(G(S(d,s0), training=True))
        if reg_n:
            s1.append(G(data_n, training=True))

        if reg_n or reg_self:
            l2pen = tf.reduce_mean(norm(tf.concat(s1,axis=0)))
        else:
            l2pen = 0

        D_loss = d_loss(fake_output, real_output)
        G_loss = g_loss(fake_output)

        return D_loss,G_loss,l2pen

    def gen_report(fname=None,ds=None):

        if ds is None:
            return

        steps = G(ds)
        Dout = D(ds)
        DSGout = D(S_(ds,steps))

        if fname is None:
            pass
        else:
            with open(fname,'wb') as f:
                di = {'steps':steps.numpy().tolist(),
                      'disc':Dout.numpy().tolist(),
                      'gen_o_disc':DSGout.numpy().tolist()}
                tb = paTable.from_pydict(di)
                pq.write_table(tb,f)

    losses = []

    loss_text = 'D:{:.3f} G:{:.3f} Lnorm: {:.3f}'

    def train(dataset, epochs, e0=0,datafolder=None):

        log.info('Start Training')

        dd = []
        ll = []

        for data_batch in dataset:
            d,l,*_ = data_batch
            dd.append(d)
            ll.append(l)

        dd,ll = [tf.concat(dd,axis=0), tf.concat(ll,axis=0)]

        d,l,*_ = dset_tst.take(1).get_single_element()
        dv,lv,*_ = dset_val.take(1).get_single_element()

        for epoch in range(epochs):

            epoch += e0
            start = time.time()

            for data_batch in dataset:
                d,l,*_ = data_batch
                train_step(d,l)

            tr_loss = print_step(dd,ll)

            tst_loss = print_step(d,l)

            # Save the model every 15 epochs
            #if (epoch + 1) % 15 == 0:
            #    checkpoint.save(file_prefix = checkpoint_prefix)

            if (epoch + 1) % 10 == 0:
                log.info('{} Time:{:.2f} s; loss Tr -> {} | Tst -> {}'\
                       .format(epoch + 1, time.time()-start,
                               loss_text.format(*tr_loss),
                               loss_text.format(*tst_loss)))

            if (not datafolder is None) and \
                ((epoch + 1) % report_period == 0):
                log.info('generating reports...')
                gen_report('{}/{}.pq'.format(datafolder,epoch),show_dset[0])
                if report_test:
                    gen_report('{}/t{}.pq'.format(datafolder,epoch),d)
                if report_validation:
                    gen_report('{}/v{}.pq'.format(datafolder,epoch),dv)
                log.info('...|')
                #print ('reports Generated')

            if ckpt is None:
                pass
            elif (epoch + 1) % 10 == 0:
                log.info('Saving checkpoints...')
                checkpoint='{}G/{:04d}.ckpt'.format(ckpt,epoch)
                G.save_weights(checkpoint)
                checkpoint='{}D/{:04d}.ckpt'.format(ckpt,epoch)
                D.save_weights(checkpoint)
                log.info('...!')

            losses.append([tr_loss,tst_loss])

        return losses

    losses = train(dset,epochs,datafolder=report)

    for data_batch in dset:
        data,labels = data_batch

    save_losses(loss_fname,losses)

    return None

if __name__ == '__main__':
    exit(main_loop())
