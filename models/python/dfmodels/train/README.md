# Training schemes

This training schemes are often closely related to the models they are meant
to train.

The schemes used in the publication are the ones labelled 01 and 02

01-Gc-GAN.py: The main model presented on the paper
01b-Gc-GAN-noise.py: The model of the paper with noise added on the inputs,
                     it showed no particular benefit and was discarded
02-cGAN-Labels-new.py: The second model published, that learns the labels
025-cGAN.py: A model which learns the labels, but there are two generators,
             the second one tries to learn how to generate D1 samples by trying
             to reconstruct the steps of samples already transformed
             The result was that this second generator cant learn anything.
03-CycleGAN-new.py: A scheme based on the CycleGAN.
04-CycleGAN-Full.py: Other scheme based on CycleGAN.

This modules can be called as commandline scripts, some examples are provided.

For the paper experiments we automatized the calls to cross-validate several
parameter values. Those routines are provided on a separate section of the
repository.
