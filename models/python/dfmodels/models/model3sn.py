import tensorflow as tf
from ..tf import layers as daflayers
import tensorflow_addons as tfa

SpectralNormalization = tfa.layers.SpectralNormalization

DATATYPE = tf.float32

MODELPARAMS_D={'units_feat': [60,60,60,30,30,30],
               'units_dicc': [30,30,30,30],
               'n_classes':1}

MODELPARAMS_E={'units_feat': [60,60,60,30,30,30],
               'units_dicc': [30,30,30,30],
               'n_outs':3}


def discrim(params = MODELPARAMS_D, dropout_rate=0.2, **kwargs):
    """
    Create Discriminator model with keras functional API.
    Returns model.

    Arguments:
    ---------

    params:     Dictionary with:
                units_feat: number of units for
                            layers with channel_preserved input.
                untts_dicc: number of units for
                            layers with channel_flattened input.
                n_classes:  number of outputs for the model
    """

    for i in params:
        if i in kwargs:
            params[i] = kwargs[i]

    inputs = tf.keras.layers.Input(shape=[61,3],dtype=DATATYPE)

    # Dropout on input is simultaneous to simulate lost times
    x = tf.keras.layers.Dropout(rate=dropout_rate, noise_shape=[None,61,1],
                          name='d/dropout-in',dtype=DATATYPE)(inputs) * dropout_rate

    # Drop the epoch at the earthquke, which adds confusing information
    x = daflayers.DropEpoch(epoch=31,axis=1,dtype=DATATYPE)(x)

    x = daflayers.UnfoldMask()(x) # NaN to 0 + mask


    capas = params['units_feat']

    dense_opts = lambda i,units : {'units': units,
                                   'activation': None,
                                   'name': 'd/dense_{}'.format(i),
                                   'dtype': DATATYPE}

    drop_opts = lambda i : {'rate': 0.00,
                            'name': 'd/drop_{}'.format(i),
                            'dtype': DATATYPE}

    x = tf.transpose(x,[0,2,1]) # batch, n, 3 --> batch, 3, n
                                # each channel will be treated equal
                                # when applying dense layers.

    for i, u in enumerate(capas):
        drop = tf.keras.layers.Dropout(**drop_opts(i))
        dense = SpectralNormalization(tf.keras.layers.Dense(**dense_opts(i,u)))
        activ = tf.keras.layers.LeakyReLU()

        x = dense(x)
        x = activ(x)
        x = drop(x)

    # Flatten channels, each channel imformation treated independent
    x = tf.reshape(x,[-1,u*3])

    st = i + 1
    capas = params['units_dicc']

    for i, units in enumerate(capas):
        x = SpectralNormalization(tf.keras.layers.Dense(**dense_opts(st+i,u)))(x)
        x = tf.keras.layers.LeakyReLU()(x)
        x = tf.keras.layers.Dropout(**drop_opts(st+i))(x)

    # Compute logits (1 per class).
    logits = tf.keras.layers.Dense(units=params['n_classes'], activation=None,
                             name='discr/logits',dtype=DATATYPE)(x)

    discriminador = tf.keras.Model(inputs=inputs,outputs=logits)


    return discriminador

#####                      #####
###  Start of Deceiver Model ###
#####                      #####

def deceiv(params=MODELPARAMS_E, dropout_rate=0.2, **kwargs):
    """
    Create Deceiver model with keras functional API.
    Returns model.

    Arguments:
    ---------

    params:     Dictionary with:
                units_feat: number of units for
                            layers with channel_preserved input.
                untts_dicc: number of units for
                            layers with channel_flattened input.
                n_outs:  number of outputs for the model
    """

    for i in params:
        if i in kwargs:
            params[i] = kwargs[i]

    inputs = tf.keras.layers.Input(shape=[61,3],dtype=DATATYPE)

    # Dropout on input is simultaneous to simulate lost times
    x = tf.keras.layers.Dropout(rate=dropout_rate, noise_shape=[None,61,1],
                          name='g/dropout-in',dtype=DATATYPE)(inputs) * dropout_rate

    #x = daflayers.DropEpoch(epoch=31,axis=1,dtype=DATATYPE)(x)

    x = daflayers.UnfoldMask()(x) # -99 to 0 + mask
    x,mask = tf.split(x,2,axis=1)

    capas = params['units_feat']

    dense_opts = lambda i,units : {'units': units,
                                   'activation': None,
                                   'name': 'g/dense_{}'.format(i),
                                   'dtype': DATATYPE}

    drop_opts = lambda i : {'rate': 0.05,
                            'name': 'g/drop_{}'.format(i),
                            'dtype': DATATYPE}


    x = tf.transpose(x,[0,2,1]) # batch, n, 3 --> batch, 3, n
                                # each channel will be treated equal
                                # when applying dense layers.

    for i, u in enumerate(capas):
        drop = tf.keras.layers.Dropout(**drop_opts(i))
        dense = SpectralNormalization(tf.keras.layers.Dense(**dense_opts(i,u)))
        activ = tf.keras.layers.LeakyReLU()

        x = dense(x)
        x = activ(x)
        x = drop(x)

    # Flatten channels
    x = tf.reshape(x,[-1,u*3])

    st = i+1
    capas = params['units_dicc']

    for i, units in enumerate(capas):
        x = SpectralNormalization(tf.keras.layers.Dense(**dense_opts(st+i,u)))(x)
        x = tf.keras.layers.LeakyReLU()(x)
        x = tf.keras.layers.Dropout(**drop_opts(st+i))(x)

    outputs = tf.keras.layers.Dense(units=params['n_outs'], activation=None,
                             name='g/magnitudes',dtype=DATATYPE)(x)

    estafador = tf.keras.Model(inputs=inputs,outputs=outputs)

    return estafador
