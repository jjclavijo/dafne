import tensorflow as tf
from ..tf import layers as daflayers

DATATYPE = tf.float32

"""
MODELPARAMS_D={'units_feat': [60,60,60,30,30,30],
               'units_dicc': [30,30,30,30],
               'dropout_rate':0.05}

MODELPARAMS_G={'units_feat': [60,60,60,30,30,30],
               'units_dicc': [30,30,30,30],
               'dropout_rate':0.05}
"""

MODELPARAMS_D={'units_feat': [60,60,30],
               'units_dicc': [30,30],
               'dropout_rate':0.05}

MODELPARAMS_G={'units_feat': [60,60,30],
               'units_dicc': [30,30],
               'dropout_rate':0.05}

def discrim(params = MODELPARAMS_D, **kwargs):
    """
    Create Discriminator model with keras functional API.
    Returns model.

    Arguments:
    ---------

    params:     Dictionary with:
                units_feat: number of units for
                            layers with channel_preserved input.
                untts_dicc: number of units for
                            layers with channel_flattened input.
    """

    for i in params:
        if i in kwargs:
            params[i] = kwargs[i]

    dropout_rate = params.get('dropout_rate',0.0)

    if kwargs.get("SpectralNorm",False):
        import tensorflow_addons as tfa
        Normalization = tfa.layers.SpectralNormalization
    else:
        Normalization = lambda x: x

    data_in = tf.keras.layers.Input(shape=[61,3],dtype=DATATYPE)

    label_in = tf.keras.layers.Input(shape=[1])

    l = tf.keras.layers.Embedding(2,61)(label_in)

    # Intento desesperado de que no se fije tanto en las etiquetas!
    n = tf.random.normal(shape=tf.shape(l))
    l = n + l

    l = tf.keras.layers.Reshape([-1])(l)

    inputs = [data_in,label_in]

    # Dropout on input is simultaneous to simulate lost times
    x = tf.keras.layers.Dropout(rate=dropout_rate, noise_shape=[None,61,1],
                          name='d/dropout-in',dtype=DATATYPE)(data_in)


    #x = tf.keras.layers.Concatenate()([x,l])

    # Drop the epoch at the earthquke, which adds confusing information
    x = daflayers.DropEpoch(epoch=31,axis=1,dtype=DATATYPE)(x)

    x = daflayers.UnfoldMask()(x) # NaN to 0 + mask

    capas = params['units_feat']

    dense_opts = lambda i,units : {'units': units,
                                   'activation': None,
                                   'name': 'd/dense_{}'.format(i),
                                   'dtype': DATATYPE}

    drop_opts = lambda i : {'rate': dropout_rate,
                            'name': 'd/drop_{}'.format(i),
                            'dtype': DATATYPE}

    x = tf.transpose(x,[0,2,1]) # batch, n, 3 --> batch, 3, n
                                # each channel will be treated equal
                                # when applying dense layers.

    for i, u in enumerate(capas):
        drop = tf.keras.layers.Dropout(**drop_opts(i))
        dense = tf.keras.layers.Dense(**dense_opts(i,u))
        dense = Normalization(dense)
        activ = tf.keras.layers.LeakyReLU()

        x = dense(x)
        x = activ(x)
        x = drop(x)

    # Flatten channels, each channel imformation treated independent
    #x = tf.reshape(x,[-1,u*4])
    x = tf.reshape(x,[-1,u*3])

    x = tf.keras.layers.Concatenate()([x,l])

    st = i + 1
    capas = params['units_dicc']

    for i, units in enumerate(capas):
        dense = tf.keras.layers.Dense(**dense_opts(st+i,u))
        dense = Normalization(dense)
        activ = tf.keras.layers.LeakyReLU()
        dropout = tf.keras.layers.Dropout(**drop_opts(st+i))

        x = dense(x)
        x = activ(x)
        x = dropout(x)

    # Compute logits (1 per class).
    logits = tf.keras.layers.Dense(units=1, activation=None,
                             name='discr/logits',dtype=DATATYPE)(x)

    discriminador = tf.keras.Model(inputs=inputs,outputs=logits)


    return discriminador

#####                      #####
###  Start of Deceiver Model ###
#####                      #####

def deceiv(params=MODELPARAMS_G, **kwargs):
    """
    Create Deceiver model with keras functional API.
    Returns model.

    Arguments:
    ---------

    params:     Dictionary with:
                units_feat: number of units for
                            layers with channel_preserved input.
                untts_dicc: number of units for
                            layers with channel_flattened input.
    """

    for i in params:
        if i in kwargs:
            params[i] = kwargs[i]

    dropout_rate = params.get('dropout_rate',0.05)

    if kwargs.get("SpectralNorm",False):
        import tensorflow_addons as tfa
        Normalization = tfa.layers.SpectralNormalization
    else:
        Normalization = lambda x: x

    data_in = tf.keras.layers.Input(shape=[61,3],dtype=DATATYPE)

    #noise_in = tf.keras.layers.Input(shape=[3],dtype=DATATYPE)

    #n = tf.keras.layers.Dense(61)(noise_in)
    #n = tf.keras.layers.Reshape([-1,1])(n)

    label_in = tf.keras.layers.Input(shape=[1],dtype=tf.int32)

    l = tf.keras.layers.Embedding(2,61)(label_in)

    # Intento desesperado de que no se fije tanto en las etiquetas!
    n = tf.zeros_like(l)
    l = n * l

    l = tf.keras.layers.Reshape([-1])(l)

    #inputs = [data_in,noise_in,label_in]
    inputs = [data_in,label_in]

    # Dropout on input is simultaneous to simulate lost times
    x = tf.keras.layers.Dropout(rate=dropout_rate, noise_shape=[None,61,1],
                          name='g/dropout-in',dtype=DATATYPE)(data_in)


    #x = tf.keras.layers.Concatenate()([x,l])

    x = daflayers.DropEpoch(epoch=31,axis=1,dtype=DATATYPE)(x)

    x = daflayers.UnfoldMask()(x) # -99 to 0 + mask
    x,mask = tf.split(x,2,axis=1)

    capas = params['units_feat']

    dense_opts = lambda i,units : {'units': units,
                                   'activation': None,
                                   'name': 'g/dense_{}'.format(i),
                                   'dtype': DATATYPE}

    drop_opts = lambda i : {'rate': 0.05,
                            'name': 'g/drop_{}'.format(i),
                            'dtype': DATATYPE}


    x = tf.transpose(x,[0,2,1]) # batch, n, 3 --> batch, 3, n
                                # each channel will be treated equal
                                # when applying dense layers.

    for i, u in enumerate(capas):
        drop = tf.keras.layers.Dropout(**drop_opts(i))
        dense = tf.keras.layers.Dense(**dense_opts(i,u))
        dense = Normalization(dense)
        activ = tf.keras.layers.LeakyReLU()

        x = dense(x)
        x = activ(x)
        x = drop(x)

    # Flatten channels
    #x = tf.reshape(x,[-1,u*4])
    x = tf.reshape(x,[-1,u*3])

    x = tf.keras.layers.Concatenate()([x,l])

    st = i+1
    capas = params['units_dicc']

    for i, units in enumerate(capas):
        dense = tf.keras.layers.Dense(**dense_opts(st+i,u))
        dense = Normalization(dense)
        activ = tf.keras.layers.LeakyReLU()
        dropout = tf.keras.layers.Dropout(**drop_opts(st+i))

        x = dense(x)
        x = activ(x)
        x = dropout(x)

    outputs = tf.keras.layers.Dense(units=3, activation=None,
                             name='g/magnitudes',dtype=DATATYPE)(x)

    estafador = tf.keras.Model(inputs=inputs,outputs=outputs)

    return estafador
