import tensorflow as tf
from . import layers as l
from . import metrics_losses as ll

DATATYPE = tf.float32

def multiplex(filtro: tf.keras.Model ,prop: float = 0.5) -> tf.keras.Model:
    """
           data+labels
               |
               |
              / \
             /   \
         1-prop  prop
          /         \
         Id       Filtro
         |            |
       data,     Filtro(data),
    [*labels,0]  [*labels,1]

    """
    #TODO: drop hardcoded shapes

    data = tf.keras.Input(shape=[61,3])
    label = tf.keras.Input(shape=[2])

    sellabels = l.RandomBool()(data)

    #TODO: more hardcoded shapes
    selector = tf.reshape(tf.cast(sellabels,bool),[-1,1,1])

    fdata = filtro(data)

    x = tf.where(selector,fdata,data)

    y = tf.concat([label,sellabels],axis=-1)

    return tf.keras.Model(inputs=[data,label],outputs=[x,y])

"""
Basic Training:

    get_trainer_det: Train discriminator against dataset.

    get_trainer_dec: Train deceiver against pre-trained discriminator.

    get_trainer_dec_wo_step: Train deceiver (which outputs step amounts)
                             against pre-trained discriminator.

"""

def get_trainer_det(model,optimizer=None):
    if optimizer is None:
        optimizer = tf.keras.optimizers.Adam(learning_rate=0.005)#(learning_rate=0.1)

    loss_fn = ll.get_loss_case_det(model)
    pred_loss_fn = ll.get_loss_case_det(model,training=False)

    @tf.function
    def grad(inputs, targets):

        with tf.GradientTape() as tape:
            #loss_value = adv_vs_det_loss(adv,discr, inputs, targets, training=True)
            loss_value = loss_fn(inputs, targets)

        return loss_value, tape.gradient(loss_value, model.trainable_variables)

    def train_step(inputs, targets):
        loss, grads = grad(inputs,targets)

        optimizer.apply_gradients(zip(grads, model.trainable_variables))
        #metric.update_state(loss)

        return pred_loss_fn(inputs,targets)

    return tf.function(train_step,
               input_signature=[
                   tf.TensorSpec(shape=(None, 61, 3), dtype=tf.float32),
                   tf.TensorSpec(shape=(None, 2), dtype=tf.float32)
               ]
           )

#TODO: DRY
def get_trainer_dec(model,discriminador,optimizer=None):

    if optimizer is None:
        optimizer = tf.keras.optimizers.Adadelta(learning_rate=0.01)#(learning_rate=0.1)

    loss_fn = ll.get_loss_case_dec(model,discriminador)
    pred_loss_fn = ll.get_loss_case_dec(model,discriminador,training=False)

    def grad(inputs, targets):
        with tf.GradientTape() as tape:
            #loss_value = loss_no_orig(model, inputs, targets, training=True)
            loss_value = loss_fn(inputs, targets)
            #loss_value = tf.reduce_sum(loss_value)
        return loss_value, tape.gradient(loss_value, model.trainable_variables)

    def train_step(inputs, targets):
        loss, grads = grad(inputs,targets)

        optimizer.apply_gradients(zip(grads, model.trainable_variables))

        return pred_loss_fn(inputs,targets)

    return tf.function(train_step,
               input_signature=[
                   tf.TensorSpec(shape=(None, 61, 3), dtype=tf.float32),
                   tf.TensorSpec(shape=(None, 2), dtype=tf.float32)
               ]
           )

def get_trainer_dec_wo_step(model,discriminador,optimizer=None):

    #####                         #####
    ###  Build full Estafador Model ###
    #####                         #####

    tr_inputs = tf.keras.layers.Input(shape=[61,3],dtype=DATATYPE)

    # Drop the epoch at the earthquke, which adds confusing information
    # origing of confusion, ear
    droped_inputs = l.DropEpoch(epoch=31,axis=1,dtype=DATATYPE)(tr_inputs)
    di = l.UnfoldMask()(droped_inputs) # -99 to 0 + mask
    di,dimask = tf.split(di,2,axis=1)

    steps = model(tr_inputs)

    out = l.ApplyStep(epoch=30,dtype=DATATYPE)([di,steps])

    out = tf.concat([out,dimask],axis=1)

    out = l.ApplyMask()(out)

    out = l.AddDummyEpoch(epoch=30,dtype=DATATYPE)(out)

    estafador = tf.keras.Model(inputs=tr_inputs,outputs=out)

    #####                     #####
    ###  End of Estafador Model ###
    #####                     #####

    return get_trainer_dec(estafador,discriminador,optimizer)

