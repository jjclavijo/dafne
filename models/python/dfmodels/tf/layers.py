import tensorflow as tf
import numpy as np
import logging

log = logging.getLogger(__name__)

DATATYPE=tf.float32


class DropEpoch(tf.keras.layers.Layer):
  """
  Layer for droping one epoch on the time axis

  Tensor(batches,epochs,chanels) -> Tensor(batches,epochs-i,chanels)
  """

  def __init__(self,epoch=31,axis=1,**kwargs):
    super(DropEpoch, self).__init__(**kwargs)
    self.epoch = epoch
    self.axis = axis
    self.before = epoch - 1

  def build(self, input_shape):
    self.after = input_shape[self.axis] - self.epoch
    if self.after < 0:
        raise TypeError('Tensor de dimension incorrecta')

  def call(self, inputs):
    b,_,a = tf.split(inputs,[self.before,1,self.after],axis=self.axis)

    outputs = tf.concat([b,a], axis=self.axis)

    #outputs = tf.subtract(outputs,
    #            tf.reduce_mean(outputs,self.axis,
    #                keepdims=True))

    return outputs

  def get_config(self):

    config = super().get_config().copy()

    config.update({
        'epoch': self.epoch,
        'axis': self.axis,
    })
    return config

class AddDummyEpoch(tf.keras.layers.Layer):

  def __init__(self,epoch=30,axis=1,**kwargs):
    super(AddDummyEpoch, self).__init__(**kwargs)
    self.epoch = epoch
    self.axis = axis
    self.before = epoch

  def build(self, input_shape):
    self.after = input_shape[self.axis] - self.epoch
    if self.after < 0:
        raise TypeError('Tensor de dimension incorrecta')
    shape = [1,*input_shape[1:]]
    shape[self.axis] = 1

    # HACK
    self.ndim = len(input_shape)

    log.debug('input_shape: {}, ndim: {}'.format(input_shape,self.ndim))

    self.zero = tf.zeros(shape)

  def call(self, inputs):
    b,a = tf.split(inputs,[self.before,self.after],axis=self.axis)
    bshape = [1]*self.ndim

    log.debug('input_shape: {}, ndim: {}'.format(inputs.shape,self.ndim))

    bshape[0] = tf.shape(inputs)[0]

    broadcast = tf.ones(bshape)

    return tf.concat([b,self.zero*broadcast,a], axis=self.axis)

  def get_config(self):

    config = super().get_config().copy()

    config.update({
        'epoch': self.epoch,
        'axis': self.axis,
    })
    return config

class ApplyStep(tf.keras.layers.Layer):
  """
  A Layer for aplying a pure step to the input

  inputs: step size, signal
  output: signal + size * ( [[-0.5]]*epoch+[[0.5]]*(len-epoch) )
  """

  def __init__(self,epoch=None,axis=1,**kwargs):
    """
    initialize with epoch number and epoch axis.
    """
    super(ApplyStep, self).__init__(**kwargs)
    self.epoch = epoch
    self.axis = axis

  def build(self, input_shape):
    if self.epoch is None:
      self.epoch = input_shape[0][self.axis]/2

    self.channels = input_shape[1][-1]

    remainder = input_shape[0][self.axis] - self.epoch

    log.debug('epoch: {}, channels: {}'.format(self.epoch,self.channels))

    mask = tf.constant([[0.]*int(self.channels)]*self.epoch+
                                 [[1.]*int(self.channels)]*remainder)

    log.debug('mask shape: {}'.format(mask.shape))

    self.step_mask = tf.reshape(mask,[1]+mask.shape)
    log.debug('model Built')

  def call(self, inputs):

    log.debug('Calling')
    shape = tf.shape(inputs[1])
    log.debug('input shape: {}'.format(shape))

    shape = tf.concat([shape[:self.axis], [1], shape[self.axis:]],axis=0)

    log.debug('broadcasted shape: {}'.format(shape))

    mag = tf.reshape(inputs[1],shape)
    salto = tf.multiply(mag,self.step_mask)
    outputs = tf.add(salto, inputs[0])

    outputs = tf.subtract(outputs,
                tf.reduce_mean(outputs,self.axis,
                    keepdims=True))
    return outputs

  def get_config(self):

    config = super().get_config().copy()

    config.update({
        'epoch': self.epoch,
        'axis': self.axis
    })
    return config


class UnfoldMask(tf.keras.layers.Layer):

  def __init__(self,nan=np.nan,axis=1,**kwargs):
    super(UnfoldMask, self).__init__(**kwargs)
    self.nan = nan
    self.axis = axis

  def build(self, input_shape):
    if np.isnan(self.nan):
      self.get_mask = tf.math.is_nan
    else:
      self.compare = tf.ones(tf.TensorShape(input_shape[1:]))*self.nan
      self.get_mask = lambda x:tf.equal(x,self.compare)

  def call(self, inputs):
    mask = self.get_mask(inputs)

    negated = tf.cast(mask^True,tf.float32) #y = x xor true = not x
    inputs = tf.math.multiply_no_nan(inputs,negated) #nan to 0.

    outputs = tf.concat([inputs,negated], axis=self.axis)

    return outputs

  def get_config(self):

    config = super().get_config().copy()

    config.update({
        'nan': self.nan,
        'axis': self.axis
    })
    return config

class ApplyMask(tf.keras.layers.Layer):

  def __init__(self,nan=np.nan,axis=1,**kwargs):
    super(ApplyMask, self).__init__(**kwargs)
    self.nan = nan
    self.axis = axis

  def build(self, input_shape):
    pass

  def call(self, inputs):
    inputs,negated = tf.split(inputs,2,axis=self.axis)

    negated_bool = tf.cast(negated,tf.bool)
    mask = tf.cast(negated_bool^True,tf.float32)

    outputs = inputs + tf.math.multiply_no_nan(np.nan,mask)

    return outputs

  def get_config(self):

    config = super().get_config().copy()

    config.update({
        'nan': self.nan,
        'axis': self.axis
    })
    return config


class SubstractMean(tf.keras.layers.Layer):
  """
  Layer for reduceing data to the mean level

  Tensor(batches,epochs,chanels) -> ( Tensor(batches,epochs,chanels),
                                      Tensor(batches,channels) )
  """

  def __init__(self,axis=1,**kwargs):
    super(SubstractMean, self).__init__(**kwargs)
    self.axis = axis

  def build(self, input_shape):
    self.channels = input_shape[-1]

  def call(self, inputs):

    mask = tf.math.is_nan(inputs)
    notmask = tf.logical_not(mask)
    weights = tf.reduce_sum( tf.cast( notmask,tf.float32), axis=1)

    masked = tf.where(mask, tf.zeros_like(inputs), inputs)
    means = tf.reduce_sum(masked,axis=1) / weights

    means_ = tf.tile(means,[1,61])
    means_ = tf.reshape(means_,[-1,61,3])

    outputs = tf.subtract(inputs,means_)

    return outputs,means

  def get_config(self):

    config = super().get_config().copy()

    config.update({
        'axis': self.axis
    })
    return config

class RestoreMean(tf.keras.layers.Layer):
  """
  Layer for reduceing data to the mean level

  ( Tensor(batches,epochs,chanels),
    Tensor(batches,channels) ) -> Tensor(batches,epochs,chanels)
  """

  def __init__(self,axis=1,**kwargs):
    super(RestoreMean, self).__init__(**kwargs)
    self.axis = axis

  def build(self, input_shape):
    self.channels = input_shape[0][-1]
    self.epochs = input_shape[0][self.axis]
    self.batches = input_shape[0][0]

  def call(self, inputs):

    inputs,means = inputs

    # Esto es: repetir las medias de dimension
    # batches x canales tantas veces como self.epocas dentro
    # de cada batch, para
    # generar algo que es de forma batches x epocas x canales
    means = tf.tile(means,[1,self.epochs])
    means = tf.reshape(means, [-1, self.epochs,self.channels])

    # Restaurar las medias.
    outputs = tf.add(inputs,means)

    return outputs

  def get_config(self):

    config = super().get_config().copy()

    config.update({
        'axis': self.axis
    })
    return config

class PlainCounter(tf.keras.layers.Layer):
  """
  This layer is a Counter, gets any input.
  outputs the count of times it is called.
  Usefull for debugging purposes.
  """

  def __init__(self,axis=1,**kwargs):
    super(PlainCounter, self).__init__(**kwargs)

  def build(self, input_shape):
    self.C = self.add_weight(name='counter',initializer=tf.ones_initializer,shape=(1,),dtype=float,trainable=False)

  def call(self, inputs):
    self.C.assign_add([1])
    return self.C+1

  def get_config(self):

    config = super().get_config().copy()
    return config

class RandomBool(tf.keras.layers.Layer):
  """
  Layer generates BatchSize x 1 random bool mask
  Could be simulated with dropout layer
  """
  def __init__(self,prob=0.5,**kwargs):
    super(RandomBool, self).__init__(**kwargs)
    self.prob = prob

  def build(self, input_shape):
    self.gen = tf.random.get_global_generator()

  def call(self, inputs):

    shape = [tf.shape(inputs)[0],1]
    sellabels = self.gen.binomial(shape,[1.],[self.prob],dtype=float)

    return sellabels

  def get_config(self):
    config = super().get_config().copy()
    config.update({
        'prob': self.prob
    })
    return config

"""
Variance Normalization
"""

class SubstractVar(tf.keras.layers.Layer):
  """
  Layer for reduceing data to the variance level

  Tensor(batches,epochs,chanels) -> ( Tensor(batches,epochs,chanels),
                                      Tensor(batches,channels) )
  """

  def __init__(self,axis=1,**kwargs):
    super(SubstractVar, self).__init__(**kwargs)
    self.axis = axis

  def build(self, input_shape):
    self.channels = input_shape[-1]
    self.epochs = input_shape[self.axis]

  def call(self, inputs):

    mask = tf.math.is_nan(inputs)
    notmask = tf.logical_not(mask)
    weights = tf.reduce_sum( tf.cast( notmask,tf.float32), axis=1)

    masked = tf.where(mask, tf.zeros_like(inputs), inputs)

    means = tf.reduce_sum(masked,axis=1) / weights

    means_ = tf.tile(means,[1,self.epochs])
    means_ = tf.reshape(means_,[-1,self.epochs,self.channels])

    reduced = tf.subtract(inputs,means_)

    variances = tf.reduce_sum(reduced ** 2,axis=1) / (weights - 1)

    variances_ = tf.tile(variances ** 0.5,[1,self.epochs])
    variances_ = tf.reshape(variances_,[-1,self.epochs,self.channels])

    outputs = tf.divide(inputs,variances_)

    return outputs,variances

  def get_config(self):

    config = super().get_config().copy()

    config.update({
        'axis': self.axis
    })
    return config

class RestoreVar(tf.keras.layers.Layer):
  """
  Layer for reduceing data to the variance level

  ( Tensor(batches,epochs,chanels),
    Tensor(batches,channels) ) -> Tensor(batches,epochs,chanels)
  """

  def __init__(self,axis=1,**kwargs):
    super(RestoreVar, self).__init__(**kwargs)
    self.axis = axis

  def build(self, input_shape):
    self.channels = input_shape[0][-1]
    if len(input_shape[0]) > 2:
        self.epochs = input_shape[0][self.axis]
    else:
        self.epochs = None

    self.batches = input_shape[0][0]

  def call(self, inputs):

    inputs,variances = inputs

    if self.epochs is None:
        inputs = tf.reshape(inputs,[-1,1,self.channels])
        epochs = 1
    else:
        epochs = self.epochs

    # Esto es: repetir las medias de dimension
    # batches x canales tantas veces como self.epocas dentro
    # de cada batch, para
    # generar algo que es de forma batches x epocas x canales
    variances = tf.tile(variances,[1,epochs])
    variances = tf.reshape(variances, [-1, epochs,self.channels])

    # Restaurar las medias.
    outputs = tf.multiply(inputs,variances)

    if self.epochs is None:
        outputs = tf.reshape(outputs,[-1,self.channels])

    return outputs

  def get_config(self):

    config = super().get_config().copy()

    config.update({
        'axis': self.axis
    })
    return config
"""
End Variance normalization.
"""

class TrainingK(tf.keras.layers.Layer):
  def __init__(self,value, **kwargs):
    super(TrainingK, self).__init__(**kwargs)
    self.value = value

  def call(self,input,training=None):
    if training:
      return tf.constant(self.value,dtype=tf.float32)
    else:
      return tf.constant(1,dtype=tf.float32)

