import tensorflow as tf
from math import pi
from . import layers as l

_EPOCHAXIS=1

"""
Tests:

wrapReduceMean(Id) = Id
tf_ones -> wrapReduceMean(Id*n) -> tf_ones
tf_rand -> wrapReduceMean(Id*n) -> tf_rand*n

mean(wrapSplitMean(Id)[0],axis=1) = 0
mean(Id) = wrapSlplitMean[-1]

stepApply

handleNa

unfoldNa

ignoreEpoch
"""

def wrapReduceInputMean(modelo):

    x_shape = modelo.input.shape[1:]
    x = tf.keras.Input(x_shape)

    y,mean = l.SubstractMean()(x)

    y = modelo(y)

    return tf.keras.Model(inputs=x,outputs=y)

def wrapReduceMean(modelo):

    x_shape = modelo.input.shape[1:]
    x = tf.keras.Input(x_shape)

    y,mean = l.SubstractMean()(x)

    y = modelo(y)

    y = l.RestoreMean()([y,mean])

    return tf.keras.Model(inputs=x,outputs=y)

def wrapReduceVar(modelo,index=None):

    if index is None:
        x_shape = modelo.input.shape[1:]
        x = tf.keras.Input(x_shape)

        y,var = l.SubstractVar()(x)

        y = modelo(y)

        y = l.RestoreVar()([y,var])

    else:
        x = []

        for ix in range(len(modelo.input)):
            x_shape = modelo.input[ix].shape[1:]

            xi = tf.keras.Input(x_shape,name='entrada{}'.format(ix))

            if index == ix:
                yi,var = l.SubstractVar()(xi)
            else:
                yi = xi

            x.append(yi)

        y = modelo(x)

        y = l.RestoreVar()([y,var])

    return tf.keras.Model(inputs=x,outputs=y)


def wrapSplitMean(modelo):

    x_shape = modelo.input.shape[1:]

    x = tf.keras.Input(x_shape)

    y,mean = l.SubstractMean()(x)

    y = modelo(y)

    return tf.keras.Model(inputs=x,outputs=[o,mean])

def stepApply(modelo,epoca,axis=_EPOCHAXIS,keep_step=False):

    x_shape = modelo.input.shape[1:]
    inputs = tf.keras.Input(x_shape)

    y = modelo(inputs)

    x = l.DropEpoch(epoch=31)(inputs)

    x = l.ApplyStep(epoch=30)([y,x])

    # TODO: rename epoch to after
    x = l.AddDummyEpoch(epoch=30)(x)

    if keep_step:
        return tf.keras.Model(inputs=inputs,outputs=[y,x])
    else:
        return tf.keras.Model(inputs=inputs,outputs=x)


def handleNa(modelo):

    x_shape = modelo.input.shape[1:]
    inputs = tf.keras.Input(x_shape)

    data = l.UnfoldMask()(inputs) # NaN -> 0, duplicate epoch length,
                                # 2nd half is the mask.
    data, mask = tf.split(data,2,axis=1) # could be done inside layer.

    y = modelo(data)

    if modelo.output.shape[1:] == \
                 modelo.input.shape[1:]:
        #assume mask should be reapplied
        y = tf.concat([y,mask],axis=1)
        y = l.ApplyMask()(y)

    return tf.keras.Model(inputs=inputs,outputs=y)

def unfoldNa(model):

    x_shape = model.input.shape[1:].as_list()

    if x_shape[0] % 2:
        raise ValueError('unfold NaN mask: next input epoch axis length must be is even')

    x_shape[0] = x_shape[0]//2

    inputs = tf.keras.Input(x_shape)

    data = l.UnfoldMask()(inputs) # NaN -> 0, duplicate epoch length,
                                # 2nd half is the mask.
    _, mask = tf.split(data,2,axis=1) # could be done inside layer.

    y = model(data)

    if model.output.shape[1:].as_list() == x_shape:
        #assume mask should be reapplied
        y = tf.concat([y,mask],axis=1)
        y = l.ApplyMask()(y)

    return tf.keras.Model(inputs=inputs,outputs=y)

def ignoreEpoch(model,epoch=31):

    x_shape = model.input.shape[1:].as_list()
    x_shape[0] = x_shape[0]+1

    inputs = tf.keras.Input(x_shape)

    data = l.DropEpoch(epoch=epoch)(inputs)

    y = model(data)

    if model.output.shape[1:] == \
                 model.input.shape[1:]:
        #assume epoch should be reapplied
        y = l.AddDummyEpoch(epoch=epoch-1)(y)

    return tf.keras.Model(inputs=inputs,outputs=y)

def inputDropout(model,dropout_rate=0):

    x_shape = model.input.shape[1:]

    inputs = tf.keras.Input(x_shape)

    n_shape=[None]+x_shape.as_list()
    n_shape[-1] = 1 # all channels same noise

    x = tf.keras.layers.Dropout(rate=dropout_rate,
                                noise_shape=n_shape,
                                )(inputs)

    y = model(x)

    return tf.keras.Model(inputs=inputs,outputs=y)

# Estas funciones vienen de las versiones de notebooks del
# 10 de Enero de 2022

STEP = tf.constant([-0.5]*30 + [0] + [0.5]*30,dtype=tf.float32)


def apply_step(muestras,escalones):
    """
    muestras batch x 61 x 3, Escalones: batch x 3
    """

    return tf.transpose(
                tf.reshape(escalones,(-1,3,1)) * \
                tf.broadcast_to(STEP,(1,3,61)),
                    (0,2,1)) \
           + tf.cast(muestras,'float32')

def apply_step_R(muestras,escalones):
    """
    muestras batch x 61 x 3, Escalones: batch x 3
    """

    mbatch =  tf.transpose(
                tf.reshape(escalones,(-1,3,1)) * \
                tf.broadcast_to(STEP,(1,3,61)),
                    (0,2,1)) \
           + tf.cast(muestras,'float32')

    #cols = ['este','norte']
    length = tf.shape(mbatch)[0]
    length_ep = 61

    angs = tf.random.uniform(shape=[length],minval=-pi,maxval=pi)
    c = tf.math.cos(angs)
    s = tf.math.sin(angs)

    rots = tf.stack([[c,-s],
                     [s,c]])

    rots = tf.transpose(rots,(2,0,1))
    rots = tf.reshape(tf.repeat(rots,61,axis=0),(-1,61,2,2))

    data = mbatch[:,:,:2]
    data = tf.reshape(data,(-1,61,1,2))
    rotated = tf.reduce_sum(tf.math.multiply(data,rots),axis=-1)

    data = tf.stack([rotated[:,:,0],rotated[:,:,1],mbatch[:,:,-1]],axis=-1)

    #return
    return data

def random_step(muestras,escalones, nsamples=10):
    """
    muestras batch x 61 x 3, Escalones: batch x 6
    """
    nmuestras = tf.shape(muestras)[0]

    muestras = tf.reshape(tf.transpose(tf.broadcast_to(muestras,(nsamples,nmuestras,61,3)),(1,0,2,3)),(-1,61,3))

    mus = escalones[:,:3]
    sigmas = escalones[:,3:]

    noise = tf.random.normal(shape=(nsamples,nmuestras,3))

    escalones = tf.broadcast_to(mus,(nsamples,nmuestras,3)) +\
                    noise * tf.exp(tf.broadcast_to(sigmas,(nsamples,nmuestras,3)) / 2)

    escalones = tf.reshape(tf.transpose(escalones,(1,0,2)),(-1,3))

    return tf.transpose(
                tf.reshape(escalones,(-1,3,1)) * \
                tf.broadcast_to(STEP,(1,3,61)),
                    (0,2,1)) \
           + tf.cast(muestras,'float32')
