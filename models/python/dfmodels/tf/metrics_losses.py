import tensorflow as tf

class OneHotAccuracy(tf.keras.metrics.Accuracy):
    """
    Keras Accuracy metric takes a single integer label.
    this version takes one hot encoded vector.
    """
    def update_state(self, y_true, y_pred, sample_weight=None):
        y_true = tf.argmax(y_true,1)
        y_pred = tf.argmax(y_pred,1)
        return super(OneHotAccuracy,self).update_state(y_true,y_pred,sample_weight)

def accuracy_cost( y_true, y_pred):
    y_true = tf.argmax(y_true,1)
    y_pred = tf.argmax(y_pred,1)

    y_true = tf.cast(y_true,tf.float32) * 2. - 1.
    y_pred = tf.cast(y_pred,tf.float32) * 2. - 1.

    out = ( tf.multiply(y_true,y_pred) + 1. ) / 2.

    return tf.math.reduce_mean(out)

"""
For custom training loops we build two sets of losses

losses for case recognition, where labels are of dimension (2,) representing
p+,p-

losses for case and origin recognition, with labels of dimension (3,) representing
p+,p-,orign{0:data,1:altered}


case detection loss is simply a BinaryCrossentropy

case deceiver loss is computed as follows:

    data is split in [p+ > p-] and [p- > p+] groups.

    loss+  = [p+ > p-] >> deceiver >> apply step >> detection loss
    loss+_ = [p+ > p-] >> detection loss
    Dloss+ = 1 - (loss+ - loss+_)

    loss-  = [p- > p+] >> deceiver >> apply step >> detection loss
    loss-_ = [p- > p+] >> detection loss
    Dloss- = loss- - loss-_ >> abs

    loss = mean Dloss+ + mean Dloss-

This is, we both: - reward decreasing the detection of positives an
                  - penalize any alterarion of negative case detection.
"""

def get_loss_case_det(model, training=True):
    bce = tf.keras.losses\
                  .BinaryCrossentropy(from_logits=True,
                                      reduction=tf.keras.losses.Reduction.SUM)

    def loss_case_det(x, y):
        # training=training is needed only if there are layers with different
        # behavior during training versus inference (e.g. Dropout).
        y_ = model(x, training=training)

        return bce(y_true=y, y_pred=y_) / tf.cast(tf.shape(y)[0],dtype=tf.float32)

    return tf.function(
               loss_case_det,
               input_signature=[
                   tf.TensorSpec(shape=(None, 61, 3), dtype=tf.float32),
                   tf.TensorSpec(shape=(None, 2), dtype=tf.float32)
                   ]
               )


def get_loss_case_dec(model,detector, training= True, reduce=True):
    bce = tf.keras.losses\
                  .BinaryCrossentropy(from_logits=True,
                                      reduction=tf.keras.losses.Reduction.NONE)

    def loss_case_dec(x, y):
        x_ = model(x,training=training)

        # Compute mask for spliting + and - groups
        mask = tf.cast(tf.argmax(y,axis=-1),bool)

        # Compute detector responses both plain and deceived
        hat_y = detector(x, training=training)
        hat_y_ = detector(x_, training=training)

        length = tf.shape(y)[0]
        y_n = tf.stack([tf.zeros(length),tf.ones(length)],axis=1)

        # Compute losses
        loss = bce(y_true=y,y_pred=hat_y)

        #TODO: test if labels for + cases must be 0 1
        loss_ = bce(y_true=y,y_pred=hat_y_)
        loss_p = bce(y_true=y_n,y_pred=hat_y_)

        # Loss for + group
        #diffs_ = 1 - (loss_p - loss)
        diffs_ = loss_p

        # Loss for - group
        absdif = 10 * (loss_ - loss) ** 2

        # Apply mask on each group.
        cost1 = tf.boolean_mask(absdif,mask)
        cost2 = tf.boolean_mask(diffs_,tf.logical_not(mask))

        # Total Cost is sum.
        # TODO: Evaluate if wheigthing needed
        if reduce == True:
            return tf.reduce_mean(cost1) + tf.reduce_mean(cost2)
        else:
            return tf.reduce_mean(cost1) , tf.reduce_mean(cost2)

    return tf.function(
               loss_case_dec,
               input_signature=[
                   tf.TensorSpec(shape=(None, 61, 3), dtype=tf.float32),
                   tf.TensorSpec(shape=(None, 2), dtype=tf.float32)
                   ]
               )
