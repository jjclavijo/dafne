import tensorflow as tf

from pydafne.tf.layers import *

###
### Dummy models for testing purpose
###

def id_model() -> tf.keras.Model: 
    """
    build do-nothing model
    """
    inputs = tf.keras.Input(shape=[61,3])
    out = tf.identity(inputs)
    return tf.keras.Model(inputs=inputs,outputs=out)


def counter_model() -> tf.keras.Model: 
    """
    build counter times called model
    """

    inputs = tf.keras.Input(shape=[61,3])
    count = PlainCounter()(inputs)
    out = tf.ones_like(inputs)
    out = tf.keras.layers.multiply(out,count)
    
    return tf.keras.Model(inputs=inputs,outputs=out)

def add1_model() -> tf.keras.Model: 
    """
    build add_1 model
    """

    inputs = tf.keras.Input(shape=[61,3])
    out = tf.add(inputs,1)

    return tf.keras.Model(inputs=inputs,outputs=out)
