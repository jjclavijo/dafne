#!/bin/zsh

CONT=$(buildah from docker.io/python:3.7.12-slim)
MPOINT=$(buildah mount $CONT)

buildah run $CONT python -m venv /opt/venv
buildah run $CONT /opt/venv/bin/python -m pip install -U pip wheel
buildah run $CONT /opt/venv/bin/python -m pip install flit

cp -r ../python $MPOINT/opt/py.dfmodels

buildah run $CONT sh -c 'cd /opt/py.dfmodels ;env FLIT_ROOT_INSTALL=1 /opt/venv/bin/python -m flit install -s'

CONT2=$(buildah from docker.io/python:3.7.12-slim)
MPOINT2=$(buildah mount $CONT2)

cp -r --preserve=all $MPOINT/opt/venv $MPOINT2/opt/
cp -r ../python $MPOINT2/opt/py.dfmodels

buildah umount $CONT
buildah umount $CONT2

buildah config --cmd "" $CONT2
buildah config --entrypoint '["/opt/venv/bin/python"]' $CONT2

buildah commit $CONT2 dafne/models:latest

buildah rm $CONT
buildah rm $CONT2
