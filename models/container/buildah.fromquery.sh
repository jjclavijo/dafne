#!/bin/zsh

CONT=$(buildah from dafne/query:python)
MPOINT=$(buildah mount $CONT)

cp -r ../python $MPOINT/opt/py.dfmodels

buildah run $CONT sh -c 'cd /opt/py.dfmodels ;env FLIT_ROOT_INSTALL=1 /opt/venv/bin/python -m flit install -s'
buildah run $CONT /opt/venv/bin/python -m pip cache purge

buildah umount $CONT

buildah config --cmd "" $CONT
buildah config --entrypoint '["/opt/venv/bin/python"]' $CONT

buildah commit $CONT dafne/models:latest

buildah rm $CONT
