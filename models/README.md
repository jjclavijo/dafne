# Learning Models

The models are grouped in a module, that we named dfmodels

Model definitions are grouped in the `.models` submodule.

To train the models, the files in the `.train` submodule are also scripts
that can be run from the commandline.

The `.tf` submodule has some auxiliary functions, the most important are keras
layers used in the model to pre-process the samples, i.e. dropping the mid epoch.

The `invoke` script included loads the parameters from a config.json file.
We include a config.json with the parameters used for the models presented
in the paper.

Running the models requires a dataset folder, when running from containers
those folders can be mounted from the published
`registry.gitlab.com/dafne/data:artifacts` container, running for example:

```
podman run --rm \
           --mount 'type=volume,src=dafne_ds,dst=/ds,ro=true' \
           --mount 'type=volume,src=dafne_ds_soft,dst=/ds_soft,ro=true' \
           dafne/data:artifacts

podman run --rm \
           -v $(realpath .):/wdir -w=/wdir \
           --mount 'type=volume,src=dafne_ds,dst=/wdir/ds,ro=true' \
           --mount 'type=volume,src=dafne_ds_soft,dst=/wdir/ds_soft,ro=true' \
           dafne/models:latest invoke.py

podman volume rm dafne_ds dafne_ds_soft
```


Alternatively one can copy the dataset and the invoke script and run it with:

```
podman run -t -i --rm -v $(realpath .):/wdir -w=/wdir dafne/models:latest invoke.py
```

The datasets used in the paper can be extractd from the container using the
getArtifacts script on [the artifacts folder](../artifacts)
