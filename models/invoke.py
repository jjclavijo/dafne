import pathlib
import importlib
import click
import json

with open('config.json','r') as f:
    casos = json.load(f)


for caso in casos:

    routine = caso.pop('routine')
    train = importlib.import_module(f"dfmodels.train.{routine}")

    with open(f"p-{caso['report'][2:]}",'w') as f:
        print(caso,file=f)

    pathlib.Path(caso['report']).mkdir(parents=True, exist_ok=True)

    @click.command()
    @click.pass_context
    def fun(ctx):
        ctx.invoke(train.main_loop,**caso)
    try:
        fun()
    except SystemExit:
        pass
