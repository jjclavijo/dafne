\documentclass[a4paper,fleqn]{cas-sc} %Draft%

\usepackage[authoryear]{natbib}
\usepackage{graphicx} 
\usepackage{float}
\usepackage{algorithm}  
\usepackage{algpseudocode}
\usepackage{color}
\usepackage{setspace}

\usepackage[nolists,nomarkers,figuresonly]{endfloat} %draft

%% Extra packages and definitions

\usepackage{amsmath}
\usepackage{dsfont}
\usepackage[normalem]{ulem}
\usepackage{enumitem}

\usepackage{longtable}

\usepackage{tikz, pgfplots}
\pgfplotsset{compat=1.16}

\usetikzlibrary{positioning}
\usetikzlibrary{shapes.geometric}

\usepackage{multibib}
\newcites{SII}{S2 References: Seismic Detection Reports Review}

\renewcommand{\efloatseparator}{\vfill}
\renewcommand{\efloatend}{\clearpage}

\usepackage{xfp}

%% End Extra packages

\newcommand{\colorComments}{black} 
 
%%%Author definitions
\def\tsc#1{\csdef{#1}{\textsc{\lowercase{#1}}\xspace}}
\tsc{WGM}
\tsc{QE}
\tsc{EP}
\tsc{PMS}
\tsc{BEC}
\tsc{DE}
%%%

\usepackage{lineno}
\linenumbers %draft%

\begin{document}
\let\WriteBookmarks\relax
\def\floatpagepagefraction{1}
\def\textpagefraction{.001}

\shorttitle{Seismic deformation Adversarial Learning from GNSS}
\shortauthors{Clavijo JJ - Martínez JF}

\title[mode = title]{Adversarial learning of permanent seismic deformation from GNSS coordinate timeseries}

\author[1]{Javier José Clavijo}[type=editor,
                        auid=000,bioid=1,orcid=0000-0003-0984-1984]
\credit{ Algorithm, data processing, software development, data analysis, report writing }

\author[1,2]{Julián Facundo Martínez} 
\credit{ Mathematical formulation, report revision }

\address[1]{Facultad de Ingeniería, Universidad de Buenos Aires, Paseo Colón 850, C1063ACV, Buenos Aires, Argentina}
\address[2]{Instituto de Cálculo, Universidad de Buenos Aires - CONICET, Intendente Güiraldes 2160, Ciudad Universitaria, C1428EGA, Buenos Aires, Argentina}

\begin{abstract}
Deformation produced by an earthquake has a wide variety of forms. 
%% --- Background ---
Therefore, there are a variety of models for quantifying the amount of deformation observed on GNSS coordinate timeseries, each of them based on different assumptions about the underlying mechanism that generates the data. 
%% I: Underlaying mechanisms means: power spectra of a modelled additive noise, about the statistical properties of the series, about the presence of parametrizable signals, etc.
Hence, it is of interest to look for methods relying on minimal assumptions about the observed position series. 
%% I: Assumptions are: 
%% I: Finite-length timeseries of coordinates are sampled from some unknown probability distribution.
%% I: The distribution of samples from observation sites affected by seismic events must share at least part of its properties with the rest of the samples.

%% --- Our proposal ---
In this work we propose a semiparametric method, based on adversarial learning, to perform inference of the permanent seismic deformation.
%% I: Motivation: Adversarial learning is a tool that proved to be very flexible for approximating arbitrary statistical distributions from a set of its samples.
The only assumption done is that the probability distributions of GNSS fixed-length coordinate series for periods with and without observable seismic deformation differs mainly in the permanent deformation, represented by an additive scaled heavyside function.

%% --- Results ---
A dataset based on the series of GNSS coordinates published by the Nevada Geodetic Laboratory was built, and  an adversarial model was trained over this dataset.
In order to train the algorithm, an initial labeling of the samples was conducted using time, position an magnitude information of seismic events from the USGS database.
It was shown that learning was possible with the available real data, and multiple sanity checks were run, showing consistency of the offset estimations compared with a trajectory model based estimator and with published offsets for well studied events on the South American active margin.

To assess the capabilities of the method in a more controlled environment, further experiments were conducted on synthetic data.
Those experiments confirmed that the presence of postseismic transient signals does not impede learning.

As a derivative, our proposal allows to refine imperfect initial estimations for the presence/absence of seismic deformation.  
\end{abstract}

\nolinenumbers


\begin{keywords}
Seismic Displacement \sep Generative Models \sep Adversarial learning \sep GNSS
\end{keywords}

\linenumbers

\doublespacing 

%% 
%% 
%% ----------------------------------------------------------------------------------
%% 
%% 
%%
%% TODO: NOTE FOR THE EDITORS
%%
%%
%% Dear Editors: Note that we included the source for the supplementary material
%%               here, in order to keep reference to supplementary figures and
%%               sections in the text. We know that cross-referencing sections 
%%               is discouraged, but we found no other way to include the information
%%               that synthetic data added to the paper without substantially altering
%%               the desired narrative, which was centered on the work with real data.
%%
%%
%% ----------------------------------------------------------------------------------
%%
%%


%%
%% Supplementary material 1
%%

\doublespacing
\resetlinenumber

%% 
%%
%% This section resets page and float counters, adds an S. prefix to sections
%% And an S prefix to figures.
%%
%%

\makeatletter
\edef\Sfigure{\inteval{\c@figure}}
\renewcommand{\thefigure}{S\@arabic\numexpr \c@figure - \Sfigure} 
\makeatother
\renewcommand{\thetable}{S\arabic{table}} 

\makeatletter
\edef\Spage{\inteval{\c@page}}
\renewcommand{\thepage}{\@arabic\numexpr \c@page - \Spage + 1} 
\makeatother

\renewcommand{\lastpage}{\pageref{fig:EEhist_Synth}}

\renewcommand{\thetable}{S\arabic{table}} 
\makeatletter
\edef\Ssection{\inteval{\c@section}}
\renewcommand{\thesection}{S.\@arabic\numexpr \c@section - \Ssection} 
\makeatother


\section{Supplementary Figures, Training, and complementary evaluation \label{section:trainingEvaluation}}

\subsection{Training process with real data.}
\label{training_real}

In this section we provide plots of the training process for both models presented in the paper, when trained on the real data dataset.
We consider two loss functions: 
\begin{itemize}
	\item 
	generation loss function (...)
	$G:= \tfrac{1}{n^{*,1}} \sum_{x \in \mathcal{D}^{*, 1}} -f^*(d_\beta(g_\theta(x))), \qquad \text{where } f^*(t):= \tfrac{t^2}{4}+ t,$
	\item 
	global loss function (...)
	$V:= \tfrac{1}{n^{*,0}} \sum_{x \in \mathcal{D}^{*, 0}} d_\beta(x) + G.$
\end{itemize}

Both functions were computed for training and test sets during training.

Figures \ref{fig:loss} and \ref{fig:augloss} shows the training process of the model presented in Subsection \ref{amplituted_GAN}.
The latter with destructive data augmentation being used (see \ref{destructive_data_augmentation}).

Figures \ref{fig:altloss} and \ref{fig:regloss} shows the training process of model presented in Subsection \ref{learning_labels}.
The first of both figures shows each term of the loss function, the second one, the regularization term.
Finally, Figure \ref{fig:jumps} is similar to Figure \ref{fig:show_saltos_p} of the paper, but for the aforementioned model. 

\begin{figure}
\centering
Please see the submitted Supplementary Material PDF
%\includegraphics[width=9cm]{figs_s1/S01-ModelLoss.png}
\caption{
	Generation and global loss (see \ref{training_real}) for each training step, evaluated on the Train and Test sets.
}
\label{fig:loss}
\end{figure}

\begin{figure}
\centering
Please see the submitted Supplementary Material PDF
%\includegraphics[width=9cm]{figs_s1/S02-ModelAugLoss.png}
\caption{
	Generation and global loss (see \ref{training_real}) for each training step, evaluated on the Train and Test sets for the model trained as described in \ref{destructive_data_augmentation}.
}
\label{fig:augloss}
%TODO: Labels
\end{figure}

\begin{figure}
\centering
Please see the submitted Supplementary Material PDF
%\includegraphics[width=0.8\textwidth]{figs_s1/S03-AlternativeModelLoss.png}
\caption{
	Generation and global loss (see \ref{training_real}) for each training step, evaluated on the Train and Test sets for the model presented in \ref{learning_labels}.
	First row) provenance prediction output; Second row) label prediction output.
} 
\label{fig:altloss}
%TODO: Labels
\end{figure}

\begin{figure}
\centering
Please see the submitted Supplementary Material PDF
%\includegraphics[width=9cm]{figs_s1/S04-AlternativeModelRegularization.png}
\caption{
	Regularization loss for each training step, for the model presented in \ref{learning_labels}.
	Evaluation done on the Train and Test sets.
}
\label{fig:regloss}
%TODO: Labels
\end{figure}

\begin{figure}
\centering
Please see the submitted Supplementary Material PDF
%\includegraphics[width=1\textwidth]{figs_s1/S11.AlternativeJumps_n.png}
\caption{
	Samples from the $\mathcal{D}^{\star,1}$ group [green] and the output of $g_\theta(\cdot)$ for those samples [dark blue] after 1000 steps of training for the model presented in \ref{learning_labels}. 
	The red bars indicate the scale of 1cm (first twelve lines), 10cm (following three lines) or 1m (last line). 
}
\label{fig:jumps}
\end{figure}

\subsection{Complementary estimator comparisons}

We provide results from various complementary tests on amplitude inference mentioned in section \ref{section:infered_offsets}. All the results in this section are produced by the model trained with real data.

Figures \ref{fig:promedios}, \ref{fig:promediosU},  \ref{fig:promediosU} are analog to Figures \ref{fig:hector} \ref{fig:hectorU} and \ref{fig:hectorUaug}, computed with the first baseline method mentioned in the referred section..
With this method the displacement on a single GNSS observation site for a certain earthquake is computed by comparing the averaged coordinates for the five previous days and the five posterior days. \linelabel{sup:meanBased}

\begin{figure}
\centering
Please see the submitted Supplementary Material PDF
%\includegraphics[width=7cm]{figs_r1/04.Media.png}
\caption{2D histogram for comparison between estimated amplitudes and a mean-based displacement estimator for individual timeseries, channels East and North. The dashed red line marks the centimeter amplitude. }
\label{fig:promedios}
\end{figure}

\begin{figure}
\centering
Please see the submitted Supplementary Material PDF
%\includegraphics[width=7cm]{figs_r1/05.MediaU.png}
\caption{2D histogram for comparison between estimated amplitudes and a mean-based displacement estimator for individual timeseries, channel Up. The dashed red line marks the centimeter amplitude.}
\label{fig:promediosU}
\end{figure}

\begin{figure}
\centering
Please see the submitted Supplementary Material PDF
%\includegraphics[width=7cm]{figs_r1/11.MediaUaug.png}
\caption{2D histogram for comparison between estimated amplitudes and a mean-based displacement estimator for individual timeseries, channel Up. The dashed red line marks the centimeter amplitude.}
\label{fig:promediosUaug}
\end{figure}

On figure \ref{fig:MSE_Real} the Mean Square Error of the amplitudes estimations over the synthetic datasets is plotted. It is worth noting that this dataset, as discussed in supplementary section \ref{section:synthetic}, has notable differences with the training dataset, and the resulting MSE is not necessarily an indicator of the estimation error when dealing with real data.

\begin{figure}
\centering
 
Please see the submitted Supplementary Material PDF
%\includegraphics[width=12cm]{figs_s1/S09_MSE_real_ps_nops.png}
\caption{
	MSE for the model trained with real data and evaluated on the synthetic datasets.
	Samples splitted in offset amplitude ranges. 
	Solid line corresponds to the dataset including postseismic transients while dashed line to the one without it.
}
\label{fig:MSE_Real}
\end{figure}

It can be observed from this results that the presence of the transient postseismic signals in the real dataset does not prevented the learning.
The RMS Error is lower when evaluating over the dataset built without postseismic transient, indicating that this signal interferes with the estimation.
The mitigation of this influence is considered for future work.

\subsection{Recursive Application \label{ssec:recapply}}

Investigation of recursive model application included comparing the estimation error for N passes of the generator (Figure \ref{fig:MSE_Real_recursive}). 

\begin{figure}
\centering
Please see the submitted Supplementary Material PDF
%\includegraphics[width=8cm]{figs_s1/S10_rmse_vs_step_vs_recursive.png}
\caption{
	RMSE of the horizontal channels for the model trained with real data and evaluated on the synthetic datasets.
  Metrics are computed recursively for passes 1 to 50 [color ramp] each 10 training steps.
}
\label{fig:MSE_Real_recursive}
\end{figure}

We notice that this procedure converges to a single value, and that the reduction of the RMSE depends on the amplitude of the offsets and the presence of postseismic transients.

Also the effect of recursive application is more noticeable on early stages of the training.

The U channel, that has the worse error metrics on single pass, improves greatly from the use of recursive application. This motivates Figure \ref{fig:hectorUrec}, similar to \ref{fig:hectorU} but using recursive application with N=50.

\begin{figure}
\centering
Please see the submitted Supplementary Material PDF
%\includegraphics[width=9cm]{figs_s1/05.Hector66Urec_n.png}
\caption{2D histogram for comparison between estimated amplitudes and hector maximum likelihood estimation for individual timeseries, channel Up. Computed with recursive application of the generator, n=50.}
\label{fig:hectorUrec}
\end{figure}

Figure \ref{fig:histEE_rec} is similar to \ref{fig:synthhist}, but the errors were computed after recursive application of the trained model. Note that the main differences are on the greater offset ranges.


\begin{figure}
\centering
Please see the submitted Supplementary Material PDF
%\includegraphics[width=9cm]{figs_s1/S05_histogram_recursive.png}
\caption{
  2d Histogram of estimation errors over synthetic datasets when using recursive application (n=50). Positive error is offset overetimation, negative is underestimation. Figure similar to \ref{fig:synthhist}.
}
\label{fig:histEE_rec}
\end{figure}

\subsection{Results for Muisne and Maule event}

A similar comparision to the one of Figure \ref{fig:EventMap1} was made for the 2010-02-27 8.8Mw Maule event. The published offsets can be found on \cite{vigny2010MwMaule2011}.

\begin{figure}
\centering
Please see the submitted Supplementary Material PDF
%\hfill\includegraphics[height=11cm]{figs_s1/S06.Maule_66_vigny_n.png}\hfill%
\caption{Left) Estimation of displacement for the 2010-02-27 8.8Mw Maule event. Right) Displacements published on \cite{vigny2010MwMaule2011} \footnotesize Attribution: Background modified from Map tiles by Stamen Design CC BY 3.0 -- Base Map Data (C) OpenStreetMap contributors}
\label{fig:EventMap2}
\end{figure}

As cited on the main text, estimations for the 2016-04-16 7.8 Mw Muisne earthquake were computed, as an example of a moderate magnitude event. The results of this estimations are plotted in figure \ref{fig:EventMap3}

\begin{figure}
\centering
Please see the submitted Supplementary Material PDF
%\includegraphics[width=9cm]{figs_r1/09.Ecuador.png}\hfill%
\caption{Estimation of displacement for the 2016-04-16 7.8 Mw Muisne event. \footnotesize Attribution: Background modified from Map tiles by Stamen Design CC BY 3.0 -- Base Map Data (C) OpenStreetMap contributors}
\label{fig:EventMap3}
\end{figure}

\processdelayedfloats

%%
%% Supplementary material 2
%%

\resetlinenumber

\section{Bibliographic cases review of seismic event detection with respect to distance. \label{section:supplementary2}}


In this section we include the bibliographic review that conduced to the criteria used for labelling the samples.

We provide all the reviewed bibliographic references, alongside with the magnitude of the analyzed event and the distance to the farthest reported offset detection.
This distance was measured on the published maps when no table was available.

We are aware of the existence of other scaling laws relating Magnitude and Distance of possible detection of seismic events, e.g. the one used by the Nevada Geodetic Laboratory and published at \url{http://geodesy.unr.edu/NGLStationPages/steps_readme.txt}. \linelabel{s:scalinglaws}
These scaling laws are usually meant to have a threshold beyond which it is considered very unlikely to have detectable signals.

This, and the advantages for implementation and experimentation provided by a direct relation based on a table and a stepwise function were the motivations for our decision in favour of a stepwise function. 
We also added a threshold for large earthquakes to avoid a large class of samples that are usually included by laws like the Nevada one, but only a fraction of which are later shown to have offsets when tested with other methods.
All this situations can be viewed in figure \ref{fig:scaling}, where we also included the Nevada threshold as a reference.

\begin{singlespace}
\begin{longtable}[]{@{}llll@{}}
Citation & Magnitude & Distance \\
\toprule
\endhead
\citeSII{larsenGlobalPositioningSystem1992} & 6,6 & 150 \\
\citeSII{hudnutCoseismicDisplacements19921994} & 7,3 & 359 \\
\citeSII{tsujiCoseismicCrustalDeformation1995} & 8,1 & 600 \\
\citeSII{hashimotoCoseismicDisplacements19951996} & 7,2 & 500 \\
\citeSII{hudnutCoseismicDisplacements19941996} & 6,7 & 80 \\
\citeSII{rueggMWAntofagastaNorth1996} & 8,1 & 200 \\
\citeSII{melbourneGeodeticSignatureM81997} & 8 & 200 \\
\citeSII{wdowinskiSouthernCaliforniaPermanent1997} & 7,3 & 350 \\
\citeSII{stramondoSeptember2619971999} & 6 & 10 \\
\citeSII{yangGeodeticallyObservedSurface2000} & 7,6 & 120 \\
\citeSII{ayhanKinematicsMw122001} & 7,2 & 150 \\
\citeSII{reilingerCoseismicPostseismicFault2000} & 7,5 & 150 \\
\citeSII{huttonSlipKinematicsDynamics2001} & 8 & 200 \\
\citeSII{johnsonFaultGeometrySlip2001} & 7,5 & {} \\
\citeSII{yagiCoseismicSlipPostseismic2001} & 6,7 & 200 \\
\citeSII{yuPreseismicDeformationCoseismic2001} & 7,5 & {} \\
\citeSII{burgmannDeformation12November2002} & 7,2 & {} \\
\citeSII{jadeEstimatesCoseismicDisplacement2002a} & 7,6 & {} \\
\citeSII{agnewCoseismicDisplacementsHector2002} & 7,1 & 100 \\
\citeSII{kaverinaCombinedInversionSeismic2002} & 7,1 & {} \\
\citeSII{klotzGPSderivedDeformationCentral1999} & 8 & 300 \\
\citeSII{larsonUsing1HzGPS2003} & 7,9 & {} \\
\citeSII{pedersenFaultSlipDistribution2003} & 6,5 & 40 \\
\citeSII{yagiCoseismicSlipPostseismic2003} & 7,7 & 200 \\
\citeSII{miura2003M8TokachiOki2004} & 8 & 150 \\
\citeSII{yellesCoseismicDeformationMay2004a} & 6,8 & 40 \\
\citeSII{jadeCoseismicPostseismicDisplacements2005} & 9,3 & {} \\
\citeSII{perfettiniGeodeticDisplacementsAftershocks2005a} & 8,4 & {} \\
\citeSII{vignyInsight2004SumatraAndaman2005} & 9,3 & 2000 \\
\citeSII{hashimotoCrustalDeformationsAssociated2006} & 9,3 & {} \\
\citeSII{kreemerGlobalDeformationGreat2006} & 9,3 & {} \\
\citeSII{kreemerCoPostseismicDeformation2006} & 8,7 & 700 \\
\citeSII{langbeinCoseismicInitialPostseismic2006} & 6 & 30 \\
\citeSII{miuraCoPostseismicSlip2006} & 7,2 & 300 \\
\citeSII{ohtaLargeSurfaceWave2006} & 9,3 & {} \\
\citeSII{subaryaPlateboundaryDeformationAssociated2006} & 9,3 & {} \\
\citeSII{chingCoseismicSourceModel2007} & 6,8 & 60 \\
\citeSII{emoreRecoveringSeismicDisplacements2007} & {} & {} \\
\citeSII{koncaRuptureKinematics20052007a} & 8,6 & {} \\
\citeSII{bilichGPSSeismologyApplication2008} & 7,9 & {} \\
\citeSII{ohtaCoseismicFaultModel2008} & 7,2 & 40 \\
\citeSII{anzideiCoseismicDeformationDestructive2009} & 6,3 & 100 \\
\citeSII{hsuCoseismicPostseismicDeformation2009} & 6,8 & 70 \\
\citeSII{diaoSlipModel20082010} & 7,9 & 400 \\
\citeSII{jinCoseismicIonosphericDeformation2010} & {} & {} \\
\citeSII{vigny2010MwMaule2011} & 8,8 & 1200 \\
\citeSII{zhouGeodeticObservationsDetecting2012} & {} & {} \\
\citeSII{shestakovAnalysisFarfieldCrustal2012} & {} & {} \\
\citeSII{serpelloniFaultGeometryCoseismicslip2012} & 6,3 & 50 \\
\citeSII{hill2010MwMentawai2012} & 7,8 & 200 \\
\citeSII{yadavCoseismicOffsetsDue2013b} & 8,6 & 2000 \\
\citeSII{liRealtimeHighrateCoseismic2013a} & 7,2 & 120 \\
\citeSII{branzantiGPSNearrealtimeCoseismic2013a} & {} & {} \\
\citeSII{bedfordHighresolutionTimevariableAfterslip2013} & {} & {} \\
\citeSII{minsonBayesianInversionFinite2014a} & {} & {} \\
\citeSII{jiangGPSConstrainedCoseismic2014a} & 6,6 & 150 \\
\citeSII{nykolaishenGPSObservationsCrustal2015} & 7,8 & 250 \\
\citeSII{melgarSeismogeodesy2014Mw62015} & 6,1 & 50 \\
\citeSII{hill2012MwWharton2015} & 8,6 & {} \\
\citeSII{fengUnifiedGPSbasedEarthquake2015} & {} & {} \\
\citeSII{yue2016KumamotoMw2017} & 7 & 55 \\
\citeSII{liuRuptureFeatures20162017} & 6,2 & 70 \\
\citeSII{kleinComprehensiveAnalysisIllapel2017} & 8,3 & 1200 \\
\citeSII{huangCoseismicDeformationTriggered2017} & 6,2 & {} \\
\citeSII{gengIntegratingGPSGLONASS2017a} & 7,8 & {} \\
\citeSII{tiryakiogluSlipDistributionSource2018} & 6,6 & 60 \\
\citeSII{scognamiglioComplexFaultGeometry2018} & 6,5 & 40 \\
\citeSII{liuSourceCharacteristics20172019} & 7 & 120 \\
\bottomrule
\caption{Bibliographic review of reported co-seismic slip detection distances.}
\end{longtable}
\end{singlespace}

\begin{figure}
\null\hfill%
\begin{tikzpicture}[text=black,draw=black,y=.0045cm]
    \draw[dotted,gray,fill=gray!40] plot[xshift=-5cm] coordinates {
      (7.2,400) (7.2,2100) 
      (9.5,2100) (9.5,1000) 
      (7.5,1000)(7.5,400)};

    \draw[dotted,blue,fill=blue!30] plot[xshift=-5cm] coordinates {
      (6,0)
      (6,50) (6.5,50)
      (6.5,100) (7,100)
      (7,400) 
      (7.2,400) (7.2,2100) 
      (5,2100)(5,0)};

    \draw[dotted,orange,fill=orange!30] plot[xshift=-5cm] coordinates {
      (6,0)
      (6,50) (6.5,50)
      (6.5,100) (7,100)
      (7,400) (7.5,400)
      (7.5,1000) (9.5,1000)
      (9.5,0)};

    \draw plot[only marks, mark=*,xshift=-5cm] coordinates {
      (6.0,10)
      (6.0,30)
      (6.1,50)
      (6.2,70)
      (6.3,100)
      (6.3,50)
      (6.5,40)
      (6.5,40)
      (6.6,150)
      (6.6,150)
      (6.6,60)
      (6.7,200)
      (6.7,80)
      (6.8,40)
      (6.8,60)
      (6.8,70)
      (7.0,120)
      (7.0,55)
      (7.1,100)
      (7.2,120)
      (7.2,150)
      (7.2,300)
      (7.2,40)
      (7.2,500)
      (7.3,350)
      (7.3,359)
      (7.5,150)
      (7.6,120)
      (7.7,200)
      (7.8,200)
      (7.8,250)
      (7.9,400)
      (8.0,150)
      (8.0,200)
      (8.0,200)
      (8.0,300)
      (8.1,200)
      (8.1,600)
      (8.3,1200)
      (8.6,2000)
      (8.7,700)
      (8.8,1200)
      (9.3,2000)};


    \draw[thick,blue,dashed] plot[xshift=-5cm,domain=5:{(log10(2100)+0.79)/0.5}] (\x,{10^(0.5*\x-0.79)});
    \draw[thick,red,-] plot[xshift=-5cm] coordinates {
      (6,0)
      (6,50) (6.5,50)
      (6.5,100) (7,100)
      (7,400) (7.5,400)
      (7.5,1000) (9,1000)};

    \draw[->,xshift=-5cm] (5,0) -- coordinate (x axis mid) (9,0);
    \draw[->,xshift=-5cm] (5,0) -- coordinate (y axis mid)(5,2100);
    \foreach \x in {5,6,7,8,9}
        \draw [xshift=-5cm](\x cm,1pt) -- (\x cm,-3pt)
            node[anchor=north] {$\x$};
    \foreach \y/\ytext in {0/0,100/100,1000/1000,2000/2000}
        \draw (1pt,\y*.0045 cm) -- (-3pt,\y*.0045 cm) node[anchor=east] {$\ytext$};
    \node[below=0.5cm] at (x axis mid) {Magnitude};
    \node[left=1.2cm,rotate=90] at (y axis mid) {Detection Distance$[km]$};
\end{tikzpicture}%
\hfill%
\begin{tikzpicture}[text=black,draw=black,y=3cm]
    \draw[dotted,gray,fill=gray!40] plot[xshift=-5cm,yshift=-3cm] coordinates {
      (7.2,2.6020) (7.2,3.4) (9.5,3.4) (9.5,3) (7.5,3)(7.5,2.6020)};

    \draw[dotted,orange,fill=orange!30] plot[xshift=-5cm,yshift=-3cm] coordinates {
      (6,1) 
      (6,1.698) (6.5,1.698)
      (6.5,2) (7,2)
      (7,2.6020) (7.5,2.6020)
      (7.5,3) (9.5,3)
      (9.5,1) (6,1)};

    \draw[dotted,blue,fill=blue!30] plot[xshift=-5cm,yshift=-3cm] coordinates {
      (6,1) 
      (6,1.698) (6.5,1.698)
      (6.5,2) (7,2)
      (7,2.6020) (7.2,2.6020)
      (7.2,3.4)
      (5,3.4) (5,1)};
    
    \draw plot[only marks, mark=*,xshift=-5cm,yshift=-3cm] coordinates {
      (6.0,{log10(10)})
      (6.0,{log10(30)})
      (6.1,{log10(50)})
      (6.2,{log10(70)})
      (6.3,{log10(100)})
      (6.3,{log10(50)})
      (6.5,{log10(40)})
      (6.5,{log10(40)})
      (6.6,{log10(150)})
      (6.6,{log10(150)})
      (6.6,{log10(60)})
      (6.7,{log10(200)})
      (6.7,{log10(80)})
      (6.8,{log10(40)})
      (6.8,{log10(60)})
      (6.8,{log10(70)})
      (7.0,{log10(120)})
      (7.0,{log10(55)})
      (7.1,{log10(100)})
      (7.2,{log10(120)})
      (7.2,{log10(150)})
      (7.2,{log10(300)})
      (7.2,{log10(40)})
      (7.2,{log10(500)})
      (7.3,{log10(350)})
      (7.3,{log10(359)})
      (7.5,{log10(150)})
      (7.6,{log10(120)})
      (7.7,{log10(200)})
      (7.8,{log10(200)})
      (7.8,{log10(250)})
      (7.9,{log10(400)})
      (8.0,{log10(150)})
      (8.0,{log10(200)})
      (8.0,{log10(200)})
      (8.0,{log10(300)})
      (8.1,{log10(200)})
      (8.1,{log10(600)})
      (8.3,{log10(1200)})
      (8.6,{log10(2000)})
      (8.7,{log10(700)})
      (8.8,{log10(1200)})
      (9.3,{log10(2000)})};

    \draw[thick,blue,dashed] plot[xshift=-5cm,yshift=-3cm,domain=5:{(log10(2500)+0.79)/0.5}] (\x,{(0.5*\x-0.79)});
    \draw[thick,red,-] plot[xshift=-5cm,yshift=-3cm] coordinates {
      (6,1) 
      (6,1.698) (6.5,1.698)
      (6.5,2) (7,2)
      (7,2.6020) (7.5,2.6020)
      (7.5,3) (9,3)};
    \draw[->,xshift=-5cm,yshift=-3cm] (5,1) -- coordinate (x axis mid) (9,1);
    \draw[->,xshift=-5cm,yshift=-3cm] (5,1) -- coordinate (y axis mid)(5,3.3);
    \foreach \x in {5,6,7,8,9}
        \draw [xshift=-5cm](\x cm,1pt) -- (\x cm,-3pt)
            node[anchor=north] {$\x$};
    \foreach \y/\ytext in {1/10,2/100,3/1000}
        \draw [yshift=-3cm](1pt,\y*3 cm) -- (-3pt,\y*3 cm) node[anchor=east] {$\ytext$};
    \node[below=0.5cm] at (x axis mid) {Magnitude};
    \node[left=1.2cm,rotate=90] at (y axis mid) {Detection Distance$[km]$};
\end{tikzpicture}\hfill\null
\caption{
	Distance of farthest offset detected with respect to event magnitude (dots) for the analyzed publications. 
	Shaded areas corresponds to: 
	Blue) samples labelled as not having position offset, 
	Orange) samples labelled as having position offset, 
	Gray) samples discarded. 
	The dashed line is the scaling law used by the Nevada Geodesy Laboratory. 
	Plots are in linear (left) and logarithmic scale (right).}
\label{fig:scaling}
\end{figure}

\processdelayedfloats

\newpage

\bibliographystyleSII{cas-model2-names}
\bibliographySII{bibliography_q} 

%%
%% Supplementary material 3
%%

\resetlinenumber

\newpage

\section{Synthetic data generation and experiments. \label{section:synthetic}}

Synthetic data was generated by using HECTOR package. \citep{bosFastErrorAnalysis2013} 
Some code refactoring was needed, such as splitting noise generation in separated functions. 
Unit tests were written to guarantee the equivalence of the results produced with the modified code.
Some code were also needed to wrap hector estimation modules in order to use to produce the estimations for figures like figure \ref{fig:hector}. 
Both new code and test are available on the repository.

The generated dataset consists in signals with noise from a mixture of the types available on HECTOR.
The type and amount of noise for each timeseries were randomly selected, with noise possibly including more than one type of noise. 
The parameters for the noise models were also randomly sampled series by series.

Regarding offset magnitudes, a distribution that mimics that of the estimated offsets for the real data was used.
For the postseismic transient, the added functions had an amplitude chosen at random from the 5\% to the 50\% of the offset magnitude.

Finally, after randomization, we obtain two datasets: one with postseismic transient terms and one without.

It is worth it to mention that the purpose of this synthetic generation it is to serve as a sanity check for our method, rather than truly approximating real data.

This is a complex problem, out of the scope of this article. 
Even though, we observe as an interesting path for future work, to use GANs in order to tune synthetic generation to approximate the real distribution.

\subsection{GANs trained with synthetic data}

We trained the same GANs described on section \ref{arquitectura-de-las-redes.} using both synthetic datasets and evaluated the results of each trained generator also using both datasets.

We present the resulting rMSE for all four combinations on figure \ref{fig:MSE_synth}.
Note that, although each agent performs better for the kind of data on which was trained, differences are rather small. 
This is coherent with the fact that postseismic transients received no special treatment during training. 
Nevertheless, the small differences on the resulting rMSE also suggests that postseismic transients present results in a small handicap for the training.

We also produced figures similar to figure \ref{fig:synthhist} for both trained agents, to compare the influence of the presence of transient postseismic signals on the estimation results (see figures \ref{fig:EEhist_Synth} and \ref{fig:EEhist_Synth_rec}).
We note that, the distribution is multimodal for large values of $a$.

The recursive application of the model reduces both the variance and the bias of the estimations for large offsets.

\begin{figure}
Please see the submitted Supplementary Material PDF
% \null\hfill\includegraphics[width=12cm]{figs_s1/S16.rMSE_Synth_vs_Synth.png}\hfill\null%
  \caption{rMSE over both synthetic datasets for generators trained with each one of the datasets. Data was split in three ranges of offset amplitudes and plotted in separated panels.}
  \label{fig:MSE_synth}
\end{figure}

\begin{figure}
Please see the submitted Supplementary Material PDF
% \null\hfill\includegraphics[width=16cm]{figs_s1/S17.hist_Synth_vs_Synth.png}\hfill\null%
  \caption{2D histogram of estimation error for synthetic data on synthetic data trained models. 
Error sign indicates Over(+)/Under(-)estimation.
Upper panels: data without postseismic transients, Lower panels: data with postseismic transients.
}
  \label{fig:EEhist_Synth}
\end{figure}

\begin{figure}

Please see the submitted Supplementary Material PDF
% \null\hfill\includegraphics[width=16cm]{figs_s1/S18.hist_Synth_vs_Synth_rec.png}\hfill\null%
  \caption{2D histogram of estimation error for synthetic data on synthetic data trained models applied recursively with n=50. 
Error sign indicates Over(+)/Under(-)estimation.
Upper panels: data without postseismic transients, Lower panels: data with postseismic transients.
}
  \label{fig:EEhist_Synth_rec}
\end{figure}

\end{document}

%% TEMPLATE Copytight notice
%% Copyright 2019 Elsevier Ltd
%% 
%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%% ! ! ! SUBMISSION CHECKLIST ! ! ! %%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%
%% Please confirm that your submission follows all the requirements of the guidelines, including the submission checklist:
%% _ Cover letter
%% _ Highlights
%% _ Authorship statement
%% _ The manuscript must be single column and double spaced
%% _ Reference must be in the author-date format
%% _ Code availability section 
%%
%% *All the manuscripts in disagreement with the guidelines will be desk-rejected without editorial check.
%%
%% --------------------------------------
%%
%% This file is part of the 'CAS Bundle'.
%%  
%% It may be distributed under the conditions of the LaTeX Project Public
%% License, either version 1.2 of this license or (at your option) any
%% later version.  The latest version of this license is in 
%%    http://www.latex-project.org/lppl.txt 
%% and version 1.2 or later is part of all distributions of LaTeX
%% version 1999/12/01 or later.
%%   
%% The list of all files belonging to the 'CAS Bundle' is
%% given in the file `manifest.txt'.
%% 
%% Template article for cas-dc documentclass for  
%% double column output.
 
% vim: set spelllang=en:
