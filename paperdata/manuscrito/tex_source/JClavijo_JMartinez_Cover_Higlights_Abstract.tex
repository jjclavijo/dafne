%% 
%% Copyright 2019 Elsevier Ltd
%% 
%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%% ! ! ! SUBMISSION CHECKLIST ! ! ! %%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%
%% Please confirm that your submission follows all the requirements of the guidelines, including the submission checklist:
%% _ Cover letter
%% _ Highlights
%% _ Authorship statement
%% _ The manuscript must be single column and double spaced
%% _ Reference must be in the author-date format
%% _ Code availability section 
%%
%% *All the manuscripts in disagreement with the guidelines will be desk-rejected without editorial check.
%%
%% --------------------------------------
%%
%% This file is part of the 'CAS Bundle'.
%%  
%% It may be distributed under the conditions of the LaTeX Project Public
%% License, either version 1.2 of this license or (at your option) any
%% later version.  The latest version of this license is in 
%%    http://www.latex-project.org/lppl.txt 
%% and version 1.2 or later is part of all distributions of LaTeX
%% version 1999/12/01 or later.
%%   
%% The list of all files belonging to the 'CAS Bundle' is
%% given in the file `manifest.txt'.
%% 
%% Template article for cas-dc documentclass for  
%% double column output.

%\documentclass[a4paper,fleqn,longmktitle]{cas-dc}

\documentclass[a4paper,fleqn]{cas-sc} %Draft%

\usepackage[authoryear]{natbib}
\usepackage{graphicx} 
\usepackage{float}
\usepackage{algorithm}  
\usepackage{algpseudocode}
\usepackage{color}
\usepackage{setspace}

\usepackage[nomarkers,figuresonly]{endfloat} %draft

%% Extra packages and definitions

\usepackage{amsmath}
\usepackage{dsfont}
\usepackage[normalem]{ulem}
\usepackage{enumitem}

\usepackage{tikz, pgfplots}
\usetikzlibrary{positioning}
\usetikzlibrary{shapes.geometric}

%% USED IN DRAFT
%% \newcommand{\obs}[1]{{\iffalse #1 \fi}}
%% \def\revisar#1{\textcolor{red}{\uline{#1}}}
%% \def\julian#1{\textcolor{red}{#1}}
%% \def\javier#1{\textcolor{blue}{\bfseries #1}}

%% End Extra packages

\newcommand{\colorComments}{black} 
 
%%%Author definitions
\def\tsc#1{\csdef{#1}{\textsc{\lowercase{#1}}\xspace}}
\tsc{WGM}
\tsc{QE}
\tsc{EP}
\tsc{PMS}
\tsc{BEC}
\tsc{DE}
%%%

%\usepackage{lineno}
%\linenumbers %draft%


\begin{document}
\let\WriteBookmarks\relax
\def\floatpagepagefraction{1}
\def\textpagefraction{.001}

\shorttitle{Seismic deformation Adversarial Learning from GNSS}
\shortauthors{Clavijo JJ - Martínez JF}

\title[mode = title]{Adversarial learning of permanent seismic deformation from GNSS coordinate timeseries}

\author[1]{Javier José Clavijo}[type=editor,
                        auid=000,bioid=1,orcid=0000-0003-0984-1984]
\credit{ Algorithm, data processing, software development, data analysis, report writing }

\author[1,2]{Julián Facundo Martínez} 
\credit{ Mathematical formulation, report revision }

\address[1]{Facultad de Ingeniería, Universidad de Buenos Aires, Paseo Colón 850, C1063ACV, Buenos Aires, Argentina}
\address[2]{Instituto de Cálculo, Universidad de Buenos Aires - CONICET, Intendente Güiraldes 2160, Ciudad Universitaria, C1428EGA, Buenos Aires, Argentina}

\begin{abstract}
Deformation produced by an earthquake has a wide variety of forms.
Therefore, there are a variety of models for quantifying the amount of deformation observed on GNSS coordinate timeseries, each of them based on different assumptions.
Assumptions are made about the power spectra of a modelled additive noise, about the statistical properties of the series, about the presence of parametrizable signals, etc.
Having this in mind, it is of interest to look for methods relying on weaker assumptions of the observed position series. 

Finite-length timeseries of coordinates are sampled from some unknown probability distribution.
Furthermore, the distribution of samples from observation sites affected by seismic events must share at least part of its properties with the rest of the samples.
Adversarial learning is a tool that probed to be very flexible for approximating arbitrary statistical distributions from a set of its samples.

Under the only assumption that the distributions of k-length samples of GNSS coordinate series for periods with and without observable seismic deformation differs mainly in the permanent deformation, repsesented by an additive scaled heavyside function, we propose the usage of adversarial learning as a displacement inference method. 

%We, therefore, propose its usage to build a new displacement estimation method that is based on the aforementioned broad assumption and the ability of those models to learn unknown distributions.

%Those assumptions are that observation timeseries are samples of an unknown statistical distribution and there is an observable permanent deformation in the form of an additive heavyside function on those samples coming from a site affected by a seismic event. 
\end{abstract}

\begin{coverletter}

Dear Editors-in-Chief,
\newline
 
please find the enclosed manuscript "Adversarial learning of permanent seismic deformation from GNSS coordinate timeseries" which we are submitting for exclusive consideration for publication in Computers \& Geosciences. We confirm that the submission follows all the requirements and includes all the items of the submission checklist.  
\newline
 
The manuscript presents a method, based on ideas from generative models, for inferring the amount of offset in the positions observed by continuously operating GNSS receivers during seismic events. We argue that posing the problem as a probability distribution matching problem has a value in itself, loosening the assumptions needed and widening the range of possible solutions. 

The methodology is fully described, the models used for the inference and its parameters are detailed, and results for the trained models are presented, using a dataset built upon publicly available coordinate timeseries. The pre-processed data and the code for dataset building and model training is made available for download and use. 
\newline

We provide the source codes in a public repository with details listed in the section "Code availability".
\newline

Thanks for your consideration. 
\newline

Sincerely,
\newline

Javier José Clavijo, Julián Facundo Martínez

Facultad de Ingeniería, Universidad de Buenos Aires\\
Paseo Colón 850, C1063ACV, Buenos Aires, Argentina\\
jclavijo@fi.uba.ar
\newline

%\textbf{Delete before submission:}
%
%Please confirm that your submission follows all the requirements of the guidelines, including the submission checklist:
%
%- Cover letter v
%
%- Highlights
%
%- Authorship statement
%
%- The manuscript must be single column and double spaced
%
%- Reference must be in the author-date format
%
%- Code availability section 
%
%*The manuscripts that do meet the requirement guidelines will be desk-rejected.
%
\end{coverletter}
%
% 
\begin{highlights}
\item Adversarial Learning is used to generate a model that infers offsets due to earthquakes on GNSS timeseries
\item A dataset is build for model training with automatic labels based on external criteria.
\item Inference method is based on weak assumptions
\item Coordinate Timeseries are considered as samples of probability distributions
\end{highlights}

\begin{keywords}
Seismic Displacement \sep Generative Models \sep Adversarial learning \sep GNSS
\end{keywords}

\maketitle 

\printcredits

\end{document}

% vim: set spelllang=en:
